
-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 19, 2016 at 10:09 AM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a2132005_rpg`
--

-- --------------------------------------------------------

--
-- Table structure for table `personagem`
--

CREATE TABLE `personagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `arma` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ataque` int(11) NOT NULL,
  `vida` int(11) NOT NULL,
  `agilidade` int(11) NOT NULL,
  `forca` int(11) NOT NULL,
  `defesa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `personagem`
--

INSERT INTO `personagem` VALUES(1, 'Humano', 'Espada', 2, 12, 2, 1, 1);
INSERT INTO `personagem` VALUES(2, 'Orc', 'Clava de Madeira', 1, 20, 0, 2, 0);
