(function(){
	var validador = new Validador(),
		interface = new Interface()
		
		$('#formulario').submit(function(){
			
			$.ajax({
				url:$(this).attr('action'),
				type:'post',
				dataType:'json',
				data:{
					id 			: $('#id').val(),
					nome 		: $('#nome').val(),
					arma 		: $('#arma').val(),
					ataque 		: $('#ataque').val(),
					vida 		: $('#vida').val(),
					agilidade 	: $('#agilidade').val(),
					forca 		: $('#forca').val(),
					defesa 		: $('#defesa').val()
				},

				beforeSend:function(){
	                interface.iniciar_carregamento();
	            },

	            success:function(json){
	                if(json !== undefined){
	                    if(json.alerta !== undefined){
	                        interface.exibirAlerta('Existe um aviso para você',json.alerta);
	                        interface.finalizar_carregamento();
	                        return false;
	                    }

	                    if(json.sucesso === 'true'){
	                    	window.location.href = base_url + 'personagem'; 
	                    }                    
	                }
	            }
	        });    
       
        	return false;
		});

		$('.excluir').click(function(){
			var url = $(this).attr('href');

			interface.exibirAlertaConfirmacaoRedirecionamento(
				'Pedido de exclusão',
				'Deseja mesmo excluir?',
				url,
				base_url + 'personagem'
			);

			return false;
		});

})();