function Mascara() {
    this.cpf = function(entrada) {
        entrada = entrada.replace(/\D/g, "");
        entrada = entrada.replace(/(\d{3})(\d)/, "$1.$2");
        entrada = entrada.replace(/(\d{3})(\d)/, "$1.$2");
        entrada = entrada.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
        
        return entrada;
    };

    this.cnpj = function(entrada) {
        entrada = entrada.replace(/\D/g, "");
        entrada = entrada.replace(/^(\d{2})(\d)/, "$1.$2");
        entrada = entrada.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
        entrada = entrada.replace(/\.(\d{3})(\d)/, ".$1/$2");
        entrada = entrada.replace(/(\d{4})(\d)/, "$1-$2");

        return entrada;
    };

    this.cep = function(entrada) {
        entrada = entrada.replace(/D/g, "");
        entrada = entrada.replace(/^(\d{5})(\d)/, "$1-$2");

        return entrada;
    };

    this.telefone = function(entrada) {
        entrada = entrada.replace(/\D/g, "");
        entrada = entrada.replace(/^(\d\d)(\d)/g, "($1) $2");
        entrada = entrada.replace(/(\d{4})(\d)/, "$1-$2");

        return entrada;
    };

    this.celular = function(entrada) {
        entrada = entrada.replace(/\D/g, "");
        entrada = entrada.replace(/^(\d\d)(\d)/g, "($1) $2");
        entrada = entrada.replace(/(\d{5})(\d)/, "$1-$2");

        return entrada;
    };

    this.data = function(entrada) {
        entrada = entrada.replace(/\D/g, "");
        entrada = entrada.replace(/(\d{2})(\d)/, "$1/$2");
        entrada = entrada.replace(/(\d{2})(\d)/, "$1/$2");

        return entrada;
    };

    this.hora = function(entrada) {
        entrada = entrada.replace(/\D/g, "");
        entrada = entrada.replace(/(\d{2})(\d)/, "$1:$2");

        return entrada;
    };

    this.inteiro = function(entrada) {
        entrada = entrada.replace(/\D/g, "");

        return entrada;
    };
    
    this.moeda = function(entrada) {  
        entrada = entrada.replace(/\D/g,"")                     //permite digitar apenas números
        entrada = entrada.replace(/[0-9]{12}/,"inválido")       //limita pra máximo 999.999.999,99
        entrada = entrada.replace(/(\d{1})(\d{8})$/,"$1.$2")    //coloca ponto antes dos últimos 8 digitos
        entrada = entrada.replace(/(\d{1})(\d{5})$/,"$1.$2")    //coloca ponto antes dos últimos 5 digitos
        entrada = entrada.replace(/(\d{1})(\d{1,2})$/,"$1,$2")  //coloca virgula antes dos últimos 2 digitos
        
        return entrada;
    };
}