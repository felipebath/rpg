function Interface(){
    var caixa_carregamento = $('#caixa_carregamento');

    this.iniciar_carregamento = function(){
        caixa_carregamento.fadeIn('slow');
    };
    
    this.finalizar_carregamento = function(){
        caixa_carregamento.fadeOut('slow');
    };

    this.exibirAlerta = function(titulo, conteudo){
    	$('#alerta_titulo').text(titulo);
    	$('#alerta_conteudo').text(conteudo);

    	$('#alerta').modal({
    		show:true
    	});
    };

    this.exibirAlertaConfirmacaoRedirecionamento = function(titulo, conteudo, url_redirecionamento, url_desistencia){
        $('#alerta_confirmacao_redirecionamento_titulo').text(titulo);
        $('#alerta_confirmacao_redirecionamento_conteudo').text(conteudo);
        $('#alerta_confirmacao_redirecionamento_url_redirecionamento').attr({href:url_redirecionamento});
        $('#alerta_confirmacao_redirecionamento_url_desistencia').attr({href:url_desistencia});
        $('#alerta_confirmacao_redirecionamento').modal({
            show:true
        });
    };
};
