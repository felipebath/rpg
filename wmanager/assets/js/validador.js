function Validador(){
    var expressao;
    
    this.requerido = function(entrada){
        if(entrada !== ''){
            return true;
        }else{
            return false;
        }
    };
    
    this.cpf = function(entrada){
        expressao = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/i;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.cnpj = function(entrada){
        expressao = /^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/i;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.inteiro = function(entrada){
        expressao = /[0-9]/;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.cep = function(entrada){
        expressao = /[0-9]{5}-[0-9]{3}/;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.telefone = function(entrada){
        expressao = /\((10)|([1-9][1-9])\) [2-9][0-9]{3}-[0-9]{4,5}/;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.data = function(entrada){
        expressao = /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.hora = function(entrada){
        expressao = /[0-9]{2}:[0-9]{2}/;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.valor = function(entrada){
        expressao = /[0-9],[0-9]/;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.email = function(entrada){
        expressao = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/i;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
    
    this.senha = function(entrada){
        expressao = /^.*(?=.{6,})(?=.*\d)(?=.*[a-z]).*$/i;
        
        if(expressao.test(entrada)){
            return true;
        }else{
            return false;
        }
    };
}