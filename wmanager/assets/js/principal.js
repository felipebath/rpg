var base_url = "http://localhost/wmanager/";

(function(){
	var mascara = new Mascara(),
		input = $('input'),
		campo;

	input.keyup(function(){
		campo = $(this);

		if(campo.data('mascara-campo') !== undefined){
			switch(campo.data('mascara-campo')){
				case 'cpf':
					campo.val(mascara.cpf(campo.val()));
				break;

				case 'cnpj':
					campo.val(mascara.cnpj(campo.val()));
				break;

				case 'telefone':
					campo.val(mascara.telefone(campo.val()));
				break;

				case 'celular':
					campo.val(mascara.celular(campo.val()));
				break;

				case 'inteiro':
					campo.val(mascara.inteiro(campo.val()));
				break;

				case 'data':
					campo.val(mascara.data(campo.val()));
				break;

				case 'cep':
					campo.val(mascara.cep(campo.val()));
				break;

				case 'moeda':
					campo.val(mascara.moeda(campo.val()));
				break;
			}
		}
	});

	input.blur(function(){
		campo = $(this);

		if(campo.data('mascara-campo') !== undefined){
			switch(campo.data('mascara-campo')){
				case 'valor-2':
					campo.val(mascara.valor(campo.val(),2));
				break;

				case 'valor-4':
				campo.val(mascara.valor(campo.val(),4));
			}
		}
	});	

	$(document).ready(function(){
        $('.calendario').datepicker({
            todayBtn: "linked",
            format: "dd/mm/yyyy",               
            todayHighlight: true
        });
    });


})();