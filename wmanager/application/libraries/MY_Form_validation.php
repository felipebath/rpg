<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    protected $CI;

    function __construct() {
        parent::__construct();

        $this->CI = & get_instance();
    }
    
    public function senha($str){
        $this->CI->form_validation->set_message('senha','O campo %s não está no formato adequado (Exige: Letras e números).');
        
        if(preg_match('/^.*(?=.{6,})(?=.*\d)(?=.*[a-z]).*$/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    public function cpf($str){
        $this->CI->form_validation->set_message('cpf','O campo %s não está no formato adequado (Ex: 22.222.222-22).');
        
        if(preg_match('/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    public function cnpj($str){
        $this->CI->form_validation->set_message('cnpj','O campo %s não está no formato adequado (Ex: 22.222.222/2222-22).');
        
        if(preg_match('/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    public function cep($str){
        $this->CI->form_validation->set_message('cep','O campo %s não está no formato adequado (Ex: 22222-222).');
        
        if(preg_match('/[0-9]{5}-[0-9]{3}/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    public function telefone($str){
        $this->CI->form_validation->set_message('telefone','O campo %s não está no formato adequado (Ex: (22)2222-2222).');
        
        if(preg_match('/\((10)|([1-9][1-9])\) [2-9][0-9]{3}-[0-9]{4}/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function data($str){
        $this->CI->form_validation->set_message('data','O campo %s não está no formato adequado (Ex: 02/03/2000).');
        
        if(preg_match('/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function hora($str){
        $this->CI->form_validation->set_message('hora','O campo %s não está no formato adequado (Ex: 15:00).');
        
        if(preg_match('/[0-9]{2}:[0-9]{2}/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
    public function valor($str){
        $this->CI->form_validation->set_message('valor','O campo %s não está no formato adequado (Ex: 15,00).');
        
        $str = str_replace(',','.',$str);

        if(preg_match('/[0-9.,]/i', $str)){
            return true;
        }else{
            return false;
        }
    }
    
}