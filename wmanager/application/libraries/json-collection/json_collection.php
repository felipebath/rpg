<?php
/*
 * @author rodolfobarretoweb@gmail.com
 * @version 0.2
 */

class json_collection {
    public $dates;

    public function assign($index, $content) {
        if (!empty($index) && !empty($content)) {
            if (is_array($this->dates)) {
                # create a new array for index
                $new_dates = array($index => $content);

                # combines two arrays
                $this->dates = array_merge($this->dates, $new_dates);
            } else {
                # create a new array
                $this->dates = array($index => $content);
            }
        } else {
            return false;
        }
    }

    public function display() {
        # print array in formatted json
        print json_encode($this->dates);
    }
}
?>