<?php

/*
 * @author rodolfobarretoweb@gmail.com
 * @version 1.5
 */

class Crypt {
    /*
     * @var $iv 
     * @acess private
     */

    private $iv;

    /*
     * @acess public
     * @param $password
     * @return String
     */

    public function __construct() {
        $this->randomizar();
    }

    /*
     * @acess public
     * @param $string
     * @return String
     */

    public function getEncrypt($string, $password) {
        return $this->encrypt($string, $password);
    }

    /*
     * @acess public
     * @param $string
     * @return String
     */

    public function getDecrypt($string, $password) {
        return $this->decrypt($string, $password);
    }

    /*
     * @acess private
     * @return String
     */

    private function randomizar() {
        # gera iv randomizado
        $tamanho = mcrypt_get_iv_size(MCRYPT_CAST_256, MCRYPT_MODE_ECB);
        $this->iv = mcrypt_create_iv($tamanho, MCRYPT_DEV_RANDOM);
    }

    /*
     * @acess private
     * @param $string
     * @return String
     */

    private function encrypt($string, $password) {
        # criptografa os dados
        $crypt = mcrypt_encrypt(MCRYPT_CAST_256, $password, $string, MCRYPT_MODE_ECB, $this->iv);

        # retorna o dado criptografado
        return $crypt;
    }

    /*
     * @acess private
     * @param $string
     * @return String
     */

    private function decrypt($string, $password) {
        # descriptografa os dados
        $decrypt = mcrypt_decrypt(MCRYPT_CAST_256, $password, $string, MCRYPT_MODE_ECB, $this->iv);

        # retorna o dado descriptografado
        return $decrypt;
    }

}

?>