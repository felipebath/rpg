<?php /* Smarty version Smarty-3.1.16, created on 2014-08-12 09:08:48
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/relatorio-contas-a-pagar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:133927900253c7c6219c4383-84747471%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '928d1825ebd747d8061a45de97f63f15d50be0f4' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/relatorio-contas-a-pagar.tpl',
      1 => 1407844462,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '133927900253c7c6219c4383-84747471',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53c7c621a35387_16962611',
  'variables' => 
  array (
    'status_filtro' => 0,
    'base_url' => 0,
    'data_criacao' => 0,
    'hora_criacao' => 0,
    'data_inicio_filtro' => 0,
    'data_final_filtro' => 0,
    'codigo_fornecedor_filtro' => 0,
    'codigo_centro_de_custo_filtro' => 0,
    'codigo_contas_a_pagar' => 0,
    'nome_fornecedor' => 0,
    'nome_fantasia_fornecedor' => 0,
    'boleto_contas_a_pagar' => 0,
    'data_emissao_contas_a_pagar' => 0,
    'data_vencimento_contas_a_pagar' => 0,
    'valor_total_contas_a_pagar' => 0,
    'nome_centro_de_custo' => 0,
    'data_pagamento_contas_a_pagar' => 0,
    'numero_cheque_contas_a_pagar' => 0,
    'nome_banco' => 0,
    'observacoes_contas_a_pagar' => 0,
    'codigo_contas_a_pagar_subtotal' => 0,
    'nome_centro_de_custo_subtotal' => 0,
    'valor_total_contas_a_pagar_subtotal' => 0,
    'somatorio_valor_total_contas_a_pagar' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53c7c621a35387_16962611')) {function content_53c7c621a35387_16962611($_smarty_tpl) {?><!DOCTYPE html>
<html lang="pt-br">
<head>
<title>Alljob - Relatório - <?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</title>
<meta charset="UTF-8">
<link href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/css/relatorio.css" rel="stylesheet">
</head>

<body>
    <header>
        <table class="table-header">
            <tr>
                <td class="td1-table-header"><h1>Relatório - <?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</h1></td>
                <td class="td2-table-header"><span>Gerado em <b><?php echo $_smarty_tpl->tpl_vars['data_criacao']->value;?>
</b> as <?php echo $_smarty_tpl->tpl_vars['hora_criacao']->value;?>
 hs</span></td>
            </tr>
        </table>
    </header>
    
    <?php if ($_smarty_tpl->tpl_vars['data_inicio_filtro']->value===''&&$_smarty_tpl->tpl_vars['data_final_filtro']->value==='') {?>
        Período: <b>Não informado</b><br>
    <?php } else { ?>
        Período: <b><?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
</b> a <b><?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
</b><br>
    <?php }?>   
    Fornecedor: <b><?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor_filtro']->value;?>
</b><br>
    Obra: <b><?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo_filtro']->value;?>
</b><br>
    Status dos vencimentos: <b><?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</b>
        
    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    
    <br>
    <br>

        <table class="table-grid">
            <tr>
                <td class="col-line-divisor-center"><b>Cod</b></td>
                <td class="col-line-divisor-center"><b>Fornecedor</b></td>
                <td class="col-line-divisor-center"><b>Boleto</b></td>
                <td class="col-line-divisor-center"><b>Emissão</b></td>
                <td class="col-line-divisor-center"><b>Vencimento</b></td>
                <td class="col-line-divisor-center"><b>Valor</b></td>
                <td class="col-line-divisor-center"><b>Obra</b></td>
                <td class="col-line-divisor-center"><b>Pagamento</b></td>
                <td class="col-line-divisor-center"><b>Cheque</b></td>
                <td class="col-line-divisor-center"><b>Banco</b></td>
                <td class="col-line-divisor-center"><b>Valor Pago</b></td>
                <td class="line-divisor-center"><b>Observações</b></td>
            </tr>

            <tr>
                <?php if ($_smarty_tpl->tpl_vars['codigo_contas_a_pagar']->value!=='0') {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                        <tr>
                            <td class="col-line-divisor-center"><?php echo $_smarty_tpl->tpl_vars['codigo_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>    
                                                            
                            <td class="col-line-divisor-justify"><?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
                            
                            <td class="col-line-divisor-center"><?php echo $_smarty_tpl->tpl_vars['boleto_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
                            
                            <td class="col-line-divisor-center">
                                <?php if ($_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                <?php }?>   
                            </td>
                            
                            <td class="col-line-divisor-center">
                                <?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                <?php }?>
                            </td>
                            
                            <td class="col-line-divisor-center">
                                <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                            </td>

                            <td class="col-line-divisor-center"><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

                            <td class="col-line-divisor-center">
                                <?php if ($_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                <?php }?>   
                            </td>
                            
                            <td class="col-line-divisor-center"><?php echo $_smarty_tpl->tpl_vars['numero_cheque_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
                            
                            <td class="col-line-divisor-center">
                                <?php if ($_smarty_tpl->tpl_vars['status_filtro']->value==='Pago') {?>    
                                    <?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                <?php }?>    
                            </td>
                            
                            <td class="col-line-divisor-center">
                                <?php if ($_smarty_tpl->tpl_vars['status_filtro']->value==='Pago') {?>
                                    <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                                <?php }?>
                            </td>
                            
                            <td class="line-divisor-justify"><?php echo $_smarty_tpl->tpl_vars['observacoes_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
                        </tr>
                    <?php endfor; endif; ?>
                <?php } else { ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
    
        <!--subtotal-->
        <table class="table-subtotal">
            <tr>
                <td>
                    <br>
                </td>
            </tr>

            <tr>
                <td><b>Obra</b></td>
                <td>&nbsp;&nbsp;</td>
                <td><b>Subtotal (por obra)</b></td>
            </tr>

            <tr>
                <?php if ($_smarty_tpl->tpl_vars['codigo_contas_a_pagar_subtotal']->value!=='0') {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_contas_a_pagar_subtotal']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                        <tr>
                            <td><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_subtotal']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
                            <td>&nbsp;&nbsp;</td>
                            <td>R$ <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar_subtotal']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>
</td>
                        </tr>
                    <?php endfor; endif; ?>
                <?php } else { ?>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                <?php }?>
            </tr>
        </table>

        <!--total geral-->
        <table class="table-total-geral">
            <tr>
                <td>
                    <br>
                </td>
            </tr>

            <tr>
                <td>
                    <div align="right">
                        <b>Total Geral</b>
                    </div>
                </td>
            </tr>

            <tr>
                <tr>
                    <td align="right">R$ <?php echo $_smarty_tpl->tpl_vars['somatorio_valor_total_contas_a_pagar']->value;?>
</td>
                </tr>
            </tr>
        </table>
    <?php endfor; endif; ?>

</body>
</html>
<?php }} ?>
