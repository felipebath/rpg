<?php /* Smarty version Smarty-3.1.16, created on 2014-09-19 14:03:54
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-contas-a-pagar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:104059948754199a3d330f43-44781347%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a0885c575a08504f4d5f34fad4efac95193a5369' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-contas-a-pagar.tpl',
      1 => 1411128232,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '104059948754199a3d330f43-44781347',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54199a3d3c0036_91511453',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'id_fornecedor_filtro' => 0,
    'id_centro_de_custo_filtro' => 0,
    'status_filtro' => 0,
    'tipo_data_filtro' => 0,
    'data_inicio_filtro' => 0,
    'data_final_filtro' => 0,
    'status' => 0,
    'data_pagamento' => 0,
    'id_fornecedor' => 0,
    'nome_fornecedor' => 0,
    'id_fornecedor_select' => 0,
    'nome_fornecedor_select' => 0,
    'nota_fiscal' => 0,
    'data_emissao' => 0,
    'id_banco' => 0,
    'nome_banco' => 0,
    'id_banco_select' => 0,
    'nome_banco_select' => 0,
    'id_centro_de_custo' => 0,
    'nome_centro_de_custo' => 0,
    'id_centro_de_custo_select' => 0,
    'nome_centro_de_custo_select' => 0,
    'boleto' => 0,
    'data_vencimento' => 0,
    'forma' => 0,
    'numero_cheque' => 0,
    'valor' => 0,
    'juros' => 0,
    'credito' => 0,
    'valor_total' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54199a3d3c0036_91511453')) {function content_54199a3d3c0036_91511453($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar contas a pagar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/contas_a_pagar.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando contas a pagar</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar" class="pull-right btn btn-primary" title="Visualizar todos">Listar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/fazerEdicao" method="post" id="formulario">

			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_filtro']->value;?>
" id="id_fornecedor_filtro">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo_filtro']->value;?>
" id="id_centro_de_custo_filtro">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
" id="status_filtro">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['tipo_data_filtro']->value;?>
" id="tipo_data_filtro">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
" id="data_inicio_filtro">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
" id="data_final_filtro">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 bg-gray" style="margin-left:10px;">
								<label>Status pagamento</label>
						       	<select class="form-control" id="status">
						           	<?php if ($_smarty_tpl->tpl_vars['status']->value==='Pago') {?>
						           		<option value="Pago">Pago</option>
						        		<option value="A pagar">A pagar</option>	
						        	<?php } else { ?>
										<option value="A pagar">A pagar</option>
						           		<option value="Pago">Pago</option>
						           	<?php }?>
						        </select>
							</div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 bg-gray">
						       	<label>Data pagamento</label>
						       	<?php if ($_smarty_tpl->tpl_vars['status']->value==='Pago') {?>
						       		<input type="text" class="form-control" id="data_pagamento" data-mascara-campo="data" maxlength="10" value="<?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['data_pagamento']->value));?>
">
						       	<?php } else { ?>
						       		<input type="text" class="form-control" id="data_pagamento" data-mascara-campo="data" maxlength="10" disabled="yes">
						       	<?php }?>	
						        <span class='alerta_formulario' id='alerta_data_pagamento'></span>
						    </div>							
						</div>

						<br>

						<div class="row"> 	
						    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						       	<label>Fornecedor</label>
						       	<select class="form-control" id="id_fornecedor">
						           	<option value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor']->value;?>
">
						           		<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value;?>

						           	</option>
						           	<?php if ($_smarty_tpl->tpl_vars['id_fornecedor_select']->value!=='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_fornecedor_select']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                        	<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                        </option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum fornecedor</option>
	                                <?php }?>    
						        </select>
						    </div>
						</div>
						
						<br>    

						<div class="row"> 	
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Nota Fiscal</label>
						       	<input type="text" class="form-control" id="nota_fiscal" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['nota_fiscal']->value;?>
">
						       	<span class='alerta_formulario' id='alerta_nota_fiscal'></span>
						    </div>

						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Data Emissão</label>
						       	<input type="text" class="form-control"  id="data_emissao" data-mascara-campo="data" maxlength="10" value="<?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['data_emissao']->value));?>
">
						       	<span class='alerta_formulario' id='alerta_data_emissao'></span>
						    </div>

						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Banco</label>
						       	<select class="form-control" id="id_banco">
						           	<option value="<?php echo $_smarty_tpl->tpl_vars['id_banco']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['nome_banco']->value;?>
</option>
						           	<?php if ($_smarty_tpl->tpl_vars['id_banco_select']->value!=='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_banco_select']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_banco_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_banco_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum banco</option>
	                                <?php }?>    
						        </select>
						    </div>
						
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Centro de custo</label>
						       	<select class="form-control" id="id_centro_de_custo">
				           			<option value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value;?>
</option>
				           			<?php if ($_smarty_tpl->tpl_vars['id_centro_de_custo_select']->value!='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_centro_de_custo_select']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                        	<?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                        </option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum centro de custo</option>
	                                <?php }?>    
						        </select>
						    </div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Boleto</label>
						       	<input type="text" class="form-control" id="boleto" maxlength="80" value="<?php echo $_smarty_tpl->tpl_vars['boleto']->value;?>
">
						       	<span class='alerta_formulario' id='alerta_boleto'></span>
						    </div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Data vencimento</label>
						       	<input type="text" class="form-control"  id="data_vencimento" data-mascara-campo="data" maxlength="10" value="<?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['data_vencimento']->value));?>
">
						        <span class='alerta_formulario' id='alerta_data_vencimento'></span>
						    </div>
						
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Forma</label>
						       	<select class="form-control" id="forma">
						           	<?php if ($_smarty_tpl->tpl_vars['forma']->value==='Dinheiro') {?>
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cartão">Cartão</option>
							           	<option value="Cheque">Cheque</option>
						        	<?php }?>

						        	<?php if ($_smarty_tpl->tpl_vars['forma']->value==='Cartão') {?>
							           	<option value="Cartão">Cartão</option>
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cheque">Cheque</option>
						        	<?php }?>

						        	<?php if ($_smarty_tpl->tpl_vars['forma']->value==='Cheque') {?>
							           	<option value="Cheque">Cheque</option>
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cartão">Cartão</option>
						        	<?php }?>
						        </select>
						    </div>
													
							<?php if ($_smarty_tpl->tpl_vars['forma']->value==='Cheque') {?>
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							       	<div id="forma_pagamento_cheque">
							       		<label>Número do cheque</label>
					            		<input type="text" class="form-control" id="numero_cheque" value="<?php echo $_smarty_tpl->tpl_vars['numero_cheque']->value;?>
">
					            		<span class='alerta_formulario' id='alerta_numero_cheque'></span>
							    	</div>
							   </div>
							<?php }?>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<div id="forma_pagamento_cheque" style="display:none;">
						       		<label>Número cheque</label>
				            		<input type="text" class="form-control" id="numero_cheque" value="<?php echo $_smarty_tpl->tpl_vars['numero_cheque']->value;?>
">
				            		<span class='alerta_formulario' id='alerta_numero_cheque'></span>
						    	</div>
						   </div>
						</div>

						<br>
					
						<div class="row">
					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						    	<label>Valor (R$)</label>
						    	<input type="text" placeholder="Digite o valor" id="valor" class="form-control" data-mascara-campo="moeda" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
">
						    	<span class='alerta_formulario' id='alerta_valor'></span>
						    </div>

					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					       		<label>Juros (R$)</label>
					       		<input type="text" id="juros" data-mascara-campo="moeda" maxlength="10" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['juros']->value;?>
">
					       	</div>
					    
					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Crédito (R$)</label>
						       	<input type="text" placeholder="Digite o valor" id="credito" class="form-control" data-mascara-campo="moeda" value="<?php echo $_smarty_tpl->tpl_vars['credito']->value;?>
">
						    </div>

						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						    	<label>Valor Total (R$)</label>
						    	<input type="text" placeholder="Digite o valor" id="valor_total" class="form-control" data-mascara-campo="moeda" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['valor_total']->value;?>
" disabled="yes">
						    </div>
						</div>    
						
						<br>

						<div class="row">    
						    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						    	<label>Observações</label>
						    	<textarea id="observacoes" class="form-control" rows="3"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
						    </div>
						</div>                                                            
					
					</div>
				</div>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ('botoes-submit.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
