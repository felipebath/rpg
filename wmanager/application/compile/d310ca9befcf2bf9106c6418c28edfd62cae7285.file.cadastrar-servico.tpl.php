<?php /* Smarty version Smarty-3.1.16, created on 2014-09-16 17:09:29
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-servico.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17599974685417270d183a54-97172140%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd310ca9befcf2bf9106c6418c28edfd62cae7285' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-servico.tpl',
      1 => 1410880167,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17599974685417270d183a54-97172140',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5417270d1b2ad6_34139847',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5417270d1b2ad6_34139847')) {function content_5417270d1b2ad6_34139847($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar serviço"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/servico.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo serviço</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
servico" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
servico/fazerCadastro" method="post" id="formulario">
			
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
                            	<select class="form-control" id="status">
                                	<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>	                             
                                </select>
	                        </div>
	                    
	                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes">
                    		</div>

                    		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Descrição</label>
	                            <textarea id="descricao" class="form-control"></textarea>
	                        </div>	

                    		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Preço</label>
	                            <input type="text" class="form-control" id="preco" maxlength="15" data-mascara-campo="moeda">
	                        </div>
	                    </div>
	                    
	                    <br>

                    	<div class="row">
                    		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Observações</label>
	                            <textarea id="observacoes" class="form-control"></textarea>
	                        </div>	
	                	</div>                                                            
					</div>
				</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
