<?php /* Smarty version Smarty-3.1.16, created on 2014-05-22 21:09:08
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/editar-centro-de-custo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1910851289537cde65c5f0d7-70275992%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '568851a90d6e29f165a67b25b52fc27210fb6324' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/editar-centro-de-custo.tpl',
      1 => 1400785747,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1910851289537cde65c5f0d7-70275992',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537cde65d01809_41989251',
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_centro_de_custo' => 0,
    'status_centro_de_custo' => 0,
    'nome_centro_de_custo' => 0,
    'endereco_centro_de_custo' => 0,
    'bairro_centro_de_custo' => 0,
    'cep_centro_de_custo' => 0,
    'cidade_centro_de_custo' => 0,
    'estado_centro_de_custo' => 0,
    'responsavel_centro_de_custo' => 0,
    'observacoes_centro_de_custo' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537cde65d01809_41989251')) {function content_537cde65d01809_41989251($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/centro_de_custo.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class='row'>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando centro de custo</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo" class="btn btn-primary pull-right" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>
		
		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/fazerEdicao" method="post" id="formulario_editar_centro_de_custo">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value;?>
" id="codigo_editar_centro_de_custo">

		<div class="tab-content">
			<div class="tab-pane active" id="cadastro">
				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
								<select class="form-control" id="status_editar_centro_de_custo">                		
                                	<?php if (($_smarty_tpl->tpl_vars['status_centro_de_custo']->value==='Ativo')) {?>
                                		<option value="Ativo" selected="selected">Ativo</option>
	                                	<option value="Inativo">Inativo</option>
	                                	<option value="Bloqueado">Bloqueado</option>		                                	
                                	<?php }?>
                                	
                                	<?php if (($_smarty_tpl->tpl_vars['status_centro_de_custo']->value==='Inativo')) {?>
                                		<option value="Ativo">Ativo</option>
	                                	<option value="Inativo" selected="selected">Inativo</option>
	                                	<option value="Bloqueado">Bloqueado</option>		                                	
                                	<?php }?>

                                	<?php if (($_smarty_tpl->tpl_vars['status_centro_de_custo']->value==='Bloqueado')) {?>
                                		<option value="Ativo">Ativo</option>
	                                	<option value="Inativo">Inativo</option>
	                                	<option value="Bloqueado" selected="selected">Bloqueado</option>		                              	
	                                <?php }?>
	                            </select>
							</div>

							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Nome</label>
								<input type="text" id="nome_editar_centro_de_custo" placeholder="Digite o nome" class="form-control"  autocomplete="yes" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value;?>
">
								<span class='alerta_formulario' id='alerta_nome_editar_centro_de_custo'></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Endereço</label>
								<input type="text" id="endereco_editar_centro_de_custo" placeholder="Digite o endereço" class="form-control"  autocomplete="yes" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['endereco_centro_de_custo']->value;?>
">
								<span class='alerta_formulario' id='alerta_endereco_editar_centro_de_custo'></span>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Bairro</label>
								<input type="text" id="bairro_editar_centro_de_custo" placeholder="Digite o bairro" class="form-control"  autocomplete="yes" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['bairro_centro_de_custo']->value;?>
">
								<span class='alerta_formulario' id='alerta_bairro_editar_centro_de_custo'></span>
							</div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>CEP</label>
								<?php if ($_smarty_tpl->tpl_vars['cep_centro_de_custo']->value!=='') {?>
									<input type="text" id="cep_editar_centro_de_custo" class="form-control" autocomplete="yes" data-mascara-campo='cep' maxlength="9" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cep_centro_de_custo']->value,'#####-###');?>
">
								<?php } else { ?>
									<input type="text" id="cep_editar_centro_de_custo" placeholder="Digite o CEP" class="form-control" autocomplete="yes" data-mascara-campo='cep' maxlength="9">
								<?php }?>
								<span class='alerta_formulario' id='alerta_cep_editar_centro_de_custo'></span>
							</div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Cidade</label>
								<input type="text" id="cidade_editar_centro_de_custo" placeholder="Digite a cidade" class="form-control"  autocomplete="yes" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['cidade_centro_de_custo']->value;?>
">
								<span class='alerta_formulario' id='alerta_cidade_editar_centro_de_custo'></span>
							</div>

							<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
								<label>Estado</label>
								<select class="form-control" id="estado_editar_centro_de_custo">
									<option value="<?php echo $_smarty_tpl->tpl_vars['estado_centro_de_custo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['estado_centro_de_custo']->value;?>
</option>
									<option value="AC">AC</option>
									<option value="AL">AL</option>
									<option value="AM">AM</option>
									<option value="AP">AP</option>
									<option value="BA">BA</option>
									<option value="CE">CE</option>
									<option value="ES">ES</option>
									<option value="GO">GO</option>
									<option value="MA">MA</option>
									<option value="MG">MG</option>
									<option value="MS">MS</option>
									<option value="MT">MT</option>
									<option value="PA">PA</option>
									<option value="PB">PB</option>
									<option value="PE">PE</option>
									<option value="PI">PI</option>
									<option value="PR">PR</option>
									<option value="RJ">RJ</option>
									<option value="RN">RN</option>
									<option value="RO">RO</option>
									<option value="RR">RR</option>
									<option value="RS">RS</option>
									<option value="SC">SC</option>
									<option value="SE">SE</option>
									<option value="SP">SP</option>
									<option value="TO">TO</option>
								 </select>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Responsável</label>
								<input type="text" id="responsavel_editar_centro_de_custo" placeholder="Digite o nome do responsável" class="form-control"  autocomplete="yes" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['responsavel_centro_de_custo']->value;?>
">
								<span class='alerta_formulario' id='alerta_responsavel_editar_centro_de_custo'></span>
							</div>

							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Observações</label>
								<textarea id="observacoes_editar_centro_de_custo" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes_centro_de_custo']->value;?>
</textarea>
								<span class='alerta_formulario' id='alerta_observacoes_editar_centro_de_custo'></span>
							</div>
						</div>	
							
					</div>	
				</div>		
			</div>
		</div> <!-- cadastro -->
	</div> 

		<br>
		<input type="submit" value="Salvar alterações" class="btn btn-primary">

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
