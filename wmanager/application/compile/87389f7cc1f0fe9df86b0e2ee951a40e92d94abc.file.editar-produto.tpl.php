<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 22:06:58
         compiled from "/opt/lampp/htdocs/rr/application/views/editar-produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:71785642553fcdbcf43efa1-92793549%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '87389f7cc1f0fe9df86b0e2ee951a40e92d94abc' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/editar-produto.tpl',
      1 => 1409083513,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '71785642553fcdbcf43efa1-92793549',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fcdbcf485002_74654457',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'status' => 0,
    'nome' => 0,
    'quantidade_minima' => 0,
    'categoria' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fcdbcf485002_74654457')) {function content_53fcdbcf485002_74654457($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar produto"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/produto.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando produto</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" name="id">	
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
	                            	<select class="form-control" name="status">
	                                	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
	                                		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>		                                	
	                                	<?php }?>
	                                	
	                                	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
	                                		<option value="Inativo">Inativo</option>
		                                	<option value="Ativo">Ativo</option>		                                	
	                                	<?php }?>	                             
	                                </select>
	                        </div>
	                    
		                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                			<label>Nome</label>
	                        	<input type="text" placeholder="Nome da obra" class="form-control" name="nome" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
	                        	<span class='alerta_formulario' name='alerta_nome'></span>
	                		</div>

	                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	                			<label>Quantidade Mínima</label>
	                        	<input type="text" placeholder="Quantidade Mínima" class="form-control" name="quantidade_minima" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['quantidade_minima']->value;?>
">
	                        	<span class='alerta_formulario' name='alerta_quantidade_minima'></span>
	                		</div>

	                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Categoria</label>
	                            	<select class="form-control" name="categoria">
	                                	<?php if (($_smarty_tpl->tpl_vars['categoria']->value==='Outros')) {?>
		                                	<option value="Outros">Outros</option>
		                                	<option value="Arquitetônico">Arquitetônico</option>
		                                	<option value="Estrutura">Estrutura</option>
		                                	<option value="Instalações">Instalações</option>
		                                <?php }?>
		                                
		                                <?php if (($_smarty_tpl->tpl_vars['categoria']->value==='Arquitetônico')) {?>
		                                	<option value="Arquitetônico">Arquitetônico</option>
		                                	<option value="Outros">Outros</option>
		                                	<option value="Estrutura">Estrutura</option>
		                                	<option value="Instalações">Instalações</option>
		                                <?php }?>

		                                <?php if (($_smarty_tpl->tpl_vars['categoria']->value==='Estrutura')) {?>
		                                	<option value="Estrutura">Estrutura</option>
		                                	<option value="Outros">Outros</option>
		                                	<option value="Arquitetônico">Arquitetônico</option>
		                                	<option value="Instalações">Instalações</option>
		                                <?php }?>	

		                                <?php if (($_smarty_tpl->tpl_vars['categoria']->value==='Instalações')) {?>
		                                	<option value="Instalações">Instalações</option>
		                                	<option value="Outros">Outros</option>
		                                	<option value="Arquitetônico">Arquitetônico</option>
		                                	<option value="Estrutura">Estrutura</option>
		                                <?php }?>	
		                            </select>
	                        </div>
	                    </div>
	                    
	                    <br>

	                    <div class="row">    
	                		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                			<label>Observações</label>
	                        	<textarea name="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
	                		</div>

	                	</div>	
					</div>
				</div>
			<br>
			<br>
			<input type="submit" value="&nbsp;&nbsp;Salvar&nbsp;&nbsp;" class="btn btn-primary">
			&nbsp;&nbsp;
			<a href="javascript:history.go(-1)" class="btn btn-primary">Cancelar</a>
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
