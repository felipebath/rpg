<?php /* Smarty version Smarty-3.1.16, created on 2016-01-18 17:20:46
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-banco.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1609959406541069c9bbe078-55347463%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3ef141c30cf8288d9acfcafe9a40892147f6b34' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-banco.tpl',
      1 => 1453128556,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1609959406541069c9bbe078-55347463',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_541069c9c06161_79125293',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'status' => 0,
    'nome' => 0,
    'agencia' => 0,
    'conta' => 0,
    'operacao' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541069c9c06161_79125293')) {function content_541069c9c06161_79125293($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar banco"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/banco.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando banco</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Status</label>
                        	<select class="form-control" id="status">
                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
                            		<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>		                                	
                            	<?php }?>
                            	
                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
                            		<option value="Inativo">Inativo</option>
                                	<option value="Ativo">Ativo</option>		                                	
                            	<?php }?>	                             
                            </select>
                        </div>
                    
                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                			<label>Agência</label>
                        	<input type="text" placeholder="Agência" class="form-control" id="agencia" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['agencia']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                			<label>Conta</label>
                        	<input type="text" placeholder="Conta" class="form-control" id="conta" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['conta']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Operacao</label>
                        	
                        	<?php if ($_smarty_tpl->tpl_vars['operacao']->value==='Jurídico') {?>
	                        	<select class="form-control" id="operacao">
	                            	<option value="Jurídico">Jurídico</option>
	                            	<option value="Físico">Físico</option>
	                            	<option value="Poupança">Poupança</option>
	                            </select>
	                        <?php }?>    

	                        <?php if ($_smarty_tpl->tpl_vars['operacao']->value==='Físico') {?>
	                        	<select class="form-control" id="operacao">
	                            	<option value="Físico">Físico</option>
	                            	<option value="Jurídico">Jurídico</option>
	                            	<option value="Poupança">Poupança</option>
	                            </select>
	                        <?php }?>    

	                        <?php if ($_smarty_tpl->tpl_vars['operacao']->value==='Poupança') {?>
	                        	<select class="form-control" id="operacao">
	                            	<option value="Poupança">Poupança</option>
	                            	<option value="Jurídico">Jurídico</option>
	                            	<option value="Físico">Físico</option>
	                            </select>
	                        <?php }?>    
                        </div>
                    </div>
                    
                    <br>

                    <div class="row">    
                		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<label>Observações</label>
                            <textarea id="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
                        </div>	
                	</div>         
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
