<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 19:10:03
         compiled from "/opt/lampp/htdocs/rr/application/views/editar-obra.tpl" */ ?>
<?php /*%%SmartyHeaderCode:75230999753fcbf4874c189-21750381%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acfbad7f68380cba52b064036830cf415f691f79' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/editar-obra.tpl',
      1 => 1409073001,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75230999753fcbf4874c189-21750381',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fcbf487816e9_07628203',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'status' => 0,
    'nome' => 0,
    'responsavel' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fcbf487816e9_07628203')) {function content_53fcbf487816e9_07628203($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar obra"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/obra.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando obra</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
obra" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
obra/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" name="id">	
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
	                            	<select class="form-control" name="status">
	                                	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
	                                		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>		                                	
	                                	<?php }?>
	                                	
	                                	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
	                                		<option value="Inativo">Inativo</option>
		                                	<option value="Ativo">Ativo</option>		                                	
	                                	<?php }?>	                             
	                                </select>
	                        </div>
	                    
		                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                			<label>Nome</label>
	                        	<input type="text" placeholder="Nome da obra" class="form-control" name="nome" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
	                        	<span class='alerta_formulario' name='alerta_nome'></span>
	                		</div>

	                		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                			<label>Responsável</label>
	                        	<input type="text" placeholder="Nome do responsável" class="form-control" name="responsavel" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['responsavel']->value;?>
">
	                        	<span class='alerta_formulario' name='alerta_responsavel'></span>
	                		</div>

	                		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                			<label>Observações</label>
	                        	<textarea name="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
	                		</div>

	                	</div>	
					</div>
				</div>
			<br>
			<br>
			<input type="submit" value="&nbsp;&nbsp;Salvar&nbsp;&nbsp;" class="btn btn-primary">
			&nbsp;&nbsp;
			<a href="javascript:history.go(-1)" class="btn btn-primary">Cancelar</a>
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
