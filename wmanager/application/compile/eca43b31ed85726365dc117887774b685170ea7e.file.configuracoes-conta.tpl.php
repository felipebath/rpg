<?php /* Smarty version Smarty-3.1.16, created on 2014-09-10 22:11:51
         compiled from "/opt/lampp/htdocs/wmanager/application/views/configuracoes-conta.tpl" */ ?>
<?php /*%%SmartyHeaderCode:80033820054107d77111758-06823582%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eca43b31ed85726365dc117887774b685170ea7e' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/configuracoes-conta.tpl',
      1 => 1410379910,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '80033820054107d77111758-06823582',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54107d77167c64_75181798',
  'variables' => 
  array (
    'nome_empresa' => 0,
    'base_url' => 0,
    'codigo_conta' => 0,
    'permissao_cliente_empresa' => 0,
    'permissao_conta_empresa' => 0,
    'permissao_financeiro_empresa' => 0,
    'permissao_estoque_empresa' => 0,
    'permissao_colaborador_empresa' => 0,
    'permissao_produto_empresa' => 0,
    'permissao_servico_empresa' => 0,
    'permissao_fornecedor_empresa' => 0,
    'permissao_centro_custo_empresa' => 0,
    'permissao_banco_empresa' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54107d77167c64_75181798')) {function content_54107d77167c64_75181798($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Configurações de conta"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1>Configurações de conta</h1>
				</div>
			</div>
		</header>

		<div class='row'>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<ul class='list-group'>
					<li class='list-group-item list-group-divisor'>Empresa</li>
					<li class='list-group-item'><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_empresa']->value);?>
</li>

					<li class='list-group-item list-group-divisor'>Conta</li>
					<li class='list-group-item'><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_conta']->value;?>
">Editar minha conta</a></li>
					
					<li class='list-group-item list-group-divisor'>Módulos</li>

					<?php if ($_smarty_tpl->tpl_vars['permissao_cliente_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Cliente</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Cliente</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_conta_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Conta</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Conta</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_financeiro_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Financeiro</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Financeiro</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_estoque_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Estoque</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Estoque</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_colaborador_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Colaborador</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Colaborador</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_produto_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Produto</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Produto</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_servico_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Serviço</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Serviço</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_fornecedor_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Fornecedor</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Fornecedor</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_centro_custo_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Centro de Custo</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Centro de Custo</li>
					<?php }?>

					<?php if ($_smarty_tpl->tpl_vars['permissao_banco_empresa']->value==='1') {?>
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Banco</li>
					<?php } else { ?>
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Banco</li>
					<?php }?>
				</ul>
			</div>
		</div>
	
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
