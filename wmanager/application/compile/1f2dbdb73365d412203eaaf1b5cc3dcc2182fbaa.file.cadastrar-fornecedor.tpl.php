<?php /* Smarty version Smarty-3.1.16, created on 2014-08-29 20:46:15
         compiled from "/opt/lampp/htdocs/rr/application/views/cadastrar-fornecedor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5886455853fdd7662355a8-15847544%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f2dbdb73365d412203eaaf1b5cc3dcc2182fbaa' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/cadastrar-fornecedor.tpl',
      1 => 1409323994,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5886455853fdd7662355a8-15847544',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fdd766263d92_84336803',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fdd766263d92_84336803')) {function content_53fdd766263d92_84336803($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/fornecedor.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo fornecedor</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>
		
		<div class="tab-content">
			<div class="tab-pane active" id="fornecedor">
				<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/fazerCadastro" method="post" id="formulario">
					<br>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						
							<div class="row">
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<label>Tipo pessoa</label>
	                            	<select class="form-control" name="tipo_pessoa" autofocus="yes">
	                                	<option value="Física">Física</option>
	                                	<option value="Jurídica">Jurídica</option>
	                                </select>
		                        </div>

		                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                           	<label>Nome</label>
		                           	<input type="text" placeholder="Nome" name="nome" autocomplete="yes" class="form-control">
		                           	<span class='alerta_formulario' name='alerta_nome'></span>
		                        </div>
								
		                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<label>Status</label>
	                            	<select class="form-control" name="status">
	                                	<option value="Ativo">Ativo</option>
	                                	<option value="Inativo">Inativo</option>	                              
	                                </select>
		                        </div>
		                    </div>

		                    <br>

	                       	<div id="pessoa_juridica" style="display:none;">
	                    		<div class="row"> 	
	                       			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	    <label>Razão Social</label>
			                                <input type="text" placeholder="Razão social" class="form-control" name="razao_social" autocomplete="yes">
			                                <span class='alerta_formulario' name='alerta_razao_social'></span>
		                        	</div>

		                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                            	    <label>Inscriçao Estadual</label>
		                                <input type="text" placeholder="Inscriçao Estadual" class="form-control" name="inscricao_estadual" autocomplete="yes">
		                                <span class='alerta_formulario' name='alerta_inscricao_estadual'></span>
		                        	</div>
		                        	     
		                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                           	    <label>CNPJ</label>
			                            <input type="text" placeholder="CNPJ" class="form-control" name="cnpj" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes">
			                            <span class='alerta_formulario' name='alerta_cnpj'></span>
			                        </div>
			                	</div>
		                    </div>	
	                            
	                        <div id="pessoa_fisica">
		                    	<div class="row"> 	
	                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>CPF</label>
		                            	<input type="text" name="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14">
		                            	<span class='alerta_formulario' name='alerta_cpf'></span>
		                            </div>
		                        </div>    
		                    </div>	

		                    <br>

		                    <div>
		                    	<div class="row"> 	
	                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Endereço</label>
		                            	<input type="text" name="endereco" placeholder="Digite o endereço" class="form-control"  autocomplete="yes">
		                            	<span class='alerta_endereco' name='alerta_endereco'></span>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Bairro</label>
		                            	<input type="text" placeholder="Digite o bairro" name="bairro" autocomplete="yes" class="form-control" maxlength="30">
		                            	<span class='alerta_formulario' name='alerta_bairro'></span>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>CEP</label>
		                            	<input type="text" placeholder="Digite o Cep" name="cep" autocomplete="yes" class="form-control" autofocus="yes" data-mascara-campo='cep' maxlength="9">
		                            	<span class='alerta_formulario' name='alerta_cep'></span>
		                            </div>


		                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
										<label>Estado</label>
										<select class="form-control" name="estado">
											<option value="AC">AC</option>
											<option value="AL">AL</option>
											<option value="AM">AM</option>
											<option value="AP">AP</option>
											<option value="BA">BA</option>
											<option value="CE">CE</option>
											<option value="ES">ES</option>
											<option value="GO">GO</option>
											<option value="MA">MA</option>
											<option value="MG">MG</option>
											<option value="MS">MS</option>
											<option value="MT">MT</option>
											<option value="PA">PA</option>
											<option value="PB">PB</option>
											<option value="PE">PE</option>
											<option value="PI">PI</option>
											<option value="PR">PR</option>
											<option value="RJ" selected="selected">RJ</option>
											<option value="RN">RN</option>
											<option value="RO">RO</option>
											<option value="RR">RR</option>
											<option value="RS">RS</option>
											<option value="SC">SC</option>
											<option value="SE">SE</option>
											<option value="SP">SP</option>
											<option value="TO">TO</option>
										 </select>
									</div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Cidade</label>
		                            	<input type="text" placeholder="Digite a Cidade" name="cidade" autocomplete="yes" class="form-control" autofocus="yes">
		                            	<span class='alerta_formulario' name='alerta_cidade'></span>
		                            </div>
		                    	</div>

		                    	<br>

		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone 1</label>
		                            	<input type="text" placeholder="Digite o Telefone" name="telefone1" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15">
		                            	<span class='alerta_formulario' name='alerta_telefone1'></span>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone 2</label>
		                            	<input type="text" placeholder="Digite o Telefone" name="telefone2" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15">	                  
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone 3</label>
		                            	<input type="text" placeholder="Digite o Telefone" name="telefone3" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15">	                  
		                            </div>

		                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>E-mail</label>
		                            	<input type="text" placeholder="Digite o e-mail" name="email" autocomplete="yes" class="form-control" maxlength="80">
		                            	<span class='alerta_formulario' name='alerta_email'></span>
		                            </div>
		                        </div>

		                        <br>

		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Contato</label>
		                            	<input type="text" placeholder="Digite o contato" name="contato" autocomplete="yes" class="form-control" maxlength="35">
		                            </div>

		                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Site</label>
		                            	<input type="text" placeholder="Digite o site" name="site" autocomplete="yes" class="form-control" maxlength="25">	                  
		                            </div>

		                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>Anotações</label>
		                            	<textarea placeholder="Anotações" name="anotacoes" class="form-control"></textarea>	                  
		                            </div>
		                        </div>

		                        <br>
		                        <br>

		                        <div class="row"> 	
		                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                        		<input type="submit" value="Salvar e continuar" class="btn btn-primary">
										&nbsp;&nbsp;
										<a href="javascript:history.go(-1)" class="btn btn-primary">Cancelar</a>
									</div>
								</div>		
		                    
		                    </div>    
						</div>
					</div>
				</form>
			</div><!-- fornecedor -->
 		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
