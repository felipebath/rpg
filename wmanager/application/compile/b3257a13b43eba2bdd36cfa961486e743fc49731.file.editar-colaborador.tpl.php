<?php /* Smarty version Smarty-3.1.16, created on 2014-09-16 15:41:39
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-colaborador.tpl" */ ?>
<?php /*%%SmartyHeaderCode:553601887541837cc808866-52850691%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b3257a13b43eba2bdd36cfa961486e743fc49731' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-colaborador.tpl',
      1 => 1410874589,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '553601887541837cc808866-52850691',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_541837cc85a274_92924574',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'nome' => 0,
    'id_cargo' => 0,
    'nome_cargo' => 0,
    'id_cargo_list' => 0,
    'nome_cargo_list' => 0,
    'status' => 0,
    'matricula' => 0,
    'cpf' => 0,
    'sexo' => 0,
    'estado_civil' => 0,
    'identidade' => 0,
    'data_de_expedicao' => 0,
    'orgao' => 0,
    'endereco' => 0,
    'complemento' => 0,
    'bairro' => 0,
    'cep' => 0,
    'estado' => 0,
    'cidade' => 0,
    'telefone' => 0,
    'celular' => 0,
    'email' => 0,
    'data_de_nascimento' => 0,
    'nacionalidade' => 0,
    'naturalidade' => 0,
    'filiacao' => 0,
    'banco' => 0,
    'conta_corrente' => 0,
    'agencia' => 0,
    'pis_pasep' => 0,
    'data_de_admissao' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541837cc85a274_92924574')) {function content_541837cc85a274_92924574($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar colaborador"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/colaborador.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando colaborador</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
colaborador" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

        <form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
colaborador/fazerEdicao" method="post" id="formulario">
            <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id"> 
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <label>Nome</label>
                            <input type="text" placeholder="Nome" id="nome" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<label>Cargo</label>
                            <select class="form-control" id="cargo">
                                <option value="<?php echo $_smarty_tpl->tpl_vars['id_cargo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['nome_cargo']->value;?>
</option>
                                <?php if ($_smarty_tpl->tpl_vars['id_cargo_list']->value!=='0') {?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_cargo_list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_cargo_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
                                            <?php echo $_smarty_tpl->tpl_vars['nome_cargo_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </option>
                                    <?php endfor; endif; ?>
                                <?php } else { ?>
                                    <option value="">Não foi possível localizar nenhum cargo</option>
                                <?php }?>
                            </select>
                        </div>
                    	
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Status</label>
                            <select class="form-control" id="status">
                                <?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
                                    <option value="Ativo">Ativo</option>
                                    <option value="Inativo">Inativo</option>
                                <?php }?>
                                
                                <?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
                                    <option value="Inativo">Inativo</option>
                                    <option value="Ativo">Ativo</option>
                                <?php }?>
                            </select>
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <label>Matrícula</label>
                            <input type="text" id="matricula" class="form-control" maxlength="15" placeholder='Digite a matrícula' value="<?php echo $_smarty_tpl->tpl_vars['matricula']->value;?>
">
                        </div>
                    </div>    

                    <br>

                   	<div class="row"> 	
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           	<label>CPF</label>
                           	<input type="text" id="cpf" placeholder="Digite o número do cpf" class="form-control" data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf']->value,'###.###.###-##');?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <label>Sexo</label>
                            <select class="form-control" id="sexo">
                                <?php if (($_smarty_tpl->tpl_vars['sexo']->value==='Masculino')) {?>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Feminino">Feminino</option>
                                <?php } else { ?>
                                    <option value="Feminino">Feminino</option>
                                    <option value="Masculino">Masculino</option>
                                <?php }?>
                            </select>
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <label>Estado Civil</label>
                            <select class="form-control" id="estado_civil">
                                <option value="<?php echo $_smarty_tpl->tpl_vars['estado_civil']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['estado_civil']->value;?>
</option>
                                <option value="Solteiro(a)">Solteiro(a)</option>
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Separado(a)">Separado(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                                <option value="Viúvo(a)">Viúvo(a)</option>                                 
                            </select>
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <label>Identidade</label>
                            <input type="text" id="identidade" placeholder="Digite a identidade" class="form-control" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['identidade']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <label>Data de expedição</label>
                            <?php if ($_smarty_tpl->tpl_vars['data_de_expedicao']->value!==''&&$_smarty_tpl->tpl_vars['data_de_expedicao']->value!=='00/00/0000') {?>
                                <input type="text" id="data_de_expedicao" data-mascara-campo='data' class="form-control" maxlength="10" placeholder='Digite a data de expedição' value="<?php echo $_smarty_tpl->tpl_vars['data_de_expedicao']->value;?>
">
                            <?php } else { ?>
                                <input type="text" id="data_de_expedicao" data-mascara-campo='data' class="form-control" maxlength="10" placeholder='Digite a data de expedição'>
                            <?php }?>    
                        </div>
                    </div>    
                    
                    <br>

                    <div>
                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Órgão</label>
                                <input type="text" id="orgao" class="form-control" maxlength="15" placeholder='Digite o órgão' value="<?php echo $_smarty_tpl->tpl_vars['orgao']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	<label>Endereço</label>
                            	<input type="text" id="endereco" placeholder="Digite o endereço" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['endereco']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Complemento</label>
                            	<input type="text" placeholder="Digite o complemento" id="complemento" class="form-control" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['complemento']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Bairro</label>
                            	<input type="text" placeholder="Digite o bairro" id="bairro" class="form-control" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['bairro']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>CEP</label>
                            	<input type="text" placeholder="Digite o Cep" id="cep" class="form-control" data-mascara-campo='cep' maxlength="9" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cep']->value,'#####-###');?>
">
                            </div>
                    	</div>

                    	<br>

                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
								<label>Estado</label>
								<select class="form-control" id="estado">
									<option value="<?php echo $_smarty_tpl->tpl_vars['estado']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['estado']->value;?>
</option>
                                    <option value="AC">AC</option>
									<option value="AL">AL</option>
									<option value="AM">AM</option>
									<option value="AP">AP</option>
									<option value="BA">BA</option>
									<option value="CE">CE</option>
									<option value="ES">ES</option>
									<option value="GO">GO</option>
									<option value="MA">MA</option>
									<option value="MG">MG</option>
									<option value="MS">MS</option>
									<option value="MT">MT</option>
									<option value="PA">PA</option>
									<option value="PB">PB</option>
									<option value="PE">PE</option>
									<option value="PI">PI</option>
									<option value="PR">PR</option>
									<option value="RJ">RJ</option>
									<option value="RN">RN</option>
									<option value="RO">RO</option>
									<option value="RR">RR</option>
									<option value="RS">RS</option>
									<option value="SC">SC</option>
									<option value="SE">SE</option>
									<option value="SP">SP</option>
									<option value="TO">TO</option>
								 </select>
							</div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Cidade</label>
                            	<input type="text" placeholder="Digite a Cidade" id="cidade" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['cidade']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Telefone</label>
                            	<?php if ($_smarty_tpl->tpl_vars['telefone']->value!=='') {?>
                                    <input type="text" placeholder="Digite o Telefone" id="telefone" class="form-control" data-mascara-campo='telefone' maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone']->value,'(##) ####-####');?>
">
                                <?php } else { ?>
                                    <input type="text" placeholder="Digite o Telefone" id="telefone" class="form-control" data-mascara-campo='telefone' maxlength="14">
                                <?php }?>
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Celular</label>
                            	<input type="text" placeholder="Digite o Telefone" id="celular" class="form-control" data-mascara-campo='celular' maxlength="15" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['celular']->value,'(##) #####-####');?>
">
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <label>E-mail</label>
                                <input type="text" placeholder="Digite o e-mail" id="email" class="form-control" maxlength="80" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">
                            </div>
                        </div>

                        <br>

                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Data de nascimento</label>
                                <input type="text" id="data_de_nascimento" data-mascara-campo='data' class="form-control" maxlength="10" placeholder='Digite a data de nascimento' value="<?php echo $_smarty_tpl->tpl_vars['data_de_nascimento']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Nacionalidade</label>
                                <input type="text" id="nacionalidade" class="form-control" maxlength="30" placeholder='Digite a nacionalidade' value="<?php echo $_smarty_tpl->tpl_vars['nacionalidade']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Naturalidade</label>
                                <input type="text" id="naturalidade" class="form-control" maxlength="30" placeholder='Digite a naturalidade' value="<?php echo $_smarty_tpl->tpl_vars['naturalidade']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <label>Filiação</label>
                                <input type="text" id="filiacao" class="form-control" maxlength="120" placeholder='Digite o nome da mãe e do pai' value="<?php echo $_smarty_tpl->tpl_vars['filiacao']->value;?>
">
                            </div>
                        </div>

                        <br>

                        <div class="row">    
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <label>Banco</label>
                                <input type="text" id="banco" class="form-control" maxlength="30" placeholder='Digite o nome do banco' value="<?php echo $_smarty_tpl->tpl_vars['banco']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Conta corrente</label>
                                <input type="text" id="conta_corrente" class="form-control" maxlength="15" placeholder='Digite a conta corrente' value="<?php echo $_smarty_tpl->tpl_vars['conta_corrente']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Agência</label>
                                <input type="text" id="agencia" class="form-control" maxlength="10" placeholder='Digite a agência' value="<?php echo $_smarty_tpl->tpl_vars['agencia']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>PIS/PASEP</label>
                                <input type="text" id="pis_pasep" class="form-control" maxlength="30" placeholder='Digite o número' value="<?php echo $_smarty_tpl->tpl_vars['pis_pasep']->value;?>
">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Data de admissão</label>
                                <?php if ($_smarty_tpl->tpl_vars['data_de_admissao']->value!==''&&$_smarty_tpl->tpl_vars['data_de_admissao']->value!=='00/00/0000') {?>
                                    <input type="text" id="data_de_admissao" class="form-control" maxlength="10" data-mascara-campo='data' placeholder='Digite a data de admissão' value="<?php echo $_smarty_tpl->tpl_vars['data_de_admissao']->value;?>
">
                                <?php } else { ?>
                                    <input type="text" id="data_de_admissao" class="form-control" maxlength="10" data-mascara-campo='data' placeholder='Digite a data de admissão'>
                                <?php }?>
                            </div>
                        </div>

                        <br>

                        <div class="row">    
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label>Observações</label>
	                            <textarea id="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
	                        </div>	
                        </div>
                	</div>                                                            
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
