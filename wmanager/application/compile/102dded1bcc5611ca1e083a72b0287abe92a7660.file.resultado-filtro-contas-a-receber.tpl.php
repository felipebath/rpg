<?php /* Smarty version Smarty-3.1.16, created on 2014-08-12 21:12:37
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-contas-a-receber.tpl" */ ?>
<?php /*%%SmartyHeaderCode:188030285653ea660464e983-85257052%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '102dded1bcc5611ca1e083a72b0287abe92a7660' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-contas-a-receber.tpl',
      1 => 1407870755,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '188030285653ea660464e983-85257052',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53ea66046e6417_59256837',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_financeiro_conta' => 0,
    'codigo_cliente_filtro' => 0,
    'codigo_centro_de_custo_filtro' => 0,
    'status_filtro' => 0,
    'tipo_data_filtro' => 0,
    'data_inicio_filtro' => 0,
    'data_final_filtro' => 0,
    'permissao_excluir_financeiro_conta' => 0,
    'codigo_contas_a_receber' => 0,
    'nome_cliente' => 0,
    'nome_fantasia_cliente' => 0,
    'parcela_contas_a_receber' => 0,
    'data_emissao_contas_a_receber' => 0,
    'data_vencimento_contas_a_receber' => 0,
    'valor_total_contas_a_receber' => 0,
    'nome_centro_de_custo' => 0,
    'observacoes_contas_a_receber' => 0,
    'permissao_editar_financeiro_conta' => 0,
    'codigo_vencimento' => 0,
    'codigo_contas_a_receber_subtotal' => 0,
    'nome_centro_de_custo_subtotal' => 0,
    'valor_total_contas_a_receber_subtotal' => 0,
    'somatorio_valor_total_contas_a_receber' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ea66046e6417_59256837')) {function content_53ea66046e6417_59256837($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Contas a receber"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/contas_a_receber.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
					<h1>Contas a receber</h1>
				</div>

				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
					
					<div class='btn-group pull-right'>
						
						<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_receber/gerarRelatorio" target="blank">
							<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_financeiro_conta']->value==='1') {?>
								<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_receber/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
							<?php }?>
							<input type="hidden" name="codigo_cliente_filtro" value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente_filtro']->value;?>
">
							<input type="hidden" name="codigo_centro_de_custo_filtro" value="<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo_filtro']->value;?>
">
							<input type="hidden" name="status_filtro" value="<?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
">
							<input type="hidden" name="tipo_data_filtro" value="<?php echo $_smarty_tpl->tpl_vars['tipo_data_filtro']->value;?>
">
							<input type="hidden" name="data_inicio_filtro" value="<?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
">
							<input type="hidden" name="data_final_filtro" value="<?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
">
							<input type="submit" value=" Gerar Relatório " class="btn btn-primary">
						</form>
					</div>
				</div>	
			</div>
		</header>

		<div class="table-responsive">
			<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_receber" id="formulario_filtrar_contas_a_receber">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<input type='submit' value='Filtrar novamente' class='pull-left btn btn-primary'>
							</div>

							<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6' class="pull-left">
								<?php if ($_smarty_tpl->tpl_vars['data_inicio_filtro']->value===''&&$_smarty_tpl->tpl_vars['data_final_filtro']->value==='') {?>
									Período: <b>Não informado</b><br>
								<?php } else { ?>
									Período: <b><?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
</b> a <b><?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
</b><br>
								<?php }?>	
								Cliente: <b><?php echo $_smarty_tpl->tpl_vars['codigo_cliente_filtro']->value;?>
</b><br>
								Obra: <b><?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo_filtro']->value;?>
</b><br>
								Status dos vencimentos: <b><?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</b><br>
								Orientação: <b><?php echo $_smarty_tpl->tpl_vars['tipo_data_filtro']->value;?>
</b>
							</div>
						</div>
					</div>
				</div>	
			</form>

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Cliente</th>
					<th>Parcela</th>
					<th>Data Emissão</th>
					<th>Vencimento</th>
					<th>Valor</th>
					<th>Obra</th>
					<th>Observações</th>
					<th>Editar</th>
					<!--<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
						<th>Ação</th>
					<?php }?>-->
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_contas_a_receber']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_contas_a_receber']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['codigo_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>	
																
								<td><?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<th><?php echo $_smarty_tpl->tpl_vars['parcela_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</th>
								
								<th>
									<?php if ($_smarty_tpl->tpl_vars['data_emissao_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
										<?php echo $_smarty_tpl->tpl_vars['data_emissao_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	
								</th>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
										<?php echo $_smarty_tpl->tpl_vars['data_vencimento_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>
								</td>
								
								<td><?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['observacoes_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_receber/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_contas_a_receber']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit" title="editar"></span></a>
									<?php }?>
								</td>
								<!--<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/inativar/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_financeiro"><span class="glyphicon glyphicon-trash" title="inativar"></span></a>
									<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/ativar/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_financeiro"><span class="glyphicon glyphicon-plus" title="ativar"></span></a>
									<?php }?>
								</td>-->
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>

			<!--subtotal-->
			<table class='table table-hover table-condensed'>
				<thead>
					<th><div align="right">Obra</div></th>
					<th><div align="right">Subtotal (por obra)</div></th>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_contas_a_receber_subtotal']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_contas_a_receber_subtotal']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td align="right"><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_subtotal']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<td align="right">R$ <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_receber_subtotal']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>
</td>
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>

			<!--total geral-->
			<table class='table table-hover table-condensed'>
				<thead>
					<th>
						<div align="right">
							Total Geral
						</div>
					</th>
				</thead>

				<tbody>
					<tr>
						<td align="right">R$ <?php echo $_smarty_tpl->tpl_vars['somatorio_valor_total_contas_a_receber']->value;?>
</td>
					</tr>
				</tbody>
			</table>

		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
