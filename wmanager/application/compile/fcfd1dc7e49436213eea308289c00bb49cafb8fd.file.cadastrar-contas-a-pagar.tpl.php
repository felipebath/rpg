<?php /* Smarty version Smarty-3.1.16, created on 2014-09-17 20:23:20
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-contas-a-pagar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2957690175418a0bde71459-52089824%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fcfd1dc7e49436213eea308289c00bb49cafb8fd' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-contas-a-pagar.tpl',
      1 => 1410978198,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2957690175418a0bde71459-52089824',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5418a0bded6466_34930967',
  'variables' => 
  array (
    'base_url' => 0,
    'id_fornecedor' => 0,
    'nome_fornecedor' => 0,
    'id_banco' => 0,
    'nome_banco' => 0,
    'id_centro_de_custo' => 0,
    'nome_centro_de_custo' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5418a0bded6466_34930967')) {function content_5418a0bded6466_34930967($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/js/contas_a_pagar.js" defer ></script>
	<section class="container-fluid">
		<header class="page-header">
			<div class='row'>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando contas a pagar</h1>
				</div>
			
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar" class="btn btn-primary pull-right" title="Visualizar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/fazerCadastro" id="formulario" method="post">

		<div class="tab-content">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	
				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					<div class="row">
													
						<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
							<br>
							<div class="row">
			                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			                    	<label>Parcelas</label>
			                    	<input type="text" class="form-control" data-mascara-campo="inteiro" id="parcela" disabled="yes">
			                    </div>
			                </div>
			                				                
			                <br>    

			                <div class="row">
			                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			                    	<label>Valor Total</label>
			                    	<input type="text" class="form-control" data-mascara-campo="moeda" id="valor_total" disabled="yes">
			                	</div>
			                </div><!--row-->
			        	</div><!--col-xs-12-->    
			    	</div><!--row--> 	
				</div><!--col-xs-12-->
				
				<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                    	<label>Fornecedor</label>
		                    	<select class="form-control" id="id_fornecedor">
		                        	<?php if ($_smarty_tpl->tpl_vars['id_fornecedor']->value!=='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_fornecedor']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum fornecedor</option>
	                                <?php }?>
	                            </select>
		                        <span class='alerta_formulario' id='alerta_codigo_fornecedor_cadastrar_financeiro'></span>
                    		</div>
                    	</div>
                    	
                    	<br>

		            	<div class="row">
				            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Nota fiscal</label>
						       	<input type="text" id="nota_fiscal" autocomplete="yes" class="form-control" autofocus="yes">
						       	<span class='alerta_formulario' id='alerta_nota_fiscal'></span>
						    </div>

						    <div class="col-xs-12 col-sm- col-md-2 col-lg-2">
						       	<label>Data emissão</label>
						       	<input type="text" class="form-control" id="data_emissao" data-mascara-campo="data" maxlength="10" autocomplete="yes">
						    	<span class='alerta_formulario' id='alerta_data_emissao'></span>
						    </div>
				         	
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Banco</label>
						       	<select class="form-control" id="id_banco">
						           	<?php if ($_smarty_tpl->tpl_vars['id_banco']->value!='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_banco']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <?php if ($_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
	                                            <option value="<?php echo $_smarty_tpl->tpl_vars['id_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                            	<?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                            </option>
	                                        <?php } else { ?>
	                                            <option value="<?php echo $_smarty_tpl->tpl_vars['id_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                            	<?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                            </option>
	                                        <?php }?>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum banco</option>
	                                <?php }?>    
						        </select>
						    </div>
						
						    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						       	<label>Centro de custo</label>
						       	<select class="form-control" id="id_centro_de_custo">
				           			<?php if ($_smarty_tpl->tpl_vars['id_centro_de_custo']->value!='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_centro_de_custo']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <?php if ($_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
	                                            <option value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                            	<?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                            </option>
	                                        <?php } else { ?>
	                                            <option value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                            	<?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                            </option>
	                                        <?php }?>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum centro de custo</option>
	                                <?php }?>    
						        </select>
						    </div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Boleto</label>
						       	<input type="text" class="form-control" id="boleto" maxlength="80" autofocus="yes" autocomplete="yes">
						    	<span class='alerta_formulario' id='alerta_boleto'></span>
						    </div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Data vcto.</label>
						       	<input type="text" class="form-control"  id="data_vencimento" data-mascara-campo="data" maxlength="10" autocomplete="off">
						    	<span class='alerta_formulario' id='alerta_data_vencimento'></span>
						    </div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status Pgto.</label>
						       	<select class="form-control" id="status">
						           	<option value="A pagar" selected="yes">A pagar</option>
						           	<option value="Pago">Pago</option>
						        </select>
							</div>
						
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Data Pgto.</label>
						       	<input type="text" class="form-control" id="data_pagamento" data-mascara-campo="data" maxlength="10" disabled="yes" autocomplete="off">
						       	<span class='alerta_formulario' id='alerta_data_pagamento'></span>
						    </div>
						
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Forma</label>
						       	<select class="form-control" id="forma">
						           	<option value="Dinheiro">Dinheiro</option>
						           	<option value="Cartão">Cartão</option>
						           	<option value="Cheque">Cheque</option>
						        </select>
						    </div>
						
						    <div id="forma_pagamento_cheque" style="display:none;">
				               	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				        	    	<label>No. cheque</label>
				            		<input type="text" class="form-control" id="numero_cheque" autocomplete="off">
				            		<span class='alerta_formulario' id='alerta_numero_cheque'></span>
				            	</div>
				        	</div>
				        </div>
				        
				        <br>

				        <div class="row">    
				            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						    	<label>Valor (R$)</label>
						    	<input type="text" placeholder="Digite o valor" id="valor" class="form-control" data-mascara-campo="moeda" maxlength="15" autocomplete="off">
						    	<span class='alerta_formulario' id='alerta_valor'></span>
						   	</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					       		<label>Juros (R$)</label>
					       		<input type="text" id="juros" placeholder="Digite o valor" data-mascara-campo="moeda" maxlength="10" class="form-control" autocomplete="off">
					       	</div>
					    
					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Crédito (R$)</label>
						       	<input type="text" placeholder="Digite o valor" id="credito" class="form-control" data-mascara-campo="moeda" autocomplete="off">
						    </div>
						
						    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						    	<label>Observações</label>
						    	<textarea id="observacoes" class="form-control"></textarea>
						   	</div>
						</div>

						<br>

						<div class="row">    
						    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<button class="btn btn-primary" id="adicionar_item" title="adicionar">
									&nbsp;<b>+</b>&nbsp;
								</button>    
							</div>
						</div>	
					</div><!--col-xs-12-->	
					



					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<br>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<table class="table table-striped table-bordered table-hover">
                                	<thead>
                                    	<tr>
	                                        <th>Boleto</th>
	                                        <th>Vencimento</th>
	                                        <th>Status</th>
	                                        <th>Valor</th>
	                                        <th>Valor Total</th>
	                                        <th>Dt. Pgto.</th>
	                                        <th></th>
	                                    </tr>
	                                </thead>

	                                <tbody id="grid_itens">
	                                	
	                                </tbody>
	                        	</table>
                            </div>
                            <br>
						</div>
					</div>
				</div>		

			</div><!--col-xs-12-->

			<br>
			<?php echo $_smarty_tpl->getSubTemplate ('botoes-submit.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


				
		</div><!--tab content--> 

		

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
