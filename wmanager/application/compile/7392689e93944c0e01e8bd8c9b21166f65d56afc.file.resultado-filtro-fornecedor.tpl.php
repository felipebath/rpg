<?php /* Smarty version Smarty-3.1.16, created on 2014-05-19 15:05:53
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-fornecedor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1112631606537646db6c1e64-91317098%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7392689e93944c0e01e8bd8c9b21166f65d56afc' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-fornecedor.tpl',
      1 => 1400267891,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1112631606537646db6c1e64-91317098',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537646db75ccc7_96007162',
  'variables' => 
  array (
    'base_url' => 0,
    'campo_filtro' => 0,
    'parametro_filtro' => 0,
    'valor_filtro' => 0,
    'codigo_fornecedor' => 0,
    'tipo_pessoa_fornecedor' => 0,
    'nome_fornecedor' => 0,
    'nome_fantasia_fornecedor' => 0,
    'telefone_fornecedor' => 0,
    'cpf_fornecedor' => 0,
    'cnpj_fornecedor' => 0,
    'permissao_editar_fornecedor_conta' => 0,
    'permissao_excluir_fornecedor_conta' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537646db75ccc7_96007162')) {function content_537646db75ccc7_96007162($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/fornecedor.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Fornecedor</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

					<div class='btn-group pull-right'>
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/" class="btn btn-primary" title="Listar todos">
							Listar todos
						</a>

						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/cadastrar" class="btn btn-primary" title="Cadastrar">	Cadastrar</a>
					</div>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			
			<!-- filtro -->
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/filtrar" id="formulario_filtrar_fornecedor">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' class="form-control" id="campo_filtrar_fornecedor">
           						<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='status_fornecedor') {?>
										<option value="status_fornecedor">Status (Ativo, Inativo)</option>
				               			<option value="nome_fornecedor">Nome do fornecedor</option>
				               			<option value="cpf_fornecedor">CPF</option>
				               			<option value="nome_fantasia_fornecedor">Nome fantasia</option>
				               			<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
				               			<option value="razao_social_fornecedor">Razão social</option>
				               			<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='nome_fornecedor') {?>
										<option value="nome_fornecedor">Nome do fornecedor</option>
		               					<option value="status_fornecedor">Status (Ativo, Inativo)</option>
		               					<option value="cpf_fornecedor">CPF</option>
		               					<option value="nome_fantasia_fornecedor">Nome fantasia</option>
		               					<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social_fornecedor">Razão social</option>
		               					<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cpf_fornecedor') {?>
										<option value="cpf_fornecedor">CPF</option>
		               					<option value="status_fornecedor">Status (Ativo, Inativo)</option>
		               					<option value="nome_fornecedor">Nome do fornecedor</option>
		               					<option value="nome_fantasia_fornecedor">Nome fantasia</option>
		               					<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social_fornecedor">Razão social</option>
		               					<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='nome_fantasia_fornecedor') {?>
										<option value="nome_fantasia_fornecedor">Nome fantasia</option>
		               					<option value="status_fornecedor">Status (Ativo, Inativo)</option>
		               					<option value="nome_fornecedor">Nome do fornecedor</option>
		               					<option value="cpf_fornecedor">CPF</option>
		               					<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social_fornecedor">Razão social</option>
		               					<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='tipo_pessoa_fornecedor') {?>
										<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="status_fornecedor">Status (Ativo, Inativo ou Bloqueado)</option>
		               					<option value="nome_fornecedor">Nome do fornecedor</option>
		               					<option value="cpf_fornecedor">CPF</option>
		               					<option value="nome_fantasia_fornecedor">Nome fantasia</option>
		               					<option value="razao_social_fornecedor">Razão social</option>
		               					<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='razao_social_fornecedor') {?>
										<option value="razao_social_fornecedor">Razão social</option>
		               					<option value="status_fornecedor">Status (Ativo, Inativo)</option>
		               					<option value="nome_fornecedor">Nome do fornecedor</option>
		               					<option value="cpf_fornecedor">CPF</option>
		               					<option value="nome_fantasia_fornecedor">Nome fantasia</option>
		               					<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cnpj_fornecedor') {?>
										<option value="cnpj_fornecedor">CNPJ</option>
										<option value="status_fornecedor">Status (Ativo, Inativo)</option>
		               					<option value="nome_fornecedor">Nome do fornecedor</option>
		               					<option value="cpf_fornecedor">CPF</option>
		               					<option value="nome_fantasia_fornecedor">Nome fantasia</option>
		               					<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social_fornecedor">Razão social</option>
		               		<?php }?>
								</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='iniciado_por') {?>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='igual') {?>
										<option value="igual">Igual</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='terminado_em') {?>
										<option value="terminado_em">Terminado em</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='em_qualquer_posicao') {?>
										<option value="em_qualquer_posicao">Em qualquer posição</option>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               				<?php }?>	
		               			</select>
							</div>
						
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cpf_fornecedor') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['valor_filtro']->value,'###.###.###-##');?>
" id="valor_filtrar_fornecedor">	
								<?php }?>

								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cnpj_fornecedor') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['valor_filtro']->value,'###.###.###-##');?>
" id="valor_filtrar_fornecedor">
								<?php }?>

								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value!='cnpj_fornecedor'&&$_smarty_tpl->tpl_vars['campo_filtro']->value!='cpf_fornecedor') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value='<?php echo $_smarty_tpl->tpl_vars['valor_filtro']->value;?>
' id="valor_filtrar_fornecedor">	
								<?php }?>
									<span class='alerta_formulario' id='alerta_valor_filtrar_fornecedor'></span>
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>
			<!-- filtro -->

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Nome/Nome Fantasia</th>
					<th>Telefone</th>
					<th>Tipo</th>
					<th>Cpf/CNPJ</th>
					<th>Editar</th>
					<th>Desativar</th>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_fornecedor']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_fornecedor']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
									<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
								<?php } else { ?>
									<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_fantasia_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>		
								<?php }?>
								<td><?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'(##)####-#####');?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['tipo_pessoa_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
									<td><?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'###.###.###.##');?>
</td>
								<?php } else { ?>
									<td><?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'##.###.###/####.##');?>
</td>
								<?php }?>
								<td>
								<?php if ($_smarty_tpl->tpl_vars['permissao_editar_fornecedor_conta']->value==='1') {?>
									<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
								<?php }?>
								</td>

								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/desativar/<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_fornecedor"><span class="glyphicon glyphicon-trash"></span></a>
									<?php }?>
								</td>
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
