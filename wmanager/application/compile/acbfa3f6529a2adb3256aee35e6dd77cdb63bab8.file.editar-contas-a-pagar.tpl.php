<?php /* Smarty version Smarty-3.1.16, created on 2014-07-31 21:32:14
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/editar-contas-a-pagar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152464883653c66b880ff407-26946441%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acbfa3f6529a2adb3256aee35e6dd77cdb63bab8' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/editar-contas-a-pagar.tpl',
      1 => 1406568465,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152464883653c66b880ff407-26946441',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53c66b8819d1d5_14130334',
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_contas_a_pagar' => 0,
    'status_pagamento_contas_a_pagar' => 0,
    'data_pagamento_contas_a_pagar' => 0,
    'codigo_fornecedor' => 0,
    'nome_fornecedor' => 0,
    'nome_fantasia_fornecedor' => 0,
    'codigo_fornecedor_select' => 0,
    'nome_fornecedor_select' => 0,
    'nome_fantasia_fornecedor_select' => 0,
    'nota_fiscal_contas_a_pagar' => 0,
    'data_emissao_contas_a_pagar' => 0,
    'codigo_banco' => 0,
    'nome_banco' => 0,
    'codigo_banco_select' => 0,
    'nome_banco_select' => 0,
    'codigo_centro_de_custo' => 0,
    'nome_centro_de_custo' => 0,
    'codigo_centro_de_custo_select' => 0,
    'nome_centro_de_custo_select' => 0,
    'boleto_contas_a_pagar' => 0,
    'data_vencimento_contas_a_pagar' => 0,
    'forma_pagamento_contas_a_pagar' => 0,
    'numero_cheque_contas_a_pagar' => 0,
    'valor_contas_a_pagar' => 0,
    'juros_contas_a_pagar' => 0,
    'credito_contas_a_pagar' => 0,
    'valor_total_contas_a_pagar' => 0,
    'observacoes_contas_a_pagar' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53c66b8819d1d5_14130334')) {function content_53c66b8819d1d5_14130334($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar contas a pagar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/contas_a_pagar.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando contas a pagar</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/fazerEdicao" method="post" id="formulario_editar_contas_a_pagar">
		<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['codigo_contas_a_pagar']->value;?>
" id="codigo_editar_contas_a_pagar">	
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="background-color:#d1d1d1; padding:10px; margin-left:10px;">
								<label>Status pagamento</label>
						       	<select class="form-control" id="status_pagamento_editar_contas_a_pagar">
						           	<?php if ($_smarty_tpl->tpl_vars['status_pagamento_contas_a_pagar']->value==='Pago') {?>
						           		<option value="Pago">Pago</option>
						        		<option value="A pagar">A pagar</option>	
						        	<?php } else { ?>
										<option value="A pagar">A pagar</option>
						           		<option value="Pago">Pago</option>
						           	<?php }?>
						        </select>
							</div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="background-color:#d1d1d1; padding:10px;">
						       	<label>Data pagamento</label>
						       	<?php if ($_smarty_tpl->tpl_vars['status_pagamento_contas_a_pagar']->value==='Pago') {?>
						       		<input type="text" class="form-control" id="data_pagamento_editar_contas_a_pagar" data-mascara-campo="data" maxlength="10" value="<?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value));?>
">
						       	<?php } else { ?>
						       		<input type="text" class="form-control" id="data_pagamento_editar_contas_a_pagar" data-mascara-campo="data" maxlength="10" disabled="yes">
						       	<?php }?>	
						        <span class='alerta_formulario' id='alerta_data_pagamento_editar_contas_a_pagar'></span>
						    </div>							
						</div>

						<br>

						<div class="row"> 	
						    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						       	<label>Fornecedor</label>
						       	<select class="form-control" id="codigo_fornecedor_editar_contas_a_pagar">
						           	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor']->value;?>
">
						           		<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value;?>

						           		<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor']->value;?>

						           	</option>
						           	<?php if ($_smarty_tpl->tpl_vars['codigo_fornecedor_select']->value!=='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_fornecedor_select']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                        	<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                        	<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                        </option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum fornecedor</option>
	                                <?php }?>    
						        </select>
						    </div>
						</div>
						
						<br>    

						<div class="row"> 	
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Nota Fiscal</label>
						       	<input type="text" class="form-control" id="nota_fiscal_editar_contas_a_pagar" maxlength="80" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['nota_fiscal_contas_a_pagar']->value;?>
">
						       	<span class='alerta_formulario' id='alerta_nota_fiscal_editar_contas_a_pagar'></span>
						    </div>

						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Data Emissão</label>
						       	<input type="text" class="form-control"  id="data_emissao_editar_contas_a_pagar" data-mascara-campo="data" maxlength="10" autocomplete="yes" value="<?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value));?>
">
						        <span class='alerta_formulario' id='alerta_data_emissao_editar_contas_a_pagar'></span>
						    </div>

						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Banco</label>
						       	<select class="form-control" id="codigo_banco_editar_contas_a_pagar">
						           	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_banco']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['nome_banco']->value;?>
</option>
						           	<?php if ($_smarty_tpl->tpl_vars['codigo_banco_select']->value!=='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_banco_select']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_banco_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_banco_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum banco</option>
	                                <?php }?>    
						        </select>
						    </div>
						
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Centro de custo</label>
						       	<select class="form-control" id="codigo_centro_de_custo_editar_contas_a_pagar">
				           			<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value;?>
</option>
				           			<?php if ($_smarty_tpl->tpl_vars['codigo_centro_de_custo_select']->value!='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_centro_de_custo_select']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_select']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum centro de custo</option>
	                                <?php }?>    
						        </select>
						    </div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Boleto</label>
						       	<input type="text" class="form-control" id="boleto_editar_contas_a_pagar" maxlength="80" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['boleto_contas_a_pagar']->value;?>
">
						       	<span class='alerta_formulario' id='alerta_boleto_editar_contas_a_pagar'></span>
						    </div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Data vencimento</label>
						       	<input type="text" class="form-control"  id="data_vencimento_editar_contas_a_pagar" data-mascara-campo="data" maxlength="10" autocomplete="yes" value="<?php echo date('d/m/Y',strtotime($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value));?>
">
						        <span class='alerta_formulario' id='alerta_data_vencimento_editar_contas_a_pagar'></span>
						    </div>
						
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Forma</label>
						       	<select class="form-control" id="forma_pagamento_editar_contas_a_pagar">
						           	<?php if ($_smarty_tpl->tpl_vars['forma_pagamento_contas_a_pagar']->value==='Dinheiro') {?>
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cartão">Cartão</option>
							           	<option value="Cheque">Cheque</option>
						        	<?php }?>

						        	<?php if ($_smarty_tpl->tpl_vars['forma_pagamento_contas_a_pagar']->value==='Cartão') {?>
							           	<option value="Cartão">Cartão</option>
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cheque">Cheque</option>
						        	<?php }?>

						        	<?php if ($_smarty_tpl->tpl_vars['forma_pagamento_contas_a_pagar']->value==='Cheque') {?>
							           	<option value="Cheque">Cheque</option>
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cartão">Cartão</option>
						        	<?php }?>
						        </select>
						    </div>
													
							<?php if ($_smarty_tpl->tpl_vars['forma_pagamento_contas_a_pagar']->value==='Cheque') {?>
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							       	<div id="forma_pagamento_cheque">
							       		<label>Número do cheque</label>
					            		<input type="text" class="form-control" id="numero_cheque_editar_contas_a_pagar" value="<?php echo $_smarty_tpl->tpl_vars['numero_cheque_contas_a_pagar']->value;?>
">
					            		<span class='alerta_formulario' id='alerta_numero_cheque_editar_contas_a_pagar'></span>
							    	</div>
							   </div>
							<?php }?>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<div id="forma_pagamento_cheque" style="display:none;">
						       		<label>Número cheque</label>
				            		<input type="text" class="form-control" id="numero_cheque_editar_contas_a_pagar" value="<?php echo $_smarty_tpl->tpl_vars['numero_cheque_contas_a_pagar']->value;?>
">
				            		<span class='alerta_formulario' id='alerta_numero_cheque_editar_contas_a_pagar'></span>
						    	</div>
						   </div>
						</div>

						<br>
					
						<div class="row">
					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						    	<label>Valor (R$)</label>
						    	<input type="text" placeholder="Digite o valor" id="valor_editar_contas_a_pagar" class="form-control" data-mascara-campo="moeda" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['valor_contas_a_pagar']->value;?>
">
						    	<span class='alerta_formulario' id='alerta_valor_editar_contas_a_pagar'></span>
						    </div>

					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					       		<label>Juros (R$)</label>
					       		<input type="text" id="juros_editar_contas_a_pagar" data-mascara-campo="moeda" maxlength="10" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['juros_contas_a_pagar']->value;?>
">
					       		<span class='alerta_endereco' id='alerta_juros_editar_contas_a_pagar'></span>
					    	</div>
					    
					    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       	<label>Crédito (R$)</label>
						       	<input type="text" placeholder="Digite o valor" id="credito_editar_contas_a_pagar" class="form-control" data-mascara-campo="moeda" value="<?php echo $_smarty_tpl->tpl_vars['credito_contas_a_pagar']->value;?>
">
						       	<span class='alerta_formulario' id='alerta_credito_editar_contas_a_pagar'></span>
						    </div>

						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						    	<label>Valor Total (R$)</label>
						    	<input type="text" placeholder="Digite o valor" id="valor_total_editar_contas_a_pagar" class="form-control" data-mascara-campo="moeda" maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value;?>
" disabled="yes">
						    	<span class='alerta_formulario' id='alerta_valor_total_editar_contas_a_pagar'></span>
						    </div>
						</div>    
						
						<br>

						<div class="row">    
						    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
						    	<label>Observações</label>
						    	<textarea id="observacoes_editar_contas_a_pagar" class="form-control" rows="3"><?php echo $_smarty_tpl->tpl_vars['observacoes_contas_a_pagar']->value;?>
</textarea>
						    	<span class='alerta_formulario' id='alerta_observacoes_editar_contas_a_pagar'></span>
						    </div>
						</div>                                                            
					
					</div>
				</div>
			<br>
			<input type="submit" value="Editar" class="btn btn-primary">
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
