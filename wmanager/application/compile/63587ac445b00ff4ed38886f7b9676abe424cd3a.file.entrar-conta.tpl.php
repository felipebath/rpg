<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 16:26:40
         compiled from "/opt/lampp/htdocs/rr/application/views/entrar-conta.tpl" */ ?>
<?php /*%%SmartyHeaderCode:39538265053fc9920e44e45-84614330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '63587ac445b00ff4ed38886f7b9676abe424cd3a' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/entrar-conta.tpl',
      1 => 1399923949,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '39538265053fc9920e44e45-84614330',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fc9920e6cbb2_53458358',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc9920e6cbb2_53458358')) {function content_53fc9920e6cbb2_53458358($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Entrar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
				<div class="panel panel-default">
					<header class="panel-heading">
						<h1 class="panel-title">Entrar no sistema</h1>
					</header>

					<div class="panel-body">
						<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/entrar" method="post" id="formulario_entrar_conta">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label>Usuário</label>
									<input type="text" id="usuario_entrar_conta" placeholder="Digite seu nome de usuário" class="form-control" autofocus="yes" autocomplete="yes" maxlength="">
									<span id="alerta_usuario_entrar_conta" class="alerta_formulario"></span>
								</div>
							</div>

							<br>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label>Senha</label>
									<input type="password" id="senha_entrar_conta" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
									<span id="alerta_senha_entrar_conta" class="alerta_formulario"></span>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<br>
									<input type="submit" class="btn btn-primary" value="Entrar">
								</div>
							</div>
						</form>		
					</div>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
