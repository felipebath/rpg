<?php /* Smarty version Smarty-3.1.16, created on 2014-07-18 21:37:13
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/listar-financeiro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16939140365390cf03a75781-13783649%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29ffcd970cdf114ccd669e01e9cdd4949f4b7539' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/listar-financeiro.tpl',
      1 => 1405712176,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16939140365390cf03a75781-13783649',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5390cf03b687c5_48041847',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_financeiro_conta' => 0,
    'codigo_cliente_filtro' => 0,
    'tipo_pessoa_cliente_filtro' => 0,
    'nome_cliente_filtro' => 0,
    'nome_fantasia_cliente_filtro' => 0,
    'codigo_fornecedor_filtro' => 0,
    'tipo_pessoa_fornecedor_filtro' => 0,
    'nome_fornecedor_filtro' => 0,
    'nome_fantasia_fornecedor_filtro' => 0,
    'permissao_excluir_financeiro_conta' => 0,
    'codigo_vencimento' => 0,
    'tipo_pessoa_cliente' => 0,
    'nome_cliente' => 0,
    'nome_fantasia_cliente' => 0,
    'tipo_pessoa_fornecedor' => 0,
    'nome_fornecedor' => 0,
    'nome_fantasia_fornecedor' => 0,
    'boleto_vencimento' => 0,
    'data_emissao_financeiro' => 0,
    'data_vencimento_vencimento' => 0,
    'valor_total_vencimento' => 0,
    'nome_centro_custo' => 0,
    'observacoes_vencimento' => 0,
    'permissao_editar_financeiro_conta' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5390cf03b687c5_48041847')) {function content_5390cf03b687c5_48041847($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Financeiro"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/financeiro.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Financeiro</h1>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_financeiro_conta']->value==='1') {?>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				<?php }?>
			</div>
		</header>

		<div class="table-responsive">
			
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/filtrar" id="formulario_filtrar_financeiro">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select id="operacao_filtro" name="operacao_filtro" class="form-control">
		               				<option value="Débito">Débito</option>
		               				<option value="Crédito">Crédito</option>
		               			</select>
							</div>

							<div id="operacao_credito_filtro" style="display:none;">
			        			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			            			<select class="form-control" id="codigo_cliente_filtro" name="codigo_cliente_filtro">
			                        	<option value="">Escolha</option>
			                        	<option value="Todos">Todos</option>
			                        	<?php if ($_smarty_tpl->tpl_vars['codigo_cliente_filtro']->value!=='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_cliente_filtro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <?php if (($_smarty_tpl->tpl_vars['tipo_pessoa_cliente_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física')) {?>
		                                        	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
		                                        		<?php echo $_smarty_tpl->tpl_vars['nome_cliente_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                        	</option>
		                                        <?php } else { ?>
		                                        	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">		    <?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                        	</option>
		                                        <?php }?>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum cliente</option>
		                                <?php }?>
			                        </select>
			                        <span class='alerta_formulario' id='alerta_codigo_cliente_filtro'></span>
			                    </div>
				            </div>	
				                
				            <div id="operacao_debito_filtro">
			            	    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			                    	<select class="form-control" id="codigo_fornecedor_filtro" name="codigo_fornecedor_filtro">
			                        	<option value="">Escolha</option>
			                        	<option value="Todos">Todos</option>
			                        	<?php if ($_smarty_tpl->tpl_vars['codigo_fornecedor_filtro']->value!=='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_fornecedor_filtro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <?php if (($_smarty_tpl->tpl_vars['tipo_pessoa_fornecedor_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física')) {?>
		                                        	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
		                                        		<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                        	</option>
		                                        <?php } else { ?>
		                                        	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">		    <?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                        	</option>
		                                        <?php }?>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum fornecedor</option>
		                                <?php }?>
		                            </select>
		                            <span class='alerta_formulario' id='alerta_codigo_fornecedor_filtro'></span>
			                    </div>
				            </div>

				            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select id='status_filtro' name='status_filtro' class="form-control">
		               				<option value="A pagar">A pagar</option>
		               				<option value="Pago">Pago</option>
		               			</select>
							</div>

							<div id="status_pago_filtro" style="display:none;">
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<select name='tipo_data_filtro' class="form-control">
			               				<option value="vencimento">Vencimento</option>
			               				<option value="pagamento">Pagamento</option>
			               			</select>
								</div>
							</div>	

							<div id="status_a_pagar_filtro">
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<select name='tipo_data_filtro' class="form-control" disabled="disabled">
			               				<option value="vencimento">Vencimento</option>
			               			</select>
								</div>
							</div>	

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<input type="text" class="form-control" id="data_inicio_filtro" name="data_inicio_filtro" placeholder="Data início" maxlength="10" data-mascara-campo='data' autocomplete="off">
								<span class='alerta_formulario' id='alerta_data_inicio_filtro'></span>
		               		</div>

		               		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<input type="text" class="form-control" id="data_final_filtro" name="data_final_filtro" placeholder="Data final" maxlength="10" data-mascara-campo='data' autocomplete="off">
								<span class='alerta_formulario' id='alerta_data_final_filtro'></span>
		               		</div>
						</div>
						
						<br>

						<div class="row">
							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Cliente/Fornecedor</th>
					<th>Boleto</th>
					<th>Emissão</th>
					<th>Vencimento</th>
					<th>Valor</th>
					<th>Obra</th>
					<th>Observações</th>
					<th>Editar</th>
					<!--<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
						<th>Ação</th>
					<?php }?>-->
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_vencimento']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_vencimento']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
																
								<td>
									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
										<?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	 

									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_fornecedor']->value==='Física') {?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	
								</td>
								
								<th><?php echo $_smarty_tpl->tpl_vars['boleto_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</th>
								
								<th>
									<?php if ($_smarty_tpl->tpl_vars['data_emissao_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
										<?php echo $_smarty_tpl->tpl_vars['data_emissao_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	
								</th>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['data_vencimento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
										<?php echo $_smarty_tpl->tpl_vars['data_vencimento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>
								</td>
								
								<td><?php echo $_smarty_tpl->tpl_vars['valor_total_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['nome_centro_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['observacoes_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/editarVencimento/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit" title="editar"></span></a>
									<?php }?>
								</td>

								<!--<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/inativar/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_financeiro"><span class="glyphicon glyphicon-trash" title="inativar"></span></a>
									<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/ativar/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_financeiro"><span class="glyphicon glyphicon-plus" title="ativar"></span></a>
									<?php }?>
								</td>-->
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>

			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
			
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
