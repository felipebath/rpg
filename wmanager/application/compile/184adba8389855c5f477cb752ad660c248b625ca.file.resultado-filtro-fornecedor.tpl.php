<?php /* Smarty version Smarty-3.1.16, created on 2014-08-29 21:45:53
         compiled from "/opt/lampp/htdocs/rr/application/views/resultado-filtro-fornecedor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12504207165400d6eb3314c8-48879865%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '184adba8389855c5f477cb752ad660c248b625ca' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/resultado-filtro-fornecedor.tpl',
      1 => 1409341552,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12504207165400d6eb3314c8-48879865',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5400d6eb3de8f9_01129224',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_fornecedor_conta' => 0,
    'campo_filtro' => 0,
    'parametro_filtro' => 0,
    'valor_filtro' => 0,
    'id' => 0,
    'nome' => 0,
    'email' => 0,
    'telefone1' => 0,
    'tipo_pessoa' => 0,
    'cpf' => 0,
    'cnpj' => 0,
    'status' => 0,
    'permissao_editar_fornecedor_conta' => 0,
    'permissao_excluir_fornecedor_conta' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5400d6eb3de8f9_01129224')) {function content_5400d6eb3de8f9_01129224($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/rr/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/rr/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/fornecedor.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
					<h1>Fornecedor</h1>
				</div>

				<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
					<div class='btn-group pull-right'>
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/" class="btn btn-primary" title="Listar todos">
							Listar todos
						</a>

						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_fornecedor_conta']->value==='1') {?>
							<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/cadastrar" class="btn btn-primary" title="Cadastrar">
								Cadastrar
							</a>
						<?php }?>	
					</div>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/filtrar" id="formulario_filtro">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' class="form-control">
           							
           							<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='status') {?>
										<option value="status">Status (Ativo, Inativo)</option>
				               			<option value="nome">Nome do fornecedor</option>
				               			<option value="cpf">CPF</option>
				               			<option value="tipo_pessoa">Tipo pessoa (Física ou Jurídica)</option>
				               			<option value="razao_social">Razão social</option>
				               			<option value="cnpj">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='nome') {?>
										<option value="nome">Nome do fornecedor</option>
		               					<option value="status">Status (Ativo, Inativo)</option>
		               					<option value="cpf">CPF</option>
		               					<option value="tipo_pessoa">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social">Razão social</option>
		               					<option value="cnpj">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cpf') {?>
										<option value="cpf">CPF</option>
		               					<option value="status">Status (Ativo, Inativo)</option>
		               					<option value="nome">Nome do fornecedor</option>
		               					<option value="tipo_pessoa">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social">Razão social</option>
		               					<option value="cnpj">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='tipo_pessoa') {?>
										<option value="tipo_pessoa">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="status">Status (Ativo, Inativo ou Bloqueado)</option>
		               					<option value="nome">Nome do fornecedor</option>
		               					<option value="cpf">CPF</option>
		               					<option value="razao_social">Razão social</option>
		               					<option value="cnpj">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='razao_social') {?>
										<option value="razao_social">Razão social</option>
		               					<option value="status">Status (Ativo, Inativo)</option>
		               					<option value="nome">Nome do fornecedor</option>
		               					<option value="cpf">CPF</option>
		               					<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="cnpj_fornecedor">CNPJ</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cnpj') {?>
										<option value="cnpj">CNPJ</option>
										<option value="status">Status (Ativo, Inativo)</option>
		               					<option value="nome">Nome do fornecedor</option>
		               					<option value="cpf">CPF</option>
		               					<option value="tipo_pessoa">Tipo pessoa (Física ou Jurídica)</option>
		               					<option value="razao_social">Razão social</option>
		               				<?php }?>

								</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='iniciado_por') {?>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='igual') {?>
										<option value="igual">Igual</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='terminado_em') {?>
										<option value="terminado_em">Terminado em</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='em_qualquer_posicao') {?>
										<option value="em_qualquer_posicao">Em qualquer posição</option>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               				<?php }?>	
		               			</select>
							</div>
						
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cpf') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['valor_filtro']->value,'###.###.###-##');?>
" >	
								<?php }?>

								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cnpj') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['valor_filtro']->value,'###.###.###-##');?>
" >
								<?php }?>

								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value!='cnpj'&&$_smarty_tpl->tpl_vars['campo_filtro']->value!='cpf') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value='<?php echo $_smarty_tpl->tpl_vars['valor_filtro']->value;?>
'>	
								<?php }?>
									<span class='alerta_formulario' name='alerta_valor_filtro'></span>
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Nome</th>
					<th>E-Mail</th>
					<th>Telefone</th>
					<th>Tipo</th>
					<th>CPF/CNPJ</th>
					<th>Status</th>
					<th>Editar</th>
					<th>Ação</th>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['id']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
								
								<td><?php echo $_smarty_tpl->tpl_vars['email']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['telefone1']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!=='') {?>
										<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone1']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'(##) ####-#####');?>

									<?php }?>	
								</td>
								
								<td><?php echo $_smarty_tpl->tpl_vars['tipo_pessoa']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
										<td>
											<?php if ($_smarty_tpl->tpl_vars['cpf']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!=='') {?>
												<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'###.###.###.##');?>

											<?php }?>	
										</td>
									<?php } else { ?>
										<td>
											<?php if ($_smarty_tpl->tpl_vars['cnpj']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!=='') {?>
												<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'##.###.###/####.##');?>

											<?php }?>
										</td>	
									<?php }?>

								<td><?php echo $_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td>
								<?php if ($_smarty_tpl->tpl_vars['permissao_editar_fornecedor_conta']->value==='1') {?>
									<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/editar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
								<?php }?>
								</td>

								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Ativo') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/inativar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_fornecedor" title="inativar"><span class="glyphicon glyphicon-trash"></span></a>
									<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Inativo') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/ativar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_fornecedor" title="ativar"><span class="glyphicon glyphicon-plus"></span></a>

									<?php }?>
								</td>
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
