<?php /* Smarty version Smarty-3.1.16, created on 2014-07-03 16:50:25
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/listar-clientes.tpl" */ ?>
<?php /*%%SmartyHeaderCode:560501008537f587ba63f22-86286337%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '962ad449158d5b62ccc9e6ad82d82f3648afae89' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/listar-clientes.tpl',
      1 => 1402925592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '560501008537f587ba63f22-86286337',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537f587baf0769_11997046',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_cliente_conta' => 0,
    'permissao_excluir_cliente_conta' => 0,
    'codigo_cliente' => 0,
    'tipo_pessoa_cliente' => 0,
    'nome_cliente' => 0,
    'nome_fantasia_cliente' => 0,
    'telefone_cliente' => 0,
    'cpf_cliente' => 0,
    'cnpj_cliente' => 0,
    'razao_social_cliente' => 0,
    'email_cliente' => 0,
    'status_cliente' => 0,
    'permissao_editar_cliente_conta' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537f587baf0769_11997046')) {function content_537f587baf0769_11997046($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Clientes"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/cliente.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Clientes</h1>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_cliente_conta']->value==='1') {?>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				<?php }?>
			</div>
		</header>

		<div class="table-responsive">
			
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/filtrar" id="formulario_filtrar_cliente">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' id="campo_filtrar_cliente" class="form-control">
		               				<option value="status_cliente">Status (Ativo, Inativo ou Bloqueado)</option>
		               				<option value="nome_cliente">Nome do cliente</option>
		               				<option value="cpf_cliente">CPF</option>
		               				<option value="nome_fantasia_cliente">Nome fantasia</option>
		               				<option value="tipo_pessoa_cliente">Tipo pessoa (Física ou Jurídica)</option>
		               				<option value="razao_social_cliente">Razão social</option>
		               				<option value="cnpj_cliente">CNPJ</option>
		               			</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<option value="iniciado_por">Iniciado por</option>
		               				<option value="igual">Igual</option>
		               				<option value="terminado_em">Terminado em</option>
		               				<option value="em_qualquer_posicao">Em qualquer posição</option>
		               			</select>
							</div>
						
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<input type="search" name="valor_filtro" id="valor_filtrar_cliente" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autofocus="yes" autocomplete="yes">
								<span class='alerta_formulario' id='alerta_valor_filtrar_cliente'></span>
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Nome/Fantasia</th>
					<th>Telefone</th>
					<th>CPF/CNPJ</th>
					<th>Razão Social</th>
					<th>E-mail</th>
					<th>Tipo</th>
					<th>Status</th>
					<th>Editar</th>
					<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_cliente_conta']->value==='1') {?>
						<th>Ação</th>
					<?php }?>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_cliente']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_cliente']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['codigo_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
										<?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
	
									<?php }?>
								</td>	

								<td><?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'(##) ####-#####');?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
										<?php if ($_smarty_tpl->tpl_vars['cpf_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
											<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'###.###.###-##');?>

										<?php }?>
									<?php } else { ?>
										<?php if ($_smarty_tpl->tpl_vars['cnpj_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
											<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'##.###.###/####-##');?>
	
										<?php }?>
									<?php }?>
								</td>	
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Jurídica') {?>
										<?php echo $_smarty_tpl->tpl_vars['razao_social_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>
								</td>	

								<td><?php echo $_smarty_tpl->tpl_vars['email_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>						
								
								<td><?php echo $_smarty_tpl->tpl_vars['status_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_cliente_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit" title="editar"></span></a>
									<?php }?>
								</td>

								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_cliente_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Ativo') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/inativar/<?php echo $_smarty_tpl->tpl_vars['codigo_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_cliente"><span class="glyphicon glyphicon-trash" title="inativar"></span></a>
									<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_cliente_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Inativo') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/ativar/<?php echo $_smarty_tpl->tpl_vars['codigo_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_cliente"><span class="glyphicon glyphicon-plus" title="ativar"></span></a>
									<?php }?>
								</td>
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
