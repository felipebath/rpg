<?php /* Smarty version Smarty-3.1.16, created on 2014-09-15 18:42:36
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-cliente.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19321228665411af45cccc24-55207401%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b8116005d17710e2e81af9f5fe53f90fd17239ad' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-cliente.tpl',
      1 => 1410794468,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19321228665411af45cccc24-55207401',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5411af45cf8a49_91731240',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5411af45cf8a49_91731240')) {function content_5411af45cf8a49_91731240($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar cliente"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/cliente.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo cliente</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/fazerCadastro" method="post" id="formulario">
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Tipo pessoa</label>
                        	<select class="form-control" id="tipo_pessoa" autofocus="yes">
                            	<option value="Física">Física</option>
                            	<option value="Jurídica">Jurídica</option>
                            </select>
                        </div>
                    
                    	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                           	<label>Nome</label>
                           	<input type="text" placeholder="Nome" id="nome" class="form-control">
                        </div>
						
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<label>Status</label>
                        	<select class="form-control" id="status">
                            	<option value="Ativo">Ativo</option>
                            	<option value="Inativo">Inativo</option>	                              
                            </select>
                        </div>
                    </div>    

                    <br>

                   	<div id="pessoa_juridica" style="display:none;">
                		<div class="row"> 	
                   			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	<label>Razão Social</label>
	                            <input type="text" placeholder="Razão social" class="form-control" id="razao_social">
	                        </div>

                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        	    <label>Inscriçao Estadual</label>
                                <input type="text" placeholder="Inscriçao Estadual" class="form-control" id="inscricao_estadual">
                            </div>
                        	     
                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           	    <label>CNPJ</label>
	                            <input type="text" placeholder="CNPJ" class="form-control" id="cnpj" data-mascara-campo="cnpj" maxlength="18">
	                        </div>
	                	</div>
                    </div>	
                        
                    <div id="pessoa_fisica">
                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	<label>CPF</label>
                            	<input type="text" id="cpf" placeholder="Digite o número do cpf" class="form-control" data-mascara-campo="cpf" maxlength="14">
                            </div>
                        </div>    
                    </div>	

                    <br>

                    <div>
                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	<label>Endereço</label>
                            	<input type="text" id="endereco" placeholder="Digite o endereço" class="form-control">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Complemento</label>
                            	<input type="text" placeholder="Digite o complemento" id="complemento" class="form-control" maxlength="30">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Bairro</label>
                            	<input type="text" placeholder="Digite o bairro" id="bairro" class="form-control" maxlength="30">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>CEP</label>
                            	<input type="text" placeholder="Digite o Cep" id="cep" class="form-control" data-mascara-campo='cep' maxlength="9">
                            </div>
                    	</div>

                    	<br>

                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
								<label>Estado</label>
								<select class="form-control" id="estado">
									<option value="AC">AC</option>
									<option value="AL">AL</option>
									<option value="AM">AM</option>
									<option value="AP">AP</option>
									<option value="BA">BA</option>
									<option value="CE">CE</option>
									<option value="ES">ES</option>
									<option value="GO">GO</option>
									<option value="MA">MA</option>
									<option value="MG">MG</option>
									<option value="MS">MS</option>
									<option value="MT">MT</option>
									<option value="PA">PA</option>
									<option value="PB">PB</option>
									<option value="PE">PE</option>
									<option value="PI">PI</option>
									<option value="PR">PR</option>
									<option value="RJ" selected="selected">RJ</option>
									<option value="RN">RN</option>
									<option value="RO">RO</option>
									<option value="RR">RR</option>
									<option value="RS">RS</option>
									<option value="SC">SC</option>
									<option value="SE">SE</option>
									<option value="SP">SP</option>
									<option value="TO">TO</option>
								 </select>
							</div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Cidade</label>
                            	<input type="text" placeholder="Digite a Cidade" id="cidade" class="form-control">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Telefone</label>
                            	<input type="text" placeholder="Digite o Telefone" id="telefone" class="form-control" data-mascara-campo='telefone' maxlength="14">
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Celular</label>
                            	<input type="text" placeholder="Digite o Telefone" id="celular" class="form-control" data-mascara-campo='celular' maxlength="15">	                  
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            	<label>Fax</label>
                            	<input type="text" placeholder="Digite o Telefone" id="fax" class="form-control" data-mascara-campo='celular' maxlength="15">	                  
                            </div>
                        </div>

                        <br>

                    	<div class="row"> 	
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            	<label>E-mail</label>
                            	<input type="text" placeholder="Digite o e-mail" id="email" class="form-control" maxlength="80">
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label>Observações</label>
	                            <textarea id="observacoes" class="form-control"></textarea>
	                        </div>	
                        </div>
                	</div>                                                            
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
