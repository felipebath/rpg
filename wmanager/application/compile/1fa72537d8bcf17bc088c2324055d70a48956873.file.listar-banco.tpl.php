<?php /* Smarty version Smarty-3.1.16, created on 2014-08-13 15:24:03
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/listar-banco.tpl" */ ?>
<?php /*%%SmartyHeaderCode:654563007537f5b82e85035-94444842%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1fa72537d8bcf17bc088c2324055d70a48956873' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/listar-banco.tpl',
      1 => 1407847342,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '654563007537f5b82e85035-94444842',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537f5b82ed9617_51495644',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_banco_conta' => 0,
    'codigo_banco' => 0,
    'nome_banco' => 0,
    'agencia_banco' => 0,
    'conta_banco' => 0,
    'operacao_banco' => 0,
    'status_banco' => 0,
    'permissao_editar_banco_conta' => 0,
    'permissao_excluir_banco_conta' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537f5b82ed9617_51495644')) {function content_537f5b82ed9617_51495644($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Banco"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/banco.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Banco</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_banco_conta']->value==='1') {?>									
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
					<?php }?>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/filtrar" id="formulario_filtrar_banco">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' id="campo_filtrar_banco" class="form-control">
		               				<option value="status_banco">Status (Ativo, Inativo)</option>
		               				<option value="nome_banco">Nome do banco</option>
		               				<option value="operacao_banco">Operação (Física, Jurídica, Poupança)</option>
		               				<option value="agencia_banco">Agência</option>
		               				<option value="conta_banco">Conta</option>
		               			</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<option value="iniciado_por">Iniciado por</option>
		               				<option value="igual">Igual</option>
		               				<option value="terminado_em">Terminado em</option>
		               				<option value="em_qualquer_posicao">Em qualquer posição</option>
		               			</select>
							</div>
						
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<input type="search" name="valor_filtro" id="valor_filtrar_banco" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autofocus="yes" autocomplete="yes">
								<span class='alerta_formulario' id='alerta_valor_filtrar_banco'></span>
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>
			<br>
		<div class="table-responsive">
			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Banco</th>
					<th>Agência</th>
					<th>Conta</th>
					<th>Operação</th>
					<th>Status</th>
					<th>Editar</th>
					<th>Ação</th>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_banco']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_banco']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['codigo_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
								
								<td><?php echo $_smarty_tpl->tpl_vars['agencia_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['conta_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['operacao_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['status_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_banco_conta']->value==='1') {?>									
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
									<?php }?>	
								</td>
								<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_banco_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Ativo') {?>
									<td>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/desativar/<?php echo $_smarty_tpl->tpl_vars['codigo_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_banco"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_banco_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Inativo') {?>
									<td>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/ativar/<?php echo $_smarty_tpl->tpl_vars['codigo_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_banco"><span class="glyphicon glyphicon-plus"></span></a>
									</td>
								<?php }?>	
							</tr>
						<?php endfor; endif; ?>
					<?php }?>
				</tbody>
			</table>
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
