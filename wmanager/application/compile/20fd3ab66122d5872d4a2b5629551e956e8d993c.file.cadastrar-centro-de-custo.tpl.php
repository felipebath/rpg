<?php /* Smarty version Smarty-3.1.16, created on 2014-05-22 16:32:41
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-centro-de-custo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2072624866537caa5ceab689-31944423%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '20fd3ab66122d5872d4a2b5629551e956e8d993c' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-centro-de-custo.tpl',
      1 => 1400769159,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2072624866537caa5ceab689-31944423',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537caa5cefe340_37696377',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537caa5cefe340_37696377')) {function content_537caa5cefe340_37696377($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar centro de custo"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/centro_de_custo.js" defer></script>

	<section class="container-fluid">
	<header class="page-header">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<h1>Cadastrando novo centro de custo</h1>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
			</div>
		</div>
	</header>

	<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/fazerCadastro" method="post" id="formulario_cadastrar_centro_de_custo">

	<div class="tab-content">
		<div class="tab-pane active" id="cadastro">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Status</label>
	                     	<select class="form-control" id="status_cadastrar_centro_de_custo">
	                           	<option value="Ativo">Ativo</option>
	                           	<option value="Inativo">Inativo</option>
	                           	<option value="Bloqueado">Bloqueado</option>	                              
	                       	</select>
	             		</div>
	          
	                     <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                      	<label>Nome</label>
	                      	<input type="text" placeholder="Digite o nome" id="nome_cadastrar_centro_de_custo" autocomplete="yes" class="form-control" autofocus="yes">
	                      	<span class='alerta_formulario' id='alerta_nome_cadastrar_centro_de_custo'></span>
	                    </div>
	                </div>	

		            <br>

		            <div class="row"> 	
		                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                   	<label>Endereço</label>
		                   	<input type="text" id="endereco_cadastrar_centro_de_custo" placeholder="Digite o endereço" class="form-control"  autocomplete="yes">
		                </div>

		                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                   	<label>Bairro</label>
		                   	<input type="text" id="bairro_cadastrar_centro_de_custo" placeholder="Digite o bairro" class="form-control"  autocomplete="yes">
		                </div>

		                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                   	<label>CEP</label>
		                   	<input type="text" placeholder="Digite o Cep" id="cep_cadastrar_centro_de_custo" autocomplete="yes" class="form-control" autofocus="yes" data-mascara-campo='cep' maxlength="9">
		                </div>

		                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                   	<label>Cidade</label>
		                   	<input type="text" placeholder="Digite a Cidade" id="cidade_cadastrar_centro_de_custo" autocomplete="yes" class="form-control" autofocus="yes">
		                </div>
		                  
		                <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
							<label>Estado</label>
							<select class="form-control" id="estado_cadastrar_centro_de_custo">
								<option value="AC">AC</option>
								<option value="AL">AL</option>
								<option value="AM">AM</option>
								<option value="AP">AP</option>
								<option value="BA">BA</option>
								<option value="CE">CE</option>
								<option value="ES">ES</option>
								<option value="GO">GO</option>
								<option value="MA">MA</option>
								<option value="MG">MG</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="PA">PA</option>
								<option value="PB">PB</option>
								<option value="PE">PE</option>
								<option value="PI">PI</option>
								<option value="PR">PR</option>
								<option value="RJ" selected="selected">RJ</option>
								<option value="RN">RN</option>
								<option value="RO">RO</option>
								<option value="RR">RR</option>
								<option value="RS">RS</option>
								<option value="SC">SC</option>
								<option value="SE">SE</option>
								<option value="SP">SP</option>
								<option value="TO">TO</option>
							 </select>
						</div>
					</div>	

					<br>
						
					<div class="row"> 		
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                   	<label>Responsável</label>
		                   	<input type="text" placeholder="Digite o nome do responsável" id="responsavel_cadastrar_centro_de_custo" autocomplete="yes" class="form-control" autofocus="yes">
		                </div>

		                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
		                  	<label>Observações</label>
		                   	<textarea id="observacoes_cadastrar_centro_de_custo" autocomplete="yes" class="form-control" autofocus="yes"></textarea>
		                </div>
		        	</div>				
				
				</div>
			</div>
		</div>
	</div>

	<br>
	<input type="submit" value="Cadastrar" class="btn btn-primary">

	</form>
	</section>
	<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
