<?php /* Smarty version Smarty-3.1.16, created on 2014-09-19 18:55:38
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1553395215541193dbc256f1-09852679%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b9eb6c2dea41e7b3b0151e89fe9a75399d2dce2' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-produto.tpl',
      1 => 1411145684,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1553395215541193dbc256f1-09852679',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_541193dbc6df44_30163288',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'status' => 0,
    'nome' => 0,
    'preco_de_venda' => 0,
    'preco_de_custo' => 0,
    'quantidade' => 0,
    'id_cargo' => 0,
    'nome_categoria' => 0,
    'id_categoria_list' => 0,
    'nome_categoria_list' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541193dbc6df44_30163288')) {function content_541193dbc6df44_30163288($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar produto"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/produto.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando produto</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">	
			<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
                            	<select class="form-control" id="status">
                                	<?php if ($_smarty_tpl->tpl_vars['status']->value==='Ativo') {?>
                                		<option value="Ativo">Ativo</option>
                                		<option value="Inativo">Inativo</option>
                                	<?php } else { ?>
                                		<option value="Inativo">Inativo</option>
                                		<option value="Ativo">Ativo</option>
                                	<?php }?>
                                </select>
	                        </div>
	                    
	                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                    		</div>
                    
                    		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        	    <label>Preço de venda</label>
                                <input type="text" class="form-control" id="preco_de_venda" maxlength="15" data-mascara-campo="moeda" value="<?php echo $_smarty_tpl->tpl_vars['preco_de_venda']->value;?>
">
	                        </div>
                        		
                        	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Preço de custo</label>
	                            <input type="text" class="form-control" id="preco_de_custo" maxlength="15" data-mascara-campo="moeda" value="<?php echo $_smarty_tpl->tpl_vars['preco_de_custo']->value;?>
">
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Quantidade</label>
	                            <input type="text" class="form-control" id="quantidade" maxlength="6" value="<?php echo $_smarty_tpl->tpl_vars['quantidade']->value;?>
">
	                        </div>
	                    </div>    

	                    <br>

	                    <div class="row">
	                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Categoria</label>
                            	<select class="form-control" id="categoria">
                                	
                                	<option value="<?php echo $_smarty_tpl->tpl_vars['id_cargo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['nome_categoria']->value;?>
</option>
	                                <?php if ($_smarty_tpl->tpl_vars['id_categoria_list']->value!=='0') {?>
	                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_categoria_list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_categoria_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                            <?php echo $_smarty_tpl->tpl_vars['nome_categoria_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                        </option>
	                                    <?php endfor; endif; ?>
	                                <?php } else { ?>
	                                    <option value="">Não foi possível localizar nenhum cargo</option>
	                                <?php }?>

                                		
                                
                                </select>
	                        </div>

							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Observações</label>
	                            <textarea id="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
	                        </div>	
	                	</div>                                                            
					</div>
				</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
