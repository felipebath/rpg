<?php /* Smarty version Smarty-3.1.16, created on 2014-09-19 17:19:23
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-categoria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:494749185541c497be2c357-30131099%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd5b40bdf40692477477439fba4f5ad63de88013' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-categoria.tpl',
      1 => 1411139312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '494749185541c497be2c357-30131099',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_541c497be58b39_61862861',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541c497be58b39_61862861')) {function content_541c497be58b39_61862861($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar categoria"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/categoria.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo categoria</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
categoria/fazerCadastro" method="post" id="formulario">
			
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
                            	<select class="form-control" id="status">
                                	<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>	                             
                                </select>
	                        </div>
	                    
	                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes">
                    		</div>
                    
                    		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Observações</label>
	                            <textarea id="observacoes" class="form-control"></textarea>
	                        </div>	
	                	</div>                                                            
					</div>
				</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
