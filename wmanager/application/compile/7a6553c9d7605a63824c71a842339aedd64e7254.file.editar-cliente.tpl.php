<?php /* Smarty version Smarty-3.1.16, created on 2014-07-11 15:16:45
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/editar-cliente.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1412728261537f58893b0734-62016858%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a6553c9d7605a63824c71a842339aedd64e7254' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/editar-cliente.tpl',
      1 => 1402921522,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1412728261537f58893b0734-62016858',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537f5889429b87_84787743',
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_cliente' => 0,
    'status_cliente' => 0,
    'tipo_pessoa_cliente' => 0,
    'nome_fantasia_cliente' => 0,
    'razao_social_cliente' => 0,
    'cnpj_cliente' => 0,
    'nome_cliente' => 0,
    'cpf_cliente' => 0,
    'telefone_cliente' => 0,
    'endereco_cliente' => 0,
    'cidade_cliente' => 0,
    'cep_cliente' => 0,
    'email_cliente' => 0,
    'estado_cliente' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537f5889429b87_84787743')) {function content_537f5889429b87_84787743($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editando cliente"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/cliente.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando cliente</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/fazerEdicao" method="post" id="formulario_editar_cliente">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente']->value;?>
" id="codigo_editar_cliente">

		<div class="tab-content">
			<div class="tab-pane active" id="cadastro">
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
	                            	<select class="form-control" id="status_editar_cliente">
	                                	<?php if (($_smarty_tpl->tpl_vars['status_cliente']->value==='Ativo')) {?>
	                                		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>
		                                	<option value="Bloqueado">Bloqueado</option>
	                                	<?php }?>
	                                	<?php if (($_smarty_tpl->tpl_vars['status_cliente']->value==='Inativo')) {?>
	                                		<option value="Inativo">Inativo</option>
		                                	<option value="Ativo">Ativo</option>
		                                	<option value="Bloqueado">Bloqueado</option>
	                                	<?php }?>
	                                	<?php if (($_smarty_tpl->tpl_vars['status_cliente']->value==='Bloqueado')) {?>
	                                		<option value="Bloqueado">Bloqueado</option>
	                                		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>
		                                <?php }?>
	                                </select>
	                        </div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Tipo pessoa</label>
	                            	<select class="form-control" id="tipo_pessoa_editar_cliente">
	                                	<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value==='Física') {?>
		                                	<option value="Física">Física</option>
		                                	<option value="Jurídica">Jurídica</option>
		                                <?php }?>	
		                                <?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value==='Jurídica') {?>
		                                	<option value="Jurídica">Jurídica</option>
		                                	<option value="Física">Física</option>
		                                <?php }?>
									</select>
	                        </div>
	                    </div>

	                    <br>

                    	<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value==='Jurídica') {?>
	                       	<div id="pessoa_juridica">
	                    		<div class="row"> 	
	                       			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                        			<label>Nome fantasia</label>
	                                	<input type="text" placeholder="Nome Fantasia" class="form-control" id="nome_fantasia_editar_cliente" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente']->value;?>
">
	                                	<span class='alerta_formulario' id='alerta_nome_fantasia_editar_cliente'></span>
	                        		</div>
	                        
	                        		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	    <label>Razão Social</label>
			                                <input type="text" placeholder="Razão social" class="form-control" id="razao_social_editar_cliente" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['razao_social_cliente']->value;?>
">
			                                <span class='alerta_formulario' id='alerta_razao_social_editar_cliente'></span>
		                        	</div>
		                        		                        		
		                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                           	    <label>CNPJ</label>
			                            <?php if ($_smarty_tpl->tpl_vars['cnpj_cliente']->value!='') {?>
											<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj_editar_cliente" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj_cliente']->value,'##.###.###/####-##');?>
">
		                            	<?php } else { ?>
		                            		<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj_editar_cliente" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes">
		                            	<?php }?>
			                            <span class='alerta_formulario' id='alerta_cnpj_editar_cliente'></span>
			                        </div>
			                	</div>
		                    </div>
		                
		                	<div id="pessoa_fisica" style="display:none;">
		                    	<div class="row"> 	
	                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>Nome</label>
		                            	<input type="text" placeholder="Nome Completo" id="nome_editar_cliente" autocomplete="yes" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value;?>
">
		                            	<span class='alerta_formulario' id='alerta_nome_editar_cliente'></span>
		                            </div>
		                        	
		                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>CPF</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['cpf_cliente']->value!='') {?>
											<input type="text" id="cpf_editar_cliente" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf_cliente']->value,'###.###.###-##');?>
">
		                            	<?php } else { ?>
		                            		<input type="text" id="cpf_editar_cliente" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14">
		                            	<?php }?>
		                            	<span class='alerta_formulario' id='alerta_cpf_editar_cliente'></span>
		                            </div>
		                        </div>    
		                    </div>	
                    	<?php } else { ?>
	                       	<div id="pessoa_fisica">
		                    	<div class="row"> 	
	                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>Nome</label>
		                            	<input type="text" placeholder="Nome Completo" id="nome_editar_cliente" autocomplete="yes" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value;?>
">
		                            	<span class='alerta_formulario' id='alerta_nome_editar_cliente'></span>
		                            </div>
		                        	
		                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>CPF</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['cpf_cliente']->value!='') {?>
											<input type="text" id="cpf_editar_cliente" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf_cliente']->value,'###.###.###-##');?>
">
		                            	<?php } else { ?>
		                            		<input type="text" id="cpf_editar_cliente" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14">
		                            	<?php }?>
		                            	<span class='alerta_formulario' id='alerta_cpf_editar_cliente'></span>
		                            </div>
		                        </div>    
		                    </div>		
                    	
                    		<div id="pessoa_juridica" style="display:none;">
	                    		<div class="row"> 	
	                       			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                        			<label>Nome fantasia</label>
	                                	<input type="text" placeholder="Nome Fantasia" class="form-control" id="nome_fantasia_editar_cliente" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente']->value;?>
">
	                                	<span class='alerta_formulario' id='alerta_nome_fantasia_editar_cliente'></span>
	                        		</div>
	                        
	                        		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	    <label>Razão Social</label>
			                                <input type="text" placeholder="Razão social" class="form-control" id="razao_social_editar_cliente" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['razao_social_cliente']->value;?>
">
			                                <span class='alerta_formulario' id='alerta_razao_social_editar_cliente'></span>
		                        	</div>
		                        		
		                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                           	    <label>CNPJ</label>
			                            <?php if ($_smarty_tpl->tpl_vars['cnpj_cliente']->value!='') {?>
											<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj_editar_cliente" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj_cliente']->value,'##.###.###/####-##');?>
">
		                            	<?php } else { ?>
		                            		<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj_editar_cliente" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes">
		                            	<?php }?>
			                            <span class='alerta_formulario' id='alerta_cnpj_editar_cliente'></span>
			                        </div>
			                	</div>
		                    </div>
                    	<?php }?>
					</div>
				</div>
				<div>
	        	 <div>
	        	<div class="row"> 	
	        		<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente']->value;?>
" id="codigo_editar_cliente">
	                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	                	<label>Telefone</label>
	                	<input type="text" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone_cliente']->value,'(##)####-#####');?>
" placeholder="Digite o Telefone" id="telefone_editar_cliente" autocomplete="yes" class="form-control" autofocus="yes" data-mascara-campo='telefone' maxlength="15">
	                	<span class='alerta_formulario' id='alerta_telefone_editar_cliente'></span>
	                </div>
	            	
	            	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                	<label>Endereço</label>
	                	<input type="text" id="endereco_editar_cliente" placeholder="Digite o endereço" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['endereco_cliente']->value;?>
" autocomplete="yes">
	                	<span class='alerta_endereco' id='alerta_endereco_editar_cliente'></span>
	                </div>

	                 <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                	<label>Cidade</label>
	                	<input type="text" placeholder="Digite a Cidade" id="cidade_editar_cliente" value="<?php echo $_smarty_tpl->tpl_vars['cidade_cliente']->value;?>
" autocomplete="yes" class="form-control" autofocus="yes">
	                	<span class='alerta_formulario' id='alerta_cidade_editar_cliente'></span>
	                </div>
	            </div>
	        	<div class="row"> 		            
	                 <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	                	<label>CEP</label>
	                	<input type="text" placeholder="Digite o Cep" id="cep_editar_cliente" autocomplete="yes" class="form-control" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cep_cliente']->value,'#####-###');?>
" autofocus="yes" data-mascara-campo='cep' maxlength="9">
	                	<span class='alerta_formulario' id='alerta_cep_editar_cliente'></span>
	                </div>

	                 <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                	<label>E-mail</label>
	                	<input type="text" placeholder="Digite o E-mail" id="email_editar_cliente" autocomplete="yes" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['email_cliente']->value;?>
" autofocus="yes" maxlength="80">
	                	<span class='alerta_formulario' id='alerta_cep_editar_cliente'></span>
	                </div>
	            	
	            	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					<label>Estado</label>
					<select class="form-control" id="estado_editar_cliente">
						<option value="AC">AC</option>
						<option value="AL">AL</option>
						<option value="AM">AM</option>
						<option value="AP">AP</option>
						<option value="BA">BA</option>
						<option value="CE">CE</option>
						<option value="ES">ES</option>
						<option value="GO">GO</option>
						<option value="MA">MA</option>
						<option value="MG">MG</option>
						<option value="MS">MS</option>
						<option value="MT">MT</option>
						<option value="PA">PA</option>
						<option value="PB">PB</option>
						<option value="PE">PE</option>
						<option value="PI">PI</option>
						<option value="PR">PR</option>
						<option value="<?php echo $_smarty_tpl->tpl_vars['estado_cliente']->value;?>
" selected="selected"><?php echo $_smarty_tpl->tpl_vars['estado_cliente']->value;?>
</option>
						<option value="RJ">RJ</option>
						<option value="RN">RN</option>
						<option value="RO">RO</option>
						<option value="RR">RR</option>
						<option value="RS">RS</option>
						<option value="SC">SC</option>
						<option value="SE">SE</option>
						<option value="SP">SP</option>
						<option value="TO">TO</option>
					 </select>
				</div>
	        </div>				
		</div>
			</div> <!-- cadastro -->
		</div>

			<br>
			<input type="submit" value="Salvar alterações" class="btn btn-primary">

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
