<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 16:54:52
         compiled from "/opt/lampp/htdocs/rr/application/views/cadastrar-banco.tpl" */ ?>
<?php /*%%SmartyHeaderCode:156944441753fc9fbcb81e76-62664562%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bce2a563a4ba47b0c10dc1fe25b898f25e4a5bb9' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/cadastrar-banco.tpl',
      1 => 1402921522,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '156944441753fc9fbcb81e76-62664562',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fc9fbcbb2209_46564864',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc9fbcbb2209_46564864')) {function content_53fc9fbcbb2209_46564864($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar banco"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/banco.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo banco</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/fazerCadastro" method="post" id="formulario_cadastrar_banco">
			
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
	                            	<select class="form-control" id="status_cadastrar_banco">
	                                	<option value="Ativo">Ativo</option>
	                                	<option value="Inativo">Inativo</option>	                             
	                                </select>
	                        </div>
	                    </div>    

	                    <br>
                		<div class="row"> 	
                   			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome do Banco" class="form-control" id="nome_cadastrar_banco" maxlength="80" autofocus="yes" autocomplete="yes">
                            	<span class='alerta_formulario' id='alerta_nome_cadastrar_banco'></span>
                    		</div>
                    
                    		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	    <label>Agência</label>
	                                <input type="text" placeholder="Agência" class="form-control" id="agencia_cadastrar_banco" autocomplete="yes">
	                                <span class='alerta_formulario' id='alerta_agencia_cadastrar_banco'></span>
                        	</div>
                        
                        		
                        	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Conta</label>
	                            <input type="text" placeholder="Conta" class="form-control"  id="conta_cadastrar_banco" maxlength="11" autocomplete="yes">
	                            <span class='alerta_formulario' id='alerta_conta_cadastrar_banco'></span>
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Operacao</label>
	                            	<select class="form-control" id="operacao_cadastrar_banco">
	                                	<option value="Jurídico">Jurídico</option>
	                                	<option value="Físico">Físico</option>
	                                	<option value="Poupança">Poupança</option>
	                                </select>
	                        </div>
	                	</div>                                                            
					</div>
				</div>
			<br>
			<input type="submit" value="Cadastrar" class="btn btn-primary">
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
