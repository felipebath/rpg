<?php /* Smarty version Smarty-3.1.16, created on 2014-11-03 11:09:45
         compiled from "/opt/lampp/htdocs/wmanager/application/views/relatorio-contas-a-pagar2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:43821092754528a3e2a6ea2-51653928%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '77bb25efafb3d94ff7776330a05b8c613b782390' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/relatorio-contas-a-pagar2.tpl',
      1 => 1415020183,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '43821092754528a3e2a6ea2-51653928',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54528a3e39f133_03541370',
  'variables' => 
  array (
    'status_filtro' => 0,
    'base_url' => 0,
    'data_criacao' => 0,
    'hora_criacao' => 0,
    'id_contas_a_pagar' => 0,
    'fornecedor_checkbox' => 0,
    'nota_fiscal_checkbox' => 0,
    'data_emissao_checkbox' => 0,
    'banco_checkbox' => 0,
    'centro_de_custo_checkbox' => 0,
    'boleto_checkbox' => 0,
    'data_vencimento_checkbox' => 0,
    'status_checkbox' => 0,
    'data_pagamento_checkbox' => 0,
    'forma_checkbox' => 0,
    'numero_cheque_checkbox' => 0,
    'valor_checkbox' => 0,
    'juros_checkbox' => 0,
    'credito_checkbox' => 0,
    'valor_total_checkbox' => 0,
    'observacoes_checkbox' => 0,
    'data_vencimento_contas_a_pagar' => 0,
    'valor_total_contas_a_pagar' => 0,
    'nome_fornecedor' => 0,
    'nota_fiscal_contas_a_pagar' => 0,
    'data_emissao_contas_a_pagar' => 0,
    'nome_banco' => 0,
    'nome_centro_de_custo' => 0,
    'boleto_contas_a_pagar' => 0,
    'status_contas_a_pagar' => 0,
    'data_pagamento_contas_a_pagar' => 0,
    'forma_contas_a_pagar' => 0,
    'numero_cheque_contas_a_pagar' => 0,
    'valor_contas_a_pagar' => 0,
    'juros_contas_a_pagar' => 0,
    'credito_contas_a_pagar' => 0,
    'observacoes_contas_a_pagar' => 0,
    'colspan' => 0,
    'id_centro_de_custo' => 0,
    'subtotal_contas_a_pagar' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54528a3e39f133_03541370')) {function content_54528a3e39f133_03541370($_smarty_tpl) {?><!DOCTYPE html>
<html lang="pt-br">
<head>
<title>W Manager - Relatório - <?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</title>
<meta charset="UTF-8">
<link href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/css/relatorio.css" rel="stylesheet">
</head>

<body>
    <header>
        <table class="table-header">
            <tr>
                <td class="td1-table-header">
                    <?php if ($_smarty_tpl->tpl_vars['status_filtro']->value==='A pagar') {?>
                        <h1>RELATÓRIO DE CONTAS A PAGAR</h1>
                    <?php }?>
                    
                    <?php if ($_smarty_tpl->tpl_vars['status_filtro']->value==='Pago') {?>
                        <h1>RELATÓRIO DE CONTAS PAGAS</h1>
                    <?php }?>    
                </td>
                
                <td class="td2-table-header">
                    <span>Gerado em <b><?php echo $_smarty_tpl->tpl_vars['data_criacao']->value;?>
</b> as <?php echo $_smarty_tpl->tpl_vars['hora_criacao']->value;?>
 hs</span>
                </td>
            </tr>
        </table>
    </header>
        
    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    
    <br>

        <table>
            <tr class="tr-grid">
                <td><b>Cod</td>
                
                <?php if ($_smarty_tpl->tpl_vars['fornecedor_checkbox']->value==='on') {?>    
                    <td>Fornecedor</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['nota_fiscal_checkbox']->value==='on') {?>    
                    <td>Nota Fiscal</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['data_emissao_checkbox']->value==='on') {?>
                    <td>Emissão</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['banco_checkbox']->value==='on') {?>
                    <td>Banco</td>
                <?php }?>
                
                <?php if ($_smarty_tpl->tpl_vars['centro_de_custo_checkbox']->value==='on') {?>
                    <td>Centro de Custo</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['boleto_checkbox']->value==='on') {?>    
                    <td>Boleto</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['data_vencimento_checkbox']->value==='on') {?>
                    <td>Vencimento</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['status_checkbox']->value==='on') {?>
                    <td>Status</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['data_pagamento_checkbox']->value==='on') {?>
                    <td>Pagamento</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['forma_checkbox']->value==='on') {?>
                    <td>Forma de Pgto.</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['numero_cheque_checkbox']->value==='on') {?>
                    <td>Cheque</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['valor_checkbox']->value==='on') {?>
                    <td>Valor</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['juros_checkbox']->value==='on') {?>
                    <td>Juros</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['credito_checkbox']->value==='on') {?>
                    <td>Crédito</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['valor_total_checkbox']->value==='on') {?>
                    <td>Valor Total</td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['observacoes_checkbox']->value==='on') {?>
                    <td>Observações</td>
                <?php }?>
            </tr>

            <tr>
                <?php if ($_smarty_tpl->tpl_vars['id_contas_a_pagar']->value!=='0') {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                        
                        <?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='01/01/1970') {?>
                            <?php if ($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='0,00') {?>

                                <tr>
                                    <td class="center">
                                        <?php echo $_smarty_tpl->tpl_vars['id_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                    </td>    
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['fornecedor_checkbox']->value==='on') {?>
                                        <td class="left">
                                            <?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['nota_fiscal_checkbox']->value==='on') {?>    
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['nota_fiscal_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['data_emissao_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php if ($_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='00/00/0000') {?>
                                                <?php echo $_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                            <?php }?>   
                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['banco_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <div align="left">
                                                <?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                            </div>
                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['centro_de_custo_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>    

                                    <?php if ($_smarty_tpl->tpl_vars['boleto_checkbox']->value==='on') {?>    
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['boleto_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['data_vencimento_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='00/00/0000') {?>
                                                <?php echo $_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                            <?php }?>
                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['status_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['status_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['data_pagamento_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php if ($_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='00/00/0000') {?>
                                                <?php echo $_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                            <?php }?>   
                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['forma_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['forma_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['numero_cheque_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['numero_cheque_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>   

                                    <?php if ($_smarty_tpl->tpl_vars['valor_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['valor_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['juros_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['juros_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['credito_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['credito_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['valor_total_checkbox']->value==='on') {?>
                                        <td class="center">
                                            <?php echo $_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['observacoes_checkbox']->value==='on') {?>
                                        <td class="left">
                                            <?php echo $_smarty_tpl->tpl_vars['observacoes_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                        </td>
                                    <?php }?>
                                </tr>
                            <?php }?>
                        <?php }?>
                        
                        <?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='01/01/1970') {?> 
                            <?php if ($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]=='0,00') {?>
                                <tr>
                                    <td class="right" colspan="<?php echo $_smarty_tpl->tpl_vars['colspan']->value;?>
">
                                        <?php if ($_smarty_tpl->tpl_vars['id_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
                                            <b>SUBTOTAL</b>
                                        <?php } else { ?>
                                            <b>TOTAL GERAL</b>
                                        <?php }?>
                                        <b><?php echo $_smarty_tpl->tpl_vars['subtotal_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</b>
                                    </td>
                                </tr>            
                                        
                                <?php if ($_smarty_tpl->tpl_vars['id_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
                                    <tr>
                                        <td colspan="<?php echo $_smarty_tpl->tpl_vars['colspan']->value;?>
">&nbsp;</td> 
                                    </tr>
                                <?php }?>    
                            <?php }?>
                        <?php }?>         

                    <?php endfor; endif; ?>
                <?php } else { ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
    <?php endfor; endif; ?>

</body>
</html>
<?php }} ?>
