<?php /* Smarty version Smarty-3.1.16, created on 2016-01-18 17:33:30
         compiled from "/opt/lampp/htdocs/wmanager/application/views/listar-personagens.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2057124241569d02f9ebce05-49064452%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15febbb4893aa0b1e3c877e99861cd1c52440a56' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/listar-personagens.tpl',
      1 => 1453134791,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2057124241569d02f9ebce05-49064452',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_569d02f9f41ba7_12084356',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'nome' => 0,
    'arma' => 0,
    'ataque' => 0,
    'vida' => 0,
    'agilidade' => 0,
    'forca' => 0,
    'defesa' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569d02f9f41ba7_12084356')) {function content_569d02f9f41ba7_12084356($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Personagem"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/personagem.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Personagem</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">
						Novo
					</a>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			
			<br>
		
			<div class="table-responsive">
				<table class='table table-hover table-striped'>
					<thead>
						<th>Id</th>
						<th>Nome</th>
						<th>Arma</th>
						<th>Ataque</th>
						<th>Vida</th>
						<th>Agilidade</th>
						<th>Força</th>
						<th>Defesa</th>
						<th>Editar</th>
						<th>Excluir</th>
					</thead>

					<tbody>
						<?php if ($_smarty_tpl->tpl_vars['id']->value!=='0') {?>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
									
									<td><?php echo $_smarty_tpl->tpl_vars['arma']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['ataque']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['vida']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['agilidade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['forca']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['defesa']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<td>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem/editar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" title="editar">
											<span class="glyphicon glyphicon glyphicon-edit"></span>
										</a>
									</td>
									
									<td>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem/fazerExclusao/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="excluir" title="excluir">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</td>
								</tr>
							<?php endfor; endif; ?>
						<?php }?>
					</tbody>
				</table>
				
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
