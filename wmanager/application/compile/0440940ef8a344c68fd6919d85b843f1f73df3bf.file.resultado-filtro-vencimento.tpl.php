<?php /* Smarty version Smarty-3.1.16, created on 2014-07-11 15:06:45
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-vencimento.tpl" */ ?>
<?php /*%%SmartyHeaderCode:47817187753a17f6a981e04-46293316%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0440940ef8a344c68fd6919d85b843f1f73df3bf' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-vencimento.tpl',
      1 => 1405084003,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '47817187753a17f6a981e04-46293316',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53a17f6aa215c1_80007479',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_financeiro_conta' => 0,
    'data_inicio_filtro' => 0,
    'data_final_filtro' => 0,
    'operacao_filtro' => 0,
    'status_filtro' => 0,
    'permissao_excluir_financeiro_conta' => 0,
    'codigo_vencimento' => 0,
    'tipo_pessoa_cliente' => 0,
    'nome_cliente' => 0,
    'nome_fantasia_cliente' => 0,
    'tipo_pessoa_fornecedor' => 0,
    'nome_fornecedor' => 0,
    'nome_fantasia_fornecedor' => 0,
    'boleto_vencimento' => 0,
    'data_pagamento_vencimento' => 0,
    'data_vencimento_vencimento' => 0,
    'valor_total_vencimento' => 0,
    'nome_centro_custo' => 0,
    'observacoes_vencimento' => 0,
    'permissao_editar_financeiro_conta' => 0,
    'codigo_centro_custo_vencimento_somatorio' => 0,
    'nome_centro_custo_vencimento_somatorio' => 0,
    'valor_total_vencimento_somatorio' => 0,
    'somatorio_valor_total_vencimento' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a17f6aa215c1_80007479')) {function content_53a17f6aa215c1_80007479($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Financeiro"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/financeiro.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Financeiro</h1>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_financeiro_conta']->value==='1') {?>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				<?php }?>
			</div>
		</header>

		<div class="table-responsive">
			<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro" id="formulario_filtrar_financeiro">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<input type='submit' value='Filtrar novamente' class='pull-left btn btn-primary'>
							</div>

							<div class='col-xs-12 col-sm-6 col-md-6 col-lg-6' class="pull-left">
								Período: <b><?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
</b> a <b><?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
</b><br>
								Tipo de operação: <b><?php echo $_smarty_tpl->tpl_vars['operacao_filtro']->value;?>
</b><br>
								Status dos vencimentos: <b><?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</b>
							</div>
						</div>
					</div>
				</div>	
			</form>

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Cliente/Fornecedor</th>
					<th>Boleto</th>
					<th>Pagamento</th>
					<th>Vencimento</th>
					<th>Valor</th>
					<th>Obra</th>
					<th>Observações</th>
					<th>Editar</th>
					<!--<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
						<th>Ação</th>
					<?php }?>-->
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_vencimento']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_vencimento']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td>
									<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

								</td>
																
								<td>
									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física') {?>
										<?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	 

									<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa_fornecedor']->value==='Física') {?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	
								</td>
								
								<th><?php echo $_smarty_tpl->tpl_vars['boleto_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</th>
								
								<th>
									<?php if ($_smarty_tpl->tpl_vars['data_pagamento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
										<?php echo $_smarty_tpl->tpl_vars['data_pagamento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

									<?php }?>	
								</th>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['data_vencimento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='30/11/-0001') {?>
										<?php if ($_smarty_tpl->tpl_vars['data_vencimento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='NULL') {?>
											<?php if ($_smarty_tpl->tpl_vars['data_vencimento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='01/01/1970') {?>
												<?php echo $_smarty_tpl->tpl_vars['data_vencimento_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

											<?php }?>
										<?php }?>
									<?php }?>
								</td>
								
								<td><?php echo number_format($_smarty_tpl->tpl_vars['valor_total_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['nome_centro_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

								<td><?php echo $_smarty_tpl->tpl_vars['observacoes_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/editarVencimento/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit" title="editar"></span></a>
									<?php }?>
								</td>

								<!--<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/inativar/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_financeiro"><span class="glyphicon glyphicon-trash" title="inativar"></span></a>
									<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/ativar/<?php echo $_smarty_tpl->tpl_vars['codigo_vencimento']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_financeiro"><span class="glyphicon glyphicon-plus" title="ativar"></span></a>
									<?php }?>
								</td>-->
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>

			<!-- <p>
				<table width="35%" align="right">
					<?php if ($_smarty_tpl->tpl_vars['codigo_centro_custo_vencimento_somatorio']->value!=='0') {?>
						<?php if ($_smarty_tpl->tpl_vars['nome_centro_custo_vencimento_somatorio']->value!=='') {?>	
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_centro_custo_vencimento_somatorio']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								<?php if ($_smarty_tpl->tpl_vars['codigo_centro_custo_vencimento_somatorio']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!=='0') {?>
									<tr>
										<td width="50%">
											Sub-total de <?php echo $_smarty_tpl->tpl_vars['nome_centro_custo_vencimento_somatorio']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

										</td>
										
										<td width="50%">&nbsp;
											<b>R$ <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_vencimento_somatorio']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>
</b>
										</td>
									</tr>
								<?php }?>
							<?php endfor; endif; ?>
						<?php }?>
					<?php }?>	
				</table>
			</p>

			<p>
				<table width="35%" align="right" style="margin-top:1%; margin-left:40%;">
					<tr>
						<td width="50%">
							<b>Total Geral:</b>
						</td>
									
						<td width="50%">&nbsp;
							R$ <?php echo $_smarty_tpl->tpl_vars['somatorio_valor_total_vencimento']->value;?>

						</td>
					</tr>
				</table>
			</p> -->
			
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
