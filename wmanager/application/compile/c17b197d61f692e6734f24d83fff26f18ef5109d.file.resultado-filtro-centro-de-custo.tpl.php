<?php /* Smarty version Smarty-3.1.16, created on 2014-05-22 16:53:02
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-centro-de-custo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:734766387537e0da239e5d6-47004808%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c17b197d61f692e6734f24d83fff26f18ef5109d' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/resultado-filtro-centro-de-custo.tpl',
      1 => 1400770372,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '734766387537e0da239e5d6-47004808',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537e0da248d3c7_99626919',
  'variables' => 
  array (
    'base_url' => 0,
    'campo_filtro' => 0,
    'parametro_filtro' => 0,
    'valor_filtro' => 0,
    'permissao_excluir_fornecedor_conta' => 0,
    'codigo_centro_de_custo' => 0,
    'nome_centro_de_custo' => 0,
    'status_centro_de_custo' => 0,
    'permissao_editar_fornecedor_conta' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537e0da248d3c7_99626919')) {function content_537e0da248d3c7_99626919($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/gadministrativo/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"centro de custo"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/centro_de_custo.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Centro de Custo</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

					<div class='btn-group pull-right'>
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/" class="btn btn-primary" title="Listar todos">
							Listar todos
						</a>

						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/cadastrar" class="btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			
			<!-- filtro -->
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/filtrar" id="formulario_filtrar_centro_de_custo">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' class="form-control" id="campo_filtrar_centro_de_custo">
           							<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='status_centro_de_custo') {?>
										<option value="status_centro_de_custo" selected="selected">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='nome_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo" selected="selected">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='endereco_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo" selected="selected">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='bairro_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo" selected="selected">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cep_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo" selected="selected">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cidade_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo" selected="selected">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='estado_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo" selected="selected">Estado</option>
			               				<option value="responsavel_centro_de_custo">Responsável</option>
		               				<?php }?>

		               				<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='responsavel_centro_de_custo') {?>
										<option value="status_centro_de_custo">Status (Ativo, Inativo ou Bloqueado)</option>
			               				<option value="nome_centro_de_custo">Nome</option>
			               				<option value="endereco_centro_de_custo">Endereço</option>
			               				<option value="bairro_centro_de_custo">Bairro</option>
			               				<option value="cep_centro_de_custo">CEP</option>
			               				<option value="cidade_centro_de_custo">Cidade</option>
			               				<option value="estado_centro_de_custo">Estado</option>
			               				<option value="responsavel_centro_de_custo" selected="selected">Responsável</option>
		               				<?php }?>
								</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='iniciado_por') {?>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='igual') {?>
										<option value="igual">Igual</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='terminado_em') {?>
										<option value="terminado_em">Terminado em</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='em_qualquer_posicao') {?>
										<option value="em_qualquer_posicao">Em qualquer posição</option>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               				<?php }?>	
		               			</select>
							</div>
						
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='cep_centro_de_custo') {?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['valor_filtro']->value,'##.###-###');?>
" id="valor_filtrar_centro_de_custo">	
								<?php } else { ?>
									<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autocomplete="yes" autofocus="yes" value='<?php echo $_smarty_tpl->tpl_vars['valor_filtro']->value;?>
' id="valor_filtrar_centro_de_custo">	
								<?php }?>
									<span class='alerta_formulario' id='alerta_valor_filtrar_centro_de_custo'></span>
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>
			<!-- filtro -->

			<br>

			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Nome</th>
					<th>Status</th>
					<th>Editar</th>
					<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1') {?>
						<th>Ação</th>
					<?php }?>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['status_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_fornecedor_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
									<?php }?>
								</td>

								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1') {?>
										<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Ativo') {?>
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/desativar/<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon-trash" title="desativar"></span></a>

										<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Inativo') {?>
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/ativar/<?php echo $_smarty_tpl->tpl_vars['codigo_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_cliente"><span class="glyphicon glyphicon-plus" title="ativar"></span></a>
										<?php }?>
									<?php }?>
								</td>
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
