<?php /* Smarty version Smarty-3.1.16, created on 2014-09-19 18:57:32
         compiled from "/opt/lampp/htdocs/wmanager/application/views/resultado-filtro-produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21141621635411999a2b0858-97677366%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d2cd78b02d632d615804df1f202896b605bad84' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/resultado-filtro-produto.tpl',
      1 => 1411145851,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21141621635411999a2b0858-97677366',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5411999a32b156_75302451',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_produto_conta' => 0,
    'campo_filtro' => 0,
    'parametro_filtro' => 0,
    'valor_filtro' => 0,
    'id' => 0,
    'nome' => 0,
    'preco_de_venda' => 0,
    'preco_de_custo' => 0,
    'quantidade' => 0,
    'nome_categoria' => 0,
    'status' => 0,
    'permissao_editar_produto_conta' => 0,
    'permissao_excluir_produto_conta' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5411999a32b156_75302451')) {function content_5411999a32b156_75302451($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Produto"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/produto.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<h1>Produto</h1>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class='btn-group pull-right'>
						<div>
							<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/" class="btn btn-primary" title="Listar todos">
								Listar todos
							</a>
						
							<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_produto_conta']->value==='1') {?>									
								<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/cadastrar" class="btn btn-primary" title="Cadastrar">
									Novo
								</a>
							<?php }?>
						</div>		
					</div>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/filtrar">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' class="form-control">
	           						
	           						<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='status_produto') {?>
										<option value="status_produto">Status (Ativo, Inativo)</option>
				               			<option value="nome_produto">Nome do produto</option>
				               			<option value="categoria_produto">Categoria</option>
				               		<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='nome_produto') {?>
										<option value="nome_produto">Nome do produto</option>
				               			<option value="status_produto">Status (Ativo, Inativo)</option>
				               			<option value="categoria_produto">Categoria</option>
									<?php }?>

									<?php if ($_smarty_tpl->tpl_vars['campo_filtro']->value==='categoria_produto') {?>
										<option value="categoria_produto">Categoria</option>
										<option value="status_produto">Status (Ativo, Inativo)</option>
				               			<option value="nome_produto">Nome do produto</option>
				               		<?php }?>

								</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='iniciado_por') {?>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='igual') {?>
										<option value="igual">Igual</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="terminado_em">Terminado em</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='terminado_em') {?>
										<option value="terminado_em">Terminado em</option>
		               					<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="em_qualquer_posicao">Em qualquer posição</option>
			               			<?php }?>

			               			<?php if ($_smarty_tpl->tpl_vars['parametro_filtro']->value==='em_qualquer_posicao') {?>
										<option value="em_qualquer_posicao">Em qualquer posição</option>
										<option value="iniciado_por">Iniciado por</option>
		               					<option value="igual">Igual</option>
		               					<option value="terminado_em">Terminado em</option>
		               				<?php }?>	
		               			</select>
							</div>

							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autofocus="yes" value='<?php echo $_smarty_tpl->tpl_vars['valor_filtro']->value;?>
' required>	
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>
			
			<br>
		
			<div class="table-responsive">
				<table class='table table-hover table-striped'>
					<thead>
						<th>Cod</th>
						<th>Nome</th>
						<th>Preço de venda</th>
						<th>Preço de custo</th>
						<th>Quantidade</th>
						<th>Categoria</th>
						<th>Status</th>
						<th>Editar</th>
						<th>Ação</th>
					</thead>

					<tbody>
						<?php if ($_smarty_tpl->tpl_vars['id']->value!=='0') {?>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
									
									<td>R$ <?php echo $_smarty_tpl->tpl_vars['preco_de_venda']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td>R$ <?php echo $_smarty_tpl->tpl_vars['preco_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['quantidade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['nome_categoria']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td><?php echo $_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<td>
										<?php if ($_smarty_tpl->tpl_vars['permissao_editar_produto_conta']->value==='1') {?>									
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/editar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
												<span class="glyphicon glyphicon glyphicon-edit"></span>
											</a>
										<?php }?>	
									</td>
									
									<td>
										<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_produto_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Ativo') {?>
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/inativar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_item">
												<span class="glyphicon glyphicon-trash"></span>
											</a>
										
										<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_produto_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Inativo') {?>
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/ativar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_item">
												<span class="glyphicon glyphicon-plus"></span>
											</a>
										<?php }?>	
									</td>
									
								</tr>
							<?php endfor; endif; ?>
						<?php }?>
					</tbody>
				</table>
				
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
