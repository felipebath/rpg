<?php /* Smarty version Smarty-3.1.16, created on 2014-09-16 17:10:14
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-servico.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1929276260541847034f9b40-22181114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b5394330c270e4e4e33e9441e84ed563562331c1' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-servico.tpl',
      1 => 1410880206,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1929276260541847034f9b40-22181114',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54184703534d29_92505483',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'status' => 0,
    'nome' => 0,
    'descricao' => 0,
    'preco' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54184703534d29_92505483')) {function content_54184703534d29_92505483($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar serviço"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/servico.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando serviço</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
servico/" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
servico/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Status</label>
                        	<select class="form-control" id="status">
                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
                            		<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>		                                	
                            	<?php }?>
                            	
                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
                            		<option value="Inativo">Inativo</option>
                                	<option value="Ativo">Ativo</option>		                                	
                            	<?php }?>	                             
                            </select>
                        </div>
                    
                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Descrição</label>
	                            <textarea id="descricao" class="form-control"><?php echo $_smarty_tpl->tpl_vars['descricao']->value;?>
</textarea>
	                        </div>	

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Preço</label>
                            <input type="text" class="form-control" id="preco" maxlength="15" data-mascara-campo="moeda" value="<?php echo $_smarty_tpl->tpl_vars['preco']->value;?>
">
                        </div>
                    </div>
                    
                    <br>

                	<div class="row">
                		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<label>Observações</label>
                            <textarea id="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
                        </div>	
                	</div>         
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
