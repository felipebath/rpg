<?php /* Smarty version Smarty-3.1.16, created on 2014-08-29 20:44:53
         compiled from "/opt/lampp/htdocs/rr/application/views/editar-fornecedor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:113301254053ff95bf347556-48507512%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9709ee790a85edf3c2f3071ef5c8865d61a3d1ce' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/editar-fornecedor.tpl',
      1 => 1409337892,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113301254053ff95bf347556-48507512',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53ff95bf379aa4_47572999',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'tipo_pessoa' => 0,
    'nome' => 0,
    'status' => 0,
    'razao_social' => 0,
    'inscricao_estadual' => 0,
    'cnpj' => 0,
    'cpf' => 0,
    'endereco' => 0,
    'bairro' => 0,
    'cep' => 0,
    'estado' => 0,
    'cidade' => 0,
    'telefone1' => 0,
    'telefone2' => 0,
    'telefone3' => 0,
    'email' => 0,
    'contato' => 0,
    'site' => 0,
    'anotacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ff95bf379aa4_47572999')) {function content_53ff95bf379aa4_47572999($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/rr/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/fornecedor.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando fornecedor</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs" id="tab_fornecedor">
				<li class="active"><a href="#" title="Fornecedor" data-toggle="tab">Dados Cadastrais</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/cadastrarProduto?fornecedor_id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" title="Produtos">Produtos</a></li>
			</ul>
		</nav>
		
		<br>
		
		<div class="tab-content">
			<div class="tab-pane active" id="fornecedor">
				<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/fazerEdicao/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" method="post" id="formulario">
					<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" name="id">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						
							<div class="row">
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<label>Tipo pessoa</label>
	                            	<select class="form-control" name="tipo_pessoa" autofocus="yes">
	                                	<?php if (($_smarty_tpl->tpl_vars['tipo_pessoa']->value==='Física')) {?>
	                                		<option value="Física">Física</option>
		                                	<option value="Jurídica">Jurídica</option>		                                	
	                                	<?php } else { ?>
	                                		<option value="Jurídica">Jurídica</option>
		                                	<option value="Física">Física</option>		                                	
	                                	<?php }?>	      
	                                </select>
		                        </div>

		                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                           	<label>Nome</label>
		                           	<input type="text" placeholder="Nome" name="nome" autocomplete="yes" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
		                           	<span class='alerta_formulario' name='alerta_nome'></span>
		                        </div>
								
		                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<label>Status</label>
	                            	<select class="form-control" name="status">
	                                	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
	                                		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>		                                	
	                                	<?php }?>
	                                	
	                                	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
	                                		<option value="Inativo">Inativo</option>
		                                	<option value="Ativo">Ativo</option>		                                	
	                                	<?php }?>	      	                              
	                                </select>
		                        </div>
		                    </div>

		                    <br>

	                       	<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa']->value!=='Jurídica') {?>

		                       	<div id="pessoa_juridica" style="display:none;">
		                    		<div class="row"> 	
		                       			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                            	    <label>Razão Social</label>
				                                <input type="text" placeholder="Razão social" class="form-control" name="razao_social" autocomplete="yes"  value="<?php echo $_smarty_tpl->tpl_vars['razao_social']->value;?>
">
				                                <span class='alerta_formulario' name='alerta_razao_social'></span>
			                        	</div>

			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	    <label>Inscriçao Estadual</label>
			                                <input type="text" placeholder="Inscriçao Estadual" class="form-control" name="inscricao_estadual" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['inscricao_estadual']->value;?>
">
			                                <span class='alerta_formulario' name='alerta_inscricao_estadual'></span>
			                        	</div>
			                        	     
			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                           	    <label>CNPJ</label>
				                            <?php if ($_smarty_tpl->tpl_vars['cnpj']->value!=='') {?>
				                            	<input type="text" placeholder="CNPJ" class="form-control" name="cnpj" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj']->value,'##.###.###/####-##');?>
"> 
				                            <?php } else { ?>
				                            	<input type="text" placeholder="CNPJ" class="form-control" name="cnpj" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['cnpj']->value;?>
"> 
				                            <?php }?>
				                            <span class='alerta_formulario' name='alerta_cnpj'></span>
				                        </div>
				                	</div>
			                    </div>	
		                            
		                        <div id="pessoa_fisica">
			                    	<div class="row"> 	
		                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                            	<label>CPF</label>
			                            	<?php if ($_smarty_tpl->tpl_vars['cpf']->value!=='') {?>
			                            		<input type="text" name="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf']->value,'###.###.###-##');?>
" >
			                            	<?php } else { ?>
			                            		<input type="text" name="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo $_smarty_tpl->tpl_vars['cpf']->value;?>
" >
			                            	<?php }?>
			                            	<span class='alerta_formulario' name='alerta_cpf'></span>
			                            </div>
			                        </div>    
			                    </div>

		                    <?php } else { ?>	

		                    	<div id="pessoa_juridica">
		                    		<div class="row"> 	
		                       			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                            	    <label>Razão Social</label>
				                                <input type="text" placeholder="Razão social" class="form-control" name="razao_social" autocomplete="yes"  value="<?php echo $_smarty_tpl->tpl_vars['razao_social']->value;?>
">
				                                <span class='alerta_formulario' name='alerta_razao_social'></span>
			                        	</div>

			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	    <label>Inscriçao Estadual</label>
			                                <input type="text" placeholder="Inscriçao Estadual" class="form-control" name="inscricao_estadual" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['inscricao_estadual']->value;?>
">
			                                <span class='alerta_formulario' name='alerta_inscricao_estadual'></span>
			                        	</div>
			                        	     
			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                           	    <label>CNPJ</label>
				                            <?php if ($_smarty_tpl->tpl_vars['cnpj']->value!=='') {?>
				                            	<input type="text" placeholder="CNPJ" class="form-control" name="cnpj" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj']->value,'##.###.###/####-##');?>
"> 
				                            <?php } else { ?>
				                            	<input type="text" placeholder="CNPJ" class="form-control" name="cnpj" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['cnpj']->value;?>
"> 
				                            <?php }?>
				                            <span class='alerta_formulario' name='alerta_cnpj'></span>
				                        </div>
				                	</div>
			                    </div>	
		                            
		                        <div id="pessoa_fisica" style="display:none;">
			                    	<div class="row"> 	
		                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                            	<label>CPF</label>
			                            	<?php if ($_smarty_tpl->tpl_vars['cpf']->value!=='') {?>
			                            		<input type="text" name="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf']->value,'###.###.###-##');?>
" >
			                            	<?php } else { ?>
			                            		<input type="text" name="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo $_smarty_tpl->tpl_vars['cpf']->value;?>
" >
			                            	<?php }?>
			                            	<span class='alerta_formulario' name='alerta_cpf'></span>
			                            </div>
			                        </div>    
			                    </div>

			                <?php }?>    

		                    <br>

		                    <div>
		                    	<div class="row"> 	
	                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Endereço</label>
		                            	<input type="text" name="endereco" placeholder="Digite o endereço" class="form-control"  autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['endereco']->value;?>
">
		                            	<span class='alerta_endereco' name='alerta_endereco'></span>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Bairro</label>
		                            	<input type="text" placeholder="Digite o bairro" name="bairro" autocomplete="yes" class="form-control" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['bairro']->value;?>
">
		                            	<span class='alerta_formulario' name='alerta_bairro'></span>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>CEP</label>
		                            	<input type="text" placeholder="Digite o Cep" name="cep" autocomplete="yes" class="form-control" autofocus="yes" data-mascara-campo='cep' maxlength="9" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cep']->value,'##.###-####');?>
">
		                            	<span class='alerta_formulario' name='alerta_cep'></span>
		                            </div>


		                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
										<label>Estado</label>
										<select class="form-control" name="estado">
											<option value="<?php echo $_smarty_tpl->tpl_vars['estado']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['estado']->value;?>
</option>
											<option value="AC">AC</option>
											<option value="AL">AL</option>
											<option value="AM">AM</option>
											<option value="AP">AP</option>
											<option value="BA">BA</option>
											<option value="CE">CE</option>
											<option value="ES">ES</option>
											<option value="GO">GO</option>
											<option value="MA">MA</option>
											<option value="MG">MG</option>
											<option value="MS">MS</option>
											<option value="MT">MT</option>
											<option value="PA">PA</option>
											<option value="PB">PB</option>
											<option value="PE">PE</option>
											<option value="PI">PI</option>
											<option value="PR">PR</option>
											<option value="RJ">RJ</option>
											<option value="RN">RN</option>
											<option value="RO">RO</option>
											<option value="RR">RR</option>
											<option value="RS">RS</option>
											<option value="SC">SC</option>
											<option value="SE">SE</option>
											<option value="SP">SP</option>
											<option value="TO">TO</option>
										 </select>
									</div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Cidade</label>
		                            	<input type="text" placeholder="Digite a Cidade" name="cidade" autocomplete="yes" class="form-control" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['cidade']->value;?>
">
		                            	<span class='alerta_formulario' name='alerta_cidade'></span>
		                            </div>
		                    	</div>

		                    	<br>

		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone 1</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['telefone1']->value!=='') {?>
		                            		<input type="text" placeholder="Digite o Telefone" name="telefone1" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone1']->value,'(##) ####-####');?>
" >
		                            	<?php } else { ?>
		                            		<input type="text" placeholder="Digite o Telefone" name="telefone1" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['telefone1']->value;?>
" >
		                            	<?php }?>
		                            	<span class='alerta_formulario' name='alerta_telefone1'></span>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone 2</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['telefone2']->value!=='') {?>
		                            	   	<input type="text" placeholder="Digite o Telefone" name="telefone2" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone2']->value,'(##) ####-####');?>
" >                 
		                            	<?php } else { ?>
		                            		<input type="text" placeholder="Digite o Telefone" name="telefone2" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['telefone2']->value;?>
" >
		                            	<?php }?>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone 3</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['telefone3']->value!=='') {?>
		                            		<input type="text" placeholder="Digite o Telefone" name="telefone3" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone3']->value,'(##) ####-####');?>
" >	                  
		                           		<?php } else { ?>
		                           			<input type="text" placeholder="Digite o Telefone" name="telefone3" autocomplete="yes" class="form-control" data-mascara-campo='telefone' maxlength="15" value="<?php echo $_smarty_tpl->tpl_vars['telefone3']->value;?>
" >
		                           		<?php }?>
		                            </div>

		                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>E-mail</label>
		                            	<input type="text" placeholder="Digite o e-mail" name="email" autocomplete="yes" class="form-control" maxlength="80" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">
		                            	<span class='alerta_formulario' name='alerta_email'></span>
		                            </div>
		                        </div>

		                        <br>

		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Contato</label>
		                            	<input type="text" placeholder="Digite o contato" name="contato" autocomplete="yes" class="form-control" maxlength="35" value="<?php echo $_smarty_tpl->tpl_vars['contato']->value;?>
">
		                            </div>

		                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Site</label>
		                            	<input type="text" placeholder="Digite o site" name="site" autocomplete="yes" class="form-control" maxlength="25" value="<?php echo $_smarty_tpl->tpl_vars['site']->value;?>
">	                  
		                            </div>

		                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>Anotações</label>
		                            	<textarea placeholder="Anotações" name="anotacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['anotacoes']->value;?>
</textarea>	                  
		                            </div>
		                        </div>

		                        <br>
		                        <br>

		                        <div class="row"> 	
		                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                        		<input type="submit" value="Salvar e continuar" class="btn btn-primary"title="salvar e continuar para a tela de produtos">
										&nbsp;&nbsp;
										<a href="javascript:history.go(-1)" class="btn btn-primary">Cancelar</a>
									</div>
								</div>		
		                    
		                    </div>    
						</div>
					</div>
				</form>
			</div><!-- fornecedor -->
 		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
