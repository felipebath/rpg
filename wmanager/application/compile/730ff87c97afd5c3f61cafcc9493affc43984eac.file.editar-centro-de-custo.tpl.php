<?php /* Smarty version Smarty-3.1.16, created on 2014-09-15 19:50:32
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-centro-de-custo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:168132221054108f727029f6-45040596%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '730ff87c97afd5c3f61cafcc9493affc43984eac' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-centro-de-custo.tpl',
      1 => 1410803419,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168132221054108f727029f6-45040596',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54108f7277ee05_67865055',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'status' => 0,
    'nome' => 0,
    'observacoes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54108f7277ee05_67865055')) {function content_54108f7277ee05_67865055($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar centro de custo"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/centro_de_custo.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando centro de custo</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Status</label>
                        	<select class="form-control" id="status">
                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
                            		<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>		                                	
                            	<?php }?>
                            	
                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
                            		<option value="Inativo">Inativo</option>
                                	<option value="Ativo">Ativo</option>		                                	
                            	<?php }?>	                             
                            </select>
                        </div>
                    
                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                        </div>
                
                		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<label>Observações</label>
                            <textarea id="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
                        </div>	
                	</div>         
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ('botoes-submit.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
