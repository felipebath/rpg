<?php /* Smarty version Smarty-3.1.16, created on 2016-01-18 17:30:02
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-personagem.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1415564165569d04661f8ec0-39682538%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8fcf73ad4a315f6ff051ece18eb6d00d051ef541' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-personagem.tpl',
      1 => 1453134024,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1415564165569d04661f8ec0-39682538',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_569d046623d1f2_54712658',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569d046623d1f2_54712658')) {function content_569d046623d1f2_54712658($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar personagem"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/personagem.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo personagem</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem/fazerCadastro" method="post" id="formulario">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="30" autofocus="yes">
                		</div>
                
                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    	    <label>Arma</label>
                            <input type="text" placeholder="Arma" class="form-control" id="arma">
                        </div>
                    		
                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Ataque</label>
                            <input type="number" placeholder="Ataque" class="form-control" id="ataque">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Vida</label>
                            <input type="number" placeholder="Vida" class="form-control" id="vida">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Agilidade</label>
                            <input type="number" placeholder="Agilidade" class="form-control" id="agilidade">
                        </div>
                    </div>
                    
                    <br>

                    <div class="row">    
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Força</label>
                            <input type="number" placeholder="Força" class="form-control" id="forca">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Defesa</label>
                            <input type="number" placeholder="Defesa" class="form-control" id="defesa">
                        </div>
                    </div>    
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
