<?php /* Smarty version Smarty-3.1.16, created on 2016-01-18 16:27:07
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-banco.tpl" */ ?>
<?php /*%%SmartyHeaderCode:49818048254104df2930694-44481766%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46876deff4655c5ff0fef6afdf20da41229d1b26' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-banco.tpl',
      1 => 1453128556,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49818048254104df2930694-44481766',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54104df295e972_09629887',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54104df295e972_09629887')) {function content_54104df295e972_09629887($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar banco"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/banco.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo banco</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/fazerCadastro" method="post" id="formulario">
			
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
                            	<select class="form-control" id="status">
                                	<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>	                             
                                </select>
	                        </div>
	                    
	                    	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes">
                    		</div>
                    
                    		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        	    <label>Agência</label>
                                <input type="text" placeholder="Agência" class="form-control" id="agencia">
	                        </div>
                        		
                        	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Conta</label>
	                            <input type="text" placeholder="Conta" class="form-control" id="conta" maxlength="15">
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Operacao</label>
                            	<select class="form-control" id="operacao">
                                	<option value="Jurídico">Jurídico</option>
                                	<option value="Físico">Físico</option>
                                	<option value="Poupança">Poupança</option>
                                </select>
	                        </div>
	                    </div>    

	                    <br>

	                    <div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Observações</label>
	                            <textarea id="observacoes" class="form-control"></textarea>
	                        </div>	
	                	</div>                                                            
					</div>
				</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
