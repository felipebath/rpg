<?php /* Smarty version Smarty-3.1.16, created on 2016-01-18 17:27:00
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-personagem.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2069241735569d1254e97274-84086471%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '480390466662c0a79116b8a1693aa09afe04fb78' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-personagem.tpl',
      1 => 1453134042,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2069241735569d1254e97274-84086471',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'nome' => 0,
    'arma' => 0,
    'ataque' => 0,
    'vida' => 0,
    'agilidade' => 0,
    'forca' => 0,
    'defesa' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_569d1254eed547_77323166',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569d1254eed547_77323166')) {function content_569d1254eed547_77323166($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar personagem"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/personagem.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando personagem</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem/" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
personagem/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="30" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                		</div>
                
                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    	    <label>Arma</label>
                            <input type="text" placeholder="Arma" class="form-control" id="arma" value="<?php echo $_smarty_tpl->tpl_vars['arma']->value;?>
">
                        </div>
                    		
                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Ataque</label>
                            <input type="number" placeholder="Ataque" class="form-control" id="ataque" value="<?php echo $_smarty_tpl->tpl_vars['ataque']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Vida</label>
                            <input type="number" placeholder="Vida" class="form-control" id="vida" value="<?php echo $_smarty_tpl->tpl_vars['vida']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Agilidade</label>
                            <input type="number" placeholder="Agilidade" class="form-control" id="agilidade" value="<?php echo $_smarty_tpl->tpl_vars['agilidade']->value;?>
">
                        </div>
                    </div>
                    
                    <br>

                    <div class="row">    
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Força</label>
                            <input type="number" placeholder="Força" class="form-control" id="forca" value="<?php echo $_smarty_tpl->tpl_vars['forca']->value;?>
">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Defesa</label>
                            <input type="number" placeholder="Defesa" class="form-control" id="defesa" value="<?php echo $_smarty_tpl->tpl_vars['defesa']->value;?>
">
                        </div>
                    </div>    
				</div>
			</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
