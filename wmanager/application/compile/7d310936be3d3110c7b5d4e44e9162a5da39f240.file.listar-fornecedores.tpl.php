<?php /* Smarty version Smarty-3.1.16, created on 2014-09-11 22:46:27
         compiled from "/opt/lampp/htdocs/wmanager/application/views/listar-fornecedores.tpl" */ ?>
<?php /*%%SmartyHeaderCode:183587418654120a238f96b0-97972643%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d310936be3d3110c7b5d4e44e9162a5da39f240' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/listar-fornecedores.tpl',
      1 => 1410468383,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183587418654120a238f96b0-97972643',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_fornecedor_conta' => 0,
    'id' => 0,
    'nome' => 0,
    'tipo_pessoa' => 0,
    'telefone' => 0,
    'celular' => 0,
    'email' => 0,
    'permissao_editar_fornecedor_conta' => 0,
    'permissao_excluir_fornecedor_conta' => 0,
    'status' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54120a23965372_46419891',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54120a23965372_46419891')) {function content_54120a23965372_46419891($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/fornecedor.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Fornecedor</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_fornecedor_conta']->value==='1') {?>									
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">
							Novo
						</a>
					<?php }?>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/filtrar">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<select name='campo_filtro' class="form-control">
		               				<option value="status_fornecedor">Status (Ativo, Inativo)</option>
		               				<option value="tipo_pessoa_fornecedor">Tipo pessoa (Física, Jurídica)</option>
		               				<option value="nome_fornecedor">Nome</option>
		               				<option value="cpf_fornecedor">CPF</option>
		               				<option value="cnpj_fornecedor">CNPJ</option>
		               			</select>
							</div>
						
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<select name='parametro_filtro' class="form-control">
		               				<option value="iniciado_por">Iniciado por</option>
		               				<option value="igual">Igual</option>
		               				<option value="terminado_em">Terminado em</option>
		               				<option value="em_qualquer_posicao">Em qualquer posição</option>
		               			</select>
							</div>
						
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<input type="search" name="valor_filtro" class="form-control" placeholder="Digite sua pesquisa e pressione enter." autofocus="yes" required>
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>
			
			<br>
		
			<div class="table-responsive">
				<table class='table table-hover table-striped'>
					<thead>
						<th>Cod</th>
						<th>Nome</th>
						<th>Tipo Pessoa</th>
						<th>Telefone</th>
						<th>Celular</th>
						<th>Email</th>
						<th>Editar</th>
						<th>Ação</th>
					</thead>

					<tbody>
						<?php if ($_smarty_tpl->tpl_vars['id']->value!=='0') {?>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
									
									<td><?php echo $_smarty_tpl->tpl_vars['tipo_pessoa']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

									<td>
										<?php if ($_smarty_tpl->tpl_vars['telefone']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!=='') {?>
											<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'(##) ####-#####');?>

										<?php }?>
									</td>	

									<td>
										<?php if ($_smarty_tpl->tpl_vars['celular']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!=='') {?>
											<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['celular']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],'(##) #####-####');?>

										<?php }?>	
									</td>

									<td><?php echo $_smarty_tpl->tpl_vars['email']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
									
									<td>
										<?php if ($_smarty_tpl->tpl_vars['permissao_editar_fornecedor_conta']->value==='1') {?>									
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/editar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
												<span class="glyphicon glyphicon glyphicon-edit"></span>
											</a>
										<?php }?>	
									</td>
									
									<td>
										<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Ativo') {?>
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/inativar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="inativar_item">
												<span class="glyphicon glyphicon-trash"></span>
											</a>
										
										<?php } elseif ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1'&&$_smarty_tpl->tpl_vars['status']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Inativo') {?>
											<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/ativar/<?php echo $_smarty_tpl->tpl_vars['id']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="ativar_item">
												<span class="glyphicon glyphicon-plus"></span>
											</a>
										<?php }?>	
									</td>
									
								</tr>
							<?php endfor; endif; ?>
						<?php }?>
					</tbody>
				</table>
				
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
