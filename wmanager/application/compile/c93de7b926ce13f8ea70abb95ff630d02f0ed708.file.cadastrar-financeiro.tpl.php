<?php /* Smarty version Smarty-3.1.16, created on 2014-07-11 22:07:00
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-financeiro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5954641825387a0b05055a0-10925563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c93de7b926ce13f8ea70abb95ff630d02f0ed708' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-financeiro.tpl',
      1 => 1405109195,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5954641825387a0b05055a0-10925563',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5387a0b0556c47_88975282',
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_cliente_financeiro' => 0,
    'nome_cliente' => 0,
    'codigo_fornecedor' => 0,
    'tipo_pessoa' => 0,
    'nome_fornecedor' => 0,
    'nome_fantasia_fornecedor' => 0,
    'codigo_banco_financeiro' => 0,
    'nome_banco' => 0,
    'codigo_centro_custo_financeiro' => 0,
    'nome_centro_custo' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5387a0b0556c47_88975282')) {function content_5387a0b0556c47_88975282($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/js/financeiro.js" defer ></script>
	<section class="container-fluid">
		<header class="page-header">
			<div class='row'>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo financeiro</h1>
				</div>
			
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro" class="btn btn-primary pull-right" title="Visualizar todos">
						Visualizar todos
					</a>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs" id="tab_financeiro">
				<li class="active"><a href="#cadastro" title="Cadastro" data-toggle="tab">Cadastro</a></li>
				<li><a href="#vencimento" title="Vencimento" data-toggle="tab">Vencimentos</a></li>
			</ul>
		</nav>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
financeiro/fazerCadastro" id="formulario_cadastrar_financeiro" method="post">

		<div class="tab-content">
			<div class="tab-pane active" id="cadastro">
				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row"> 
						    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						    	<label>Operação</label>
						       	<select class="form-control" id="operacao_cadastrar_financeiro">
						           	<option value="Crédito">Crédito</option>
						           	<option value="Débito" selected="selected">Débito</option>
						        </select>
						    </div>
							
							<div id="operacao_credito" style="display:none;">
			        			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			            			<label>Cliente</label>
			                    	<select class="form-control" id="codigo_cliente_cadastrar_financeiro">
			                        	<option value="" selected="selected">Escolha</option>
			                        	<?php if ($_smarty_tpl->tpl_vars['codigo_cliente_financeiro']->value!='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_cliente_financeiro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <?php if ($_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
		                                            <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                            </option>
		                                        <?php } else { ?>
		                                            <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_cliente_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_cliente']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                            </option>
		                                        <?php }?>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum cliente</option>
		                                <?php }?>    
			                        </select>
			                        <span class='alerta_formulario' id='alerta_codigo_cliente_cadastrar_financeiro'></span>
			            		</div>
				            </div>	
				                
				            <div id="operacao_debito">
			            	    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			                    	<label>Fornecedor</label>
			                    	<select class="form-control" id="codigo_fornecedor_cadastrar_financeiro">
			                        	<option value="" selected="selected">Escolha</option>
			                        	<?php if ($_smarty_tpl->tpl_vars['codigo_fornecedor']->value!=='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_fornecedor']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <?php if (($_smarty_tpl->tpl_vars['tipo_pessoa']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==='Física')) {?>
		                                        	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
		                                        <?php } else { ?>
		                                        	<option value="<?php echo $_smarty_tpl->tpl_vars['codigo_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_fantasia_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
		                                        <?php }?>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum fornecedor</option>
		                                <?php }?>
		                            </select>
			                        <span class='alerta_formulario' id='alerta_codigo_fornecedor_cadastrar_financeiro'></span>
			                    </div>
				            </div>	

				            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						       	<label>Nota fiscal</label>
						       	<input type="text" placeholder="Digite o número da nota fiscal" id="nota_fiscal_cadastrar_financeiro" autocomplete="yes" class="form-control" autofocus="yes">
						       	<span class='alerta_formulario' id='alerta_nota_fiscal_cadastrar_financeiro'></span>
						    </div>

						    <div class="col-xs-12 col-sm- col-md-2 col-lg-2">
						       	<label>Data emissão</label>
						       	<input type="text" class="form-control"  id="data_emissao_cadastrar_financeiro" data-mascara-campo="data" maxlength="10" autocomplete="yes">
						        <span class='alerta_formulario' id='alerta_data_emissao_cadastrar_financeiro'></span>
						    </div>
				        </div><!--row-->
									        
					</div><!--col-xs-12-->
				</div><!--row-->
			</div> <!-- tab pane cadastro -->
			
			<div class="tab-pane" id="vencimento">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		
					<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						<div class="row">
														
							<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
								<br>
								<div class="row">
				                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                    	<label>Parcelas</label>
				                    	<input type="text" class="form-control" data-mascara-campo="inteiro" id="parcela_cadastrar_vencimento" maxlength="2" autocomplete="yes" disabled="yes">
				                    </div>
				                </div>
				                				                
				                <br>    

				                <div class="row">
				                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				                    	<label>Valor Total</label>
				                    	<input type="text" class="form-control" data-mascara-campo="moeda" id="valor_total_cadastrar_vencimento" disabled="yes">
				                	</div>
				                </div><!--row-->
				        	</div><!--col-xs-12-->    
				    	</div><!--row--> 	
					</div><!--col-xs-12-->
				
					<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<br>
							
							<div class="row"> 	
							    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							       	<label>Banco</label>
							       	<select class="form-control" id="codigo_banco_cadastrar_vencimento">
							           	<?php if ($_smarty_tpl->tpl_vars['codigo_banco_financeiro']->value!='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_banco_financeiro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <?php if ($_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
		                                            <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_banco_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
		                                            	<?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                            </option>
		                                        <?php } else { ?>
		                                            <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_banco_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                            </option>
		                                        <?php }?>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum banco</option>
		                                <?php }?>    
							        </select>
							    </div>
							
							    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							       	<label>Centro de custo</label>
							       	<select class="form-control" id="codigo_centro_custo_cadastrar_vencimento">
					           			<?php if ($_smarty_tpl->tpl_vars['codigo_centro_custo_financeiro']->value!='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_centro_custo_financeiro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <?php if ($_smarty_tpl->tpl_vars['nome_centro_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
		                                            <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_centro_custo_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
		                                            	<?php echo $_smarty_tpl->tpl_vars['nome_centro_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                            </option>
		                                        <?php } else { ?>
		                                            <option value="<?php echo $_smarty_tpl->tpl_vars['codigo_centro_custo_financeiro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['nome_centro_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                            </option>
		                                        <?php }?>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum centro de custo</option>
		                                <?php }?>    
							        </select>
							    </div>
							</div>

							<br>

							<div class="row">
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<label>Boleto</label>
							       	<input type="text" class="form-control" id="boleto_cadastrar_vencimento" maxlength="80" autofocus="yes" autocomplete="yes">
							       	<span class='alerta_formulario' id='alerta_boleto_cadastrar_vencimento'></span>
							    </div>
							
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							       	<label>Data vcto.</label>
							       	<input type="text" class="form-control"  id="data_vencimento_cadastrar_vencimento" data-mascara-campo="data" maxlength="10" autocomplete="yes" autocomplete="off">
							        <span class='alerta_formulario' id='alerta_data_vencimento_cadastrar_vencimento'></span>
							    </div>
							
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<label>Status Pgto.</label>
							       	<select class="form-control" id="status_pagamento_cadastrar_vencimento">
							           	<option value="A pagar" selected="yes">A pagar</option>
							           	<option value="Pago">Pago</option>
							        </select>
								</div>
							
							    <!-- se status_pagamento == 'pago' aplicar obrigatoriedade no campo data_pagamento -->
							    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							       	<label>Data Pgto.</label>
							       	<input type="text" class="form-control" id="data_pagamento_cadastrar_vencimento" data-mascara-campo="data" maxlength="10" disabled="yes" autocomplete="off">
							        <span class='alerta_formulario' id='alerta_data_pagamento_cadastrar_vencimento'></span>
							    </div>
							
							    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							       	<label>Forma</label>
							       	<select class="form-control" id="forma_pagamento_cadastrar_vencimento">
							           	<option value="Dinheiro">Dinheiro</option>
							           	<option value="Cartão">Cartão</option>
							           	<option value="Cheque">Cheque</option>
							        </select>
							    </div>
							
							    <div id="forma_pagamento_cheque" style="display:none;">
					               	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					        	    		<label>No. cheque</label>
					            		<input type="text" class="form-control"  id="numero_cheque_cadastrar_vencimento" autocomplete="off">
					            		<span class='alerta_formulario' id='alerta_numero_cheque_cadastrar_vencimento'></span>
					        		</div>
					        	</div>
					        </div>
					        
					        <br>

					        <div class="row">    
					            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							    	<label>Valor (R$)</label>
							    	<input type="text" placeholder="Digite o valor" id="valor_pagamento_cadastrar_vencimento" class="form-control" data-mascara-campo="moeda" maxlength="15" autocomplete="off">
							    	<span class='alerta_formulario' id='alerta_valor_pagamento_cadastrar_vencimento'></span>
							    </div>
							
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						       		<label>Juros (R$)</label>
						       		<input type="text" id="juros_cadastrar_vencimento" placeholder="Digite o valor" data-mascara-campo="moeda" maxlength="10" class="form-control" autocomplete="off">
						       		<span class='alerta_endereco' id='alerta_juros_cadastrar_vencimento'></span>
						    	</div>
						    
						    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							       	<label>Crédito (R$)</label>
							       	<input type="text" placeholder="Digite o valor" id="credito_cadastrar_vencimento" class="form-control" data-mascara-campo="moeda" autocomplete="off">
							       	<span class='alerta_formulario' id='alerta_credito_cadastrar_vencimento'></span>
							    </div>
							
							    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							    	<label>Observações</label>
							    	<textarea id="observacoes_cadastrar_vencimento" class="form-control"></textarea>
							    	<span class='alerta_formulario' id='alerta_observacoes_cadastrar_vencimento'></span>
							    </div>
							</div>

							<br>

							<div class="row">    
							    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<button class="btn btn-primary" id="adicionar_vencimento_cadastrar_vencimento">Adicionar vencimento</button>    
								</div>
							</div>	
						</div><!--col-xs-12-->	
					













						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<br>
							<div class="row">
								<table class="table table-striped table-bordered table-hover">
	                                <thead>
	                                    <tr>
	                                        <th>Código</th>
	                                        <th>Boleto</th>
	                                        <th>Vencimento</th>
	                                        <th>Status</th>
	                                        <th>Valor</th>
	                                        <th>Valor Total</th>
	                                        <th>Dt. Pgto.</th>
	                                        <th></th>
	                                    </tr>
	                                </thead>

	                                <tbody id="dados_vencimento_cadastrar_vencimento">
	                                	
	                                </tbody>
                            	</table>
                                
                            	<br>
							</div>
						</div>
					</div>		

				</div><!--col-xs-12-->

				<br>
				<input type="submit" value="Cadastrar" class="btn btn-primary pull-right">

			</div> <!-- tab-pane-vencimento -->	
		</div><!--tab content--> 

		

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
