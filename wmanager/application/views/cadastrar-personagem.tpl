{include file="cabecalho.tpl" titulo="Cadastrar personagem"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/personagem.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo personagem</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="{$base_url}personagem" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="{$base_url}personagem/fazerCadastro" method="post" id="formulario">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="30" autofocus="yes">
                		</div>
                
                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    	    <label>Arma</label>
                            <input type="text" placeholder="Arma" class="form-control" id="arma">
                        </div>
                    		
                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Ataque</label>
                            <input type="number" placeholder="Ataque" class="form-control" id="ataque">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Vida</label>
                            <input type="number" placeholder="Vida" class="form-control" id="vida">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Agilidade</label>
                            <input type="number" placeholder="Agilidade" class="form-control" id="agilidade">
                        </div>
                    </div>
                    
                    <br>

                    <div class="row">    
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Força</label>
                            <input type="number" placeholder="Força" class="form-control" id="forca">
                        </div>

                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                       	    <label>Defesa</label>
                            <input type="number" placeholder="Defesa" class="form-control" id="defesa">
                        </div>
                    </div>    
				</div>
			</div>
			<br>
			<br>
			{include file="botoes-submit.tpl"}
		</form>
	</section>
{include file="rodape.tpl"}