{include file="cabecalho.tpl" titulo="Turno"}
{include file="menu-2.tpl"}

<section class="container-fluid">
	<header class="page-header">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<h1>Iniciando Turno</h1>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				&nbsp;
			</div>
		</div>
	</header>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				{$calculo_dano} pontos da vida do {$perdedor} foram retirados.<br><br>
				Pontuação de vidas do {$perdedor}: {$vida}<br><br>

				{if ( $vida == '0' )}
					<h1>Fim de jogo</h1>
				{/if}
				
				{if ( $vida != '0' )}
					<a href="index.php" class="btn btn-primary">Nova Rodada</a>
				{/if}	 
			</div>
		</div>
	</div>
</section>
{include file="rodape.tpl"}