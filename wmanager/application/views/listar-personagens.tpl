{include file="cabecalho.tpl" titulo="Personagem"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/personagem.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Personagem</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="{$base_url}personagem/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">
						Novo
					</a>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			
			<br>
		
			<div class="table-responsive">
				<table class='table table-hover table-striped'>
					<thead>
						<th>Id</th>
						<th>Nome</th>
						<th>Arma</th>
						<th>Ataque</th>
						<th>Vida</th>
						<th>Agilidade</th>
						<th>Força</th>
						<th>Defesa</th>
						<th>Editar</th>
						<th>Excluir</th>
					</thead>

					<tbody>
						{if $id !== '0'}
							{section name=i loop=$id}
								<tr>
									<td>{$id[i]}</td>
									
									<td>{$nome[i]|capitalize}</td>
									
									<td>{$arma[i]}</td>

									<td>{$ataque[i]}</td>

									<td>{$vida[i]}</td>

									<td>{$agilidade[i]}</td>

									<td>{$forca[i]}</td>

									<td>{$defesa[i]}</td>
									
									<td>
										<a href="{$base_url}personagem/editar/{$id[i]}" title="editar">
											<span class="glyphicon glyphicon glyphicon-edit"></span>
										</a>
									</td>
									
									<td>
										<a href="{$base_url}personagem/fazerExclusao/{$id[i]}" class="excluir" title="excluir">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</td>
								</tr>
							{/section}
						{/if}
					</tbody>
				</table>
				
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							{$links_paginacao}
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
{include file="rodape.tpl"}