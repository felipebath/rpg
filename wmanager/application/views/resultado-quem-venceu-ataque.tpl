{include file="cabecalho.tpl" titulo="Turno"}
{include file="menu-2.tpl"}

<section class="container-fluid">
	<header class="page-header">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<h1>Iniciando Turno</h1>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				&nbsp;
			</div>
		</div>
	</header>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				{$atacante} (Atacante): {$dado_atacante} <br> 
				{$defensor} (Defensor): {$dado_defensor}<br>
				{$resultado}<br>
				Quem <b>venceu</b> o ataque foi o <b>{$vencedor}</b><br><br>

				// verificando o codigo do resultado para definir a url e label do button
				{if ( $codigo_resultado === '1' )}
					<a href='lancarDadosCalcularDano/{$vencedor}' class="btn btn-primary">Lançar dados para calcular dano</a>
				{/if}	
				
				{if ( $codigo_resultado === '2' )}
					<a href='index.php' class="btn btn-primary">voltar ao início</a>
				{/if}

				{if ( $codigo_resultado === '3' )}
					<a href='lancarDadosAtaque.php/{$vencedor}' class="btn btn-primary">vencedor começa atacando</a>
				{/if}
			</div>
		</div>
	</div>
</section>
{include file="rodape.tpl"}