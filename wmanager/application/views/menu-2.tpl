<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="menu-principal">
	<div class="container-fluid"> 
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ul-menu-principal">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		     </button>

			<a href="{$base_url}personagem" class="navbar-brand" title="início">W-MANAGER RPG&nbsp;&nbsp;</a>
		</div> 

		<div class="collapse navbar-collapse" id="ul-menu-principal">
			<ul class="nav navbar-nav navbar-left">
				
				<li>
					<li><a href="{$base_url}personagem/" title="Personagem">Personagem</a></li>
				</li>

				<li>
					<li><a href="{$base_url}turno/" title="Turno">Turno</a></li>
				</li>
			</ul>
		</div>
	</div>
</nav>