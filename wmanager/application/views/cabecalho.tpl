<!DOCTYPE html>
<html lang="pt-br">
	<head>
        <title>W Manager - {$titulo}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="{$base_url}/assets/css/estilo.css" rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js" defer></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js" defer></script>
        <script src="{$base_url}/assets/js/interface.js" defer></script>
        <script src="{$base_url}/assets/js/mascara.js" defer></script>
        <script src="{$base_url}/assets/js/validador.js" defer></script>
        <script src="{$base_url}/assets/js/principal.js" defer></script>
        <link href="{$base_url}/assets/css/datepicker.css" rel="stylesheet">
        <script src="{$base_url}/assets/js/bootstrap-datepicker.js" defer></script>
    </head>
    <body>

        <div id="caixa_carregamento">
            <div id="icone_carregamento">
                <img src="{$base_url}/assets/images/icone_carregamento.gif" alt="Carregando">
            </div>
        </div>