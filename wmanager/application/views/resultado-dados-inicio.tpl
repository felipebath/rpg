{include file="cabecalho.tpl" titulo="Turno"}
{include file="menu-2.tpl"}

<section class="container-fluid">
	<header class="page-header">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<h1>Iniciando Turno</h1>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				&nbsp;
			</div>
		</div>
	</header>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<u>Pontuação</u><br>
				<b>Humano: </b>{$humano}<br>
				<b>Orc: </b>{$orc}<br>

				{if ($codigo_resultado != '3')}
					Quem começa atacando é o {$resultado}.<br>
					<a href="{baseurl}turno/lancarDadosAtaque/{resultado}" class="btn btn-primary">Lançar dados para atacar</a>
				{/if}

				{if ($codigo_resultado == '3')}
					Tivemos um {$resultado}. Vamos jogar novamente!<br>
					<a href="{baseurl}turno/" class="btn btn-primary">Voltar ao início</a></p>	
				{/if}
			</div>
		</div>
	</div>
</section>
{include file="rodape.tpl"}