{include file="cabecalho.tpl" titulo="Turno"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}
	
<section class="container-fluid">
	<header class="page-header">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<h1>Iniciando Turno</h1>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				&nbsp;
			</div>
		</div>
	</header>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				{if $id !== '0'}
					{section name=i loop=$id}
				
						<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<h1>{$nome[i]}</h1>
							<p>Arma: {$arma[i]}</p>
							<p>Ataque: {$ataque[i]}</p>
							<p>Vida: {$vida[i]}</p>
							<p>Agilidade: {$agilidade[i]}</p>
							<p>Força: {$forca[i]}</p>
							<p>Defesa: {$defesa[i]}</p>
						</div>
					{/section}			
				{/if}

				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<h1>Resultado</h1>
					<p>Resultado Dado do Humano: {$dado_player_1}</p>
					<p>Resultado do Dado do Orc: {$dado_player_2}</p>
					<p>Mensagem: {$message}</p>
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">	
					<a href="{$base_url}turno/lancarDadosInicio" class="btn btn-primary">Lançar dados para ver quem começa</a>
				</div>
				<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
					&nbsp;
				</div>	
			</div>	
		</div>
	</div>
</section>
{include file="rodape.tpl"}