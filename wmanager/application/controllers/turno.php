<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Turno extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('personagem_model','personagem');
	}

	public function index(){
		
		$this->db->order_by('nome ASC');
		$sql_listar = $this->db->get('personagem');

		if ($sql_listar->num_rows() !== 0) 
		{
			foreach ($sql_listar->result_array() as $dados) 
			{
				$id[] 			= $dados['id'];
				$nome[] 		= $dados['nome'];
				$arma[] 		= $dados['arma'];
				$ataque[] 		= $dados['ataque'];
				$vida[] 		= $dados['vida'];
				$agilidade[]	= $dados['agilidade'];
				$forca[] 		= $dados['forca'];
				$defesa[] 		= $dados['defesa'];
			}
		} 
		else 
		{
			$id 		= '';
			$nome 		= '';
			$arma 		= '';
			$ataque 	= '';
			$vida 		= '';
			$agilidade 	= '';
			$forca 		= '';
			$defesa 	= '';
		}

		$this->smarty_ci->assign('id',$id);
		$this->smarty_ci->assign('nome',$nome);
		$this->smarty_ci->assign('arma',$arma);
		$this->smarty_ci->assign('ataque',$ataque);
		$this->smarty_ci->assign('vida',$vida);
		$this->smarty_ci->assign('agilidade',$agilidade);
		$this->smarty_ci->assign('forca',$forca);
		$this->smarty_ci->assign('defesa',$defesa);
		$this->smarty_ci->display('listar-turnos.tpl');

		$this->smarty_ci->display('inicio-turno.tpl');
	}

	public function lancarDadosInicio(){
		
		$humano = mt_rand(1,6);
		$orc 	= mt_rand(1,8);

		if ( $humano > $orc )
		{
			$resultado = 'Humano';
			$codigo_resultado = '1';
		}
		
		if ( $humano < $orc )
		{
			$resultado = 'Orc';
			$codigo_resultado = '2';	
		}	

		if ( $humano === $orc )
		{
			$resultado = 'Empate';
			$codigo_resultado = '3';
		}
		
		if ( $codigo_resultado !== '3' )
		{
			$resultado = "Quem começa atacando é o $resultado";
		}
		else
		{
			$resultado = 'Tivemos um empate. Vamos jogar de novo!';
		}	

		$this->smarty_ci->assign('codigo_resultado',$codigo_resultado);
		$this->smarty_ci->assign('resultado',$resultado);
		$this->smarty_ci->assign('humano',$humano);
		$this->smarty_ci->assign('orc',$orc);
		$this->smarty_ci->display('resultado-dados-inicio.tpl');
	}

	public function lancarDadosAtaque( $resultado ){
		
		$resultado = strip_tags( $resultado );

		if ( $resultado === 'Humano' )
		{
			$atacante = 'Humano';
			$defensor = 'Orc';

			$dado_atacante 	= mt_rand(1,6) + 2 + 2;
			$dado_defensor	= mt_rand(1,8) + 0 + 1;
		}
		else
		{
			$atacante = 'Orc';
			$defensor = 'Humano';

			$dado_atacante 	= mt_rand(1,8) + 0 + 1;
			$dado_defensor	= mt_rand(1,6) + 2 + 2;
		}

		if ( $dado_atacante > $dado_defensor )
		{
			$vencedor = $atacante;
			$resultado = 'Ataque bem sucedido.';
			$codigo_resultado = '1';
		}
		
		if ( $dado_atacante === $dado_defensor )
		{
			$vencedor = $defensor;
			$resultado = 'Valores iguais. Defensor ganhou e não sofreu danos.';
			$codigo_resultado = '2';
		}

		if ( $dado_atacante < $dado_defensor )
		{
			$vencedor = $defensor;
			$resultado = 'Defensor ganhou e não sofreu danos. Agora é a vez do Defensor atacar.';
			$codigo_resultado = '3';
		}
		
		$this->smarty_ci->assign('codigo_resultado',$codigo_resultado);
		$this->smarty_ci->assign('resultado',$resultado);
		$this->smarty_ci->assign('vencedor',$vencedor);
		$this->smarty_ci->assign('atacante',$atacante);
		$this->smarty_ci->assign('defensor',$defensor);
		$this->smarty_ci->assign('dado_atacante',$dado_atacante);
		$this->smarty_ci->assign('dado_defensor',$dado_defensor);
		$this->smarty_ci->display('resultado-quem-venceu-ataque.tpl');
	}

	public function lancarDadosCalculaDano( $vencedor ){

		$vencedor = strip_tags( $vencedor );

		if ( $vencedor === 'Humano' )
		{
			$perdedor = 'Orc';
			$calculo_dano = mt_rand(1,6) + 1;
		}
		else
		{
			$vencedor = 'Orc';
			$perdedor = 'Humano';
			$calculo_dano = mt_rand(1,8) + 2;
		}

		$dados = elements( array( 'id', 'nome',	'vida' ),$this->input->post());
		$this->personagem->setId    ($dados['id']);
		$this->personagem->setNome  ($dados['nome']);
		$this->personagem->setVida 	($dados['vida']);
		
		try{
			$sql = $this->personagem->getCalcularDano();
			if ($sql) 
			{
				$this->json_collection->assign('sucesso','true');
				$this->json_collection->display();
				exit;
			} 
			else 
			{
				$this->json_collection->assign('alerta','Ocorreu uma falha na subtração do dano');
				$this->json_collection->display();
			}		
		}catch(Exception $e){
			$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
			$this->json_collection->display();
			exit;
		}

		// obtendo a pontuação de vidas do perdedor
		$this->db->where('nome',$perdedor);
		$sql_listar = $this->db->get('personagem');

		if ($sql_listar->num_rows() !== 0) 
		{
			foreach ($sql_listar->result_array() as $dados) {
				$vida = $dados['vida'];
			}
		} 
		else 
		{
			$vida = '';
		}

		$this->smarty_ci->assign('vida',$vida);
		$this->smarty_ci->assign('calculo_dano',$calculo_dano);
		$this->smarty_ci->assign('perdedor',$perdedor);
		$this->smarty_ci->display('resultado-calculo-dano.tpl');
	}
}