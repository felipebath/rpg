<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personagem extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('personagem_model','personagem');
	}

	public function index( $pagina ){
		$pagina = ( int )$pagina;

		if( $pagina <= 0 || $pagina === 1 ){
			$pagina_posterior = 0;
		}else{
			$pagina_posterior = $pagina;
		}

		$this->db->limit(10,$pagina_posterior);		
		$this->db->order_by('nome ASC');
		$sql_listar = $this->db->get('personagem');

		if ($sql_listar->num_rows() !== 0) 
		{
			foreach ($sql_listar->result_array() as $dados) 
			{
				$id[] 			= $dados['id'];
				$nome[] 		= $dados['nome'];
				$arma[] 		= $dados['arma'];
				$ataque[] 		= $dados['ataque'];
				$vida[] 		= $dados['vida'];
				$agilidade[]	= $dados['agilidade'];
				$forca[] 		= $dados['forca'];
				$defesa[] 		= $dados['defesa'];
			}
		} 
		else 
		{
			$id 		= '';
			$nome 		= '';
			$arma 		= '';
			$ataque 	= '';
			$vida 		= '';
			$agilidade 	= '';
			$forca 		= '';
			$defesa 	= '';
		}

		$sql_numero_registros = $this->db->count_all('personagem');
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'personagem/index/';
		$config['total_rows'] = $sql_numero_registros;
		$config['per_page'] = 10; 
		$config['first_link'] = 'Primeira';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config); 
		$links_paginacao = $this->pagination->create_links();
		$this->smarty_ci->assign('numero_registros',$sql_numero_registros);
		$this->smarty_ci->assign('pagina',$pagina);
		$this->smarty_ci->assign('links_paginacao',$links_paginacao);
		$this->smarty_ci->assign('id',$id);
		$this->smarty_ci->assign('nome',$nome);
		$this->smarty_ci->assign('arma',$arma);
		$this->smarty_ci->assign('ataque',$ataque);
		$this->smarty_ci->assign('vida',$vida);
		$this->smarty_ci->assign('agilidade',$agilidade);
		$this->smarty_ci->assign('forca',$forca);
		$this->smarty_ci->assign('defesa',$defesa);
		$this->smarty_ci->display('listar-personagens.tpl');
	}

	public function cadastrar(){
		$this->smarty_ci->display('cadastrar-personagem.tpl');
	}

	public function fazerCadastro(){
		$this->form_validation->set_rules('nome','Nome','required|max_length[30]');
		
		if (!$this->form_validation->run()) 
		{
			$this->form_validation->set_error_delimiters("", "");
			$this->json_collection->assign('alerta',form_error('nome'));
			$this->json_collection->display();
			exit;
		} 
		else 
		{
			$dados = elements(
					array(
						'nome',
						'arma',
						'ataque',
						'vida',
						'agilidade',
						'forca',
						'defesa'
					),$this->input->post()
				);
						
			$this->personagem->setNome   	($dados['nome']);
			$this->personagem->setArma  	($dados['arma']);
			$this->personagem->setAtaque  	($dados['ataque']);
			$this->personagem->setVida 		($dados['vida']);
			$this->personagem->setAgilidade ($dados['agilidade']);
			$this->personagem->setForca 	($dados['forca']);
			$this->personagem->setDefesa 	($dados['defesa']);

			try{
				
				$sql = $this->personagem->getCadastrar();
				
				if ($sql) {
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				} else {
					$this->json_collection->assign('alerta','Ocorreu uma falha no cadastro do personagem');
					$this->json_collection->display();
				}

			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}		
	}

	public function editar( $id ){
		$id = (int) $id;

		$this->db->where('id',$id);
		$sql_listar = $this->db->get('personagem');

		if ($sql_listar->num_rows() !== 0) 
		{
			foreach ($sql_listar->result_array() as $dados) {
				$id 		= $dados['id'];
				$nome 		= $dados['nome'];
				$arma 		= $dados['arma'];
				$ataque 	= $dados['ataque'];
				$vida 		= $dados['vida'];
				$agilidade 	= $dados['agilidade'];
				$forca    	= $dados['forca'];
				$defesa    	= $dados['defesa'];
			}
		} 
		else 
		{
				$id 		= '';
				$nome 		= '';
				$arma 		= '';
				$ataque 	= '';
				$vida 		= '';
				$agilidade 	= '';
				$forca    	= '';
				$defesa    	= '';
		}

		$this->smarty_ci->assign('id',$id);
		$this->smarty_ci->assign('nome',$nome);
		$this->smarty_ci->assign('arma',$arma);
		$this->smarty_ci->assign('ataque',$ataque);
		$this->smarty_ci->assign('vida',$vida);
		$this->smarty_ci->assign('agilidade',$agilidade);
		$this->smarty_ci->assign('forca',$forca);
		$this->smarty_ci->assign('defesa',$defesa);
		$this->smarty_ci->display('editar-personagem.tpl');
	}

	public function fazerEdicao(){
		$this->form_validation->set_rules('nome','Nome','required|max_length[30]');
		
		if (!$this->form_validation->run()) {
			$this->form_validation->set_error_delimiters("", "");
			$this->json_collection->assign('alerta',form_error('nome'));
			$this->json_collection->display();
			exit;
		} else {
			$dados = elements(
					array(
						'id',
						'nome',
						'arma',
						'ataque',
						'vida',
						'agilidade',
						'forca',
						'defesa'
					),$this->input->post()
				);
			
			$this->personagem->setId        ($dados['id']);
			$this->personagem->setNome   	($dados['nome']);
			$this->personagem->setArma  	($dados['arma']);
			$this->personagem->setAtaque  	($dados['ataque']);
			$this->personagem->setVida 		($dados['vida']);
			$this->personagem->setAgilidade ($dados['agilidade']);
			$this->personagem->setForca 	($dados['forca']);
			$this->personagem->setDefesa 	($dados['defesa']);

			try{
				$sql = $this->personagem->getEditar();
				if ($sql) 
				{
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				} 
				else 
				{
					$this->json_collection->assign('alerta','Ocorreu uma falha na edição do personagem');
					$this->json_collection->display();
				}		
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}		
	}

	public function fazerExclusao( $id ){
		$id = (int) $id;
		$this->personagem->setId($id);
		
		try{
			$entrada = $this->personagem->getExcluir();
			if ($entrada) {
				header("Location:" . base_url() . 'personagem');
			} else {
				$this->json_collection->assign('alerta',form_error('Ocorreu uma falha ao excluir o personagem'));
				$this->json_collection->display();
			}			
		}catch(Exception $e){
			$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
			$this->json_collection->display();
			exit;
		}
	}
}