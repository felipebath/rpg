<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library('Smarty-3.1.16/smarty_ci');

        $this->smarty_ci->assign('application_dir',$this->config->config['application_dir']);
        $this->smarty_ci->assign('base_url', base_url());

        $this->smarty_ci->assign('nome_empresa_sessao',$this->session->userdata('nome_empresa'));

        $this->smarty_ci->assign('usuario_conta_sessao',$this->session->userdata('usuario_conta'));    

        $this->smarty_ci->assign('permissao_cadastrar_conta_conta',$this->session->userdata('permissao_cadastrar_conta_conta'));
        $this->smarty_ci->assign('permissao_visualizar_conta_conta',$this->session->userdata('permissao_visualizar_conta_conta'));
        $this->smarty_ci->assign('permissao_editar_conta_conta',$this->session->userdata('permissao_editar_conta_conta'));
        $this->smarty_ci->assign('permissao_excluir_conta_conta',$this->session->userdata('permissao_excluir_conta_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_cliente_conta',$this->session->userdata('permissao_cadastrar_cliente_conta'));
        $this->smarty_ci->assign('permissao_visualizar_cliente_conta',$this->session->userdata('permissao_visualizar_cliente_conta'));
        $this->smarty_ci->assign('permissao_editar_cliente_conta',$this->session->userdata('permissao_editar_cliente_conta'));
        $this->smarty_ci->assign('permissao_excluir_cliente_conta',$this->session->userdata('permissao_excluir_cliente_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_colaborador_conta',$this->session->userdata('permissao_cadastrar_colaborador_conta'));
        $this->smarty_ci->assign('permissao_visualizar_colaborador_conta',$this->session->userdata('permissao_visualizar_colaborador_conta'));
        $this->smarty_ci->assign('permissao_editar_colaborador_conta',$this->session->userdata('permissao_editar_colaborador_conta'));
        $this->smarty_ci->assign('permissao_excluir_colaborador_conta',$this->session->userdata('permissao_excluir_colaborador_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_estoque_conta',$this->session->userdata('permissao_cadastrar_estoque_conta'));
        $this->smarty_ci->assign('permissao_visualizar_estoque_conta',$this->session->userdata('permissao_visualizar_estoque_conta'));
        $this->smarty_ci->assign('permissao_editar_estoque_conta',$this->session->userdata('permissao_editar_estoque_conta'));
        $this->smarty_ci->assign('permissao_excluir_estoque_conta',$this->session->userdata('permissao_excluir_estoque_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_financeiro_conta',$this->session->userdata('permissao_cadastrar_financeiro_conta'));
        $this->smarty_ci->assign('permissao_visualizar_financeiro_conta',$this->session->userdata('permissao_visualizar_financeiro_conta'));
        $this->smarty_ci->assign('permissao_editar_financeiro_conta',$this->session->userdata('permissao_editar_financeiro_conta'));
        $this->smarty_ci->assign('permissao_excluir_financeiro_conta',$this->session->userdata('permissao_excluir_financeiro_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_fornecedor_conta',$this->session->userdata('permissao_cadastrar_fornecedor_conta'));
        $this->smarty_ci->assign('permissao_visualizar_fornecedor_conta',$this->session->userdata('permissao_visualizar_fornecedor_conta'));
        $this->smarty_ci->assign('permissao_editar_fornecedor_conta',$this->session->userdata('permissao_editar_fornecedor_conta'));
        $this->smarty_ci->assign('permissao_excluir_fornecedor_conta',$this->session->userdata('permissao_excluir_fornecedor_conta'));
        
        $this->smarty_ci->assign('permissao_cadastrar_estoque_conta',$this->session->userdata('permissao_cadastrar_estoque_conta'));
        $this->smarty_ci->assign('permissao_visualizar_estoque_conta',$this->session->userdata('permissao_visualizar_estoque_conta'));
        $this->smarty_ci->assign('permissao_editar_estoque_conta',$this->session->userdata('permissao_editar_estoque_conta'));
        $this->smarty_ci->assign('permissao_excluir_estoque_conta',$this->session->userdata('permissao_excluir_estoque_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_produto_conta',$this->session->userdata('permissao_cadastrar_produto_conta'));
        $this->smarty_ci->assign('permissao_visualizar_produto_conta',$this->session->userdata('permissao_visualizar_produto_conta'));
        $this->smarty_ci->assign('permissao_editar_produto_conta',$this->session->userdata('permissao_editar_produto_conta'));
        $this->smarty_ci->assign('permissao_excluir_produto_conta',$this->session->userdata('permissao_excluir_produto_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_servico_conta',$this->session->userdata('permissao_cadastrar_servico_conta'));
        $this->smarty_ci->assign('permissao_visualizar_servico_conta',$this->session->userdata('permissao_visualizar_servico_conta'));
        $this->smarty_ci->assign('permissao_editar_servico_conta',$this->session->userdata('permissao_editar_servico_conta'));
        $this->smarty_ci->assign('permissao_excluir_servico_conta',$this->session->userdata('permissao_excluir_servico_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_banco_conta',$this->session->userdata('permissao_cadastrar_banco_conta'));
        $this->smarty_ci->assign('permissao_visualizar_banco_conta',$this->session->userdata('permissao_visualizar_banco_conta'));
        $this->smarty_ci->assign('permissao_editar_banco_conta',$this->session->userdata('permissao_editar_banco_conta'));
        $this->smarty_ci->assign('permissao_excluir_banco_conta',$this->session->userdata('permissao_excluir_banco_conta'));

        $this->smarty_ci->assign('permissao_cadastrar_centro_custo_conta',$this->session->userdata('permissao_cadastrar_centro_custo_conta'));
        $this->smarty_ci->assign('permissao_visualizar_centro_custo_conta',$this->session->userdata('permissao_visualizar_centro_custo_conta'));
        $this->smarty_ci->assign('permissao_editar_centro_custo_conta',$this->session->userdata('permissao_editar_centro_custo_conta'));
        $this->smarty_ci->assign('permissao_excluir_centro_custo_conta',$this->session->userdata('permissao_excluir_centro_custo_conta'));
    }

    public function validarSessao(){
        if($this->session->userdata('usuario_conta')){
            return true;
        }else{
            return false;
        }
    }
}