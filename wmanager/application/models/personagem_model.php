<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Personagem_model extends CI_Model{

	private $id;
	private $nome;
	private $arma;
	private $ataque;
	private $vida;
	private $agilidade;
	private $forca;
	private $defesa;	
	
	public function __construct(){
		parent:: __construct();
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	}
	
	public function getId()
	{
	    return $this->id;
	}

	public function setNome($nome)
	{
	    $this->nome = $nome;
	}
	
	public function getNome()
	{
	    return $this->nome;
	}

	public function setArma($arma)
	{
	    $this->arma = $arma;
	}
	
	public function getArma()
	{
	    return $this->arma;
	}

	public function setAtaque($ataque)
	{
	    $this->ataque = $ataque;
	}
	
	public function getAtaque()
	{
	    return $this->ataque;
	}

	public function setVida($vida)
	{
	    $this->vida = $vida;
	}
	
	public function getVida()
	{
	    return $this->vida;
	}

	public function setAgilidade($agilidade)
	{
	    $this->agilidade = $agilidade;
	}
	
	public function getAgilidade()
	{
	    return $this->agilidade;
	}

	public function setForca($forca)
	{
	    $this->forca = $forca;
	}
	
	public function getForca()
	{
	    return $this->forca;
	}

	public function setDefesa($defesa)
	{
	    $this->defesa = $defesa;
	}
	
	public function getDefesa()
	{
	    return $this->defesa;
	}
	
	public function getCadastrar(){
		return $this->cadastro();
	}

	public function cadastro(){
		$dados = array(
				'nome'		=> $this->getNome(),
				'arma' 		=> $this->getArma(),
				'ataque' 	=> $this->getAtaque(),
				'vida' 		=> $this->getVida(),
				'agilidade' => $this->getAgilidade(),
				'forca' 	=> $this->getForca(),
				'defesa' 	=> $this->getDefesa()
			);
		$sql = $this->db->insert( 'personagem',$dados );

		if ($sql) {
			return true;
		} else {
			return false;
		}		
	}

	public function getEditar(){
		return $this->editar();
	}

	public function editar(){
		$dados = array(
				'id' 		=> $this->getId(),
				'nome'		=> $this->getNome(),
				'arma' 		=> $this->getArma(),
				'ataque' 	=> $this->getAtaque(),
				'vida' 		=> $this->getVida(),
				'agilidade' => $this->getAgilidade(),
				'forca' 	=> $this->getForca(),
				'defesa' 	=> $this->getDefesa()			
		 	);

		$this->db->where('id',$this->getId());
		$sql = $this->db->update( 'personagem',$dados );

		if ($sql) {
			return true;
		} else {
			return false;
		}		
	}

	public function getExcluir(){
		return $this->excluir();
	}

	private function excluir(){
		$dados = array(	'id' => $this->getId() );

		$this->db->where('id', $this->getId());

		$sql = $this->db->delete( 'personagem',$dados );

		if( $sql )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getCalcularDano(){
		return $this->calcular();
	}

	public function calcular(){
		$dados = array(
				'id' 		=> $this->getId(),
				'nome'		=> $this->getNome(),
				'vida' 		=> $this->getVida()			
		 	);

		$this->db->where('nome',$this->getNome());
		$sql = $this->db->update( 'personagem',$dados );

		if ($sql) {
			return true;
		} else {
			return false;
		}		
	}	
}